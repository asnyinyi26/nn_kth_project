<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Charge extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'charges';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['code','name', 'product', 'amount', 'charge_option', 'charge_type', 'accounting_id', 'penalty', 'active', 'override'];
    public static function getSeqRef($t){// $t from setting table

        $setting = getSetting();
        $s_setting = getSettingKey($t,$setting);

        $arr_setting = $s_setting != null?json_decode($s_setting,true):[];
        $last_seq = self::max('seq');
        $last_seq = $last_seq > 0 ?$last_seq+1:1;

        return getAutoRef($last_seq ,$arr_setting);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($row){

            $last_seq = self::max('seq');
            $last_seq = $last_seq > 0 ?$last_seq+1:1;

            $row->seq = $last_seq;

            $userid = auth()->user()->id;
            $row->created_by = $userid;
            $row->updated_by = $userid;

        });

        static::updating(function($row)
        {
            $userid = auth()->user()->id;
            $row->updated_by = $userid;
        });

    }
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    public function loan_product()
    {
        return $this->hasMany(LoanProduct::class);
    }

    */


    public function loan_products_charge()
    {
        return $this->belongsToMany('App\Models\LoanProduct', 'charge_loan_products', 'charge_id', 'loan_product_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
