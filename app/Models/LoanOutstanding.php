<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use App\Helpers\S;

class LoanOutstanding extends Loan
{

    public function addButtonCustom()
    {
        $loan_id = $this->id;
        $last_payment_id = LoanPayment2::where('disbursement_id',$loan_id)->max('id');
        $schedule_backup = ScheduleBackup::where('loan_id',$loan_id)->first();
        $b = array();
        $client= "";
        
        if(companyReportPart() == 'company.moeyan'){
            array_push($b, '<a href="' . url("/admin/print-disbursement?is_pop=1&disbursement_id={$this->id}") . '" style="color:#0e455e;border-radius:5px;"><i class="fa fa-file-text"></i>Receipt</a>');
            $client = '<a href="'.url("/admin/client_pop?client_id={$this->client_id}").'" data-remote="false" data-toggle="modal" data-target="#show-detail-modal" class="btn btn-xs btn-info"><i class="fa fa-money"></i></a> ';
        }

        if(companyReportPart() == 'company.angkor'){
            array_push($b, '<a href="' . url("/admin/print-disbursement?is_pop=1&disbursement_id={$this->id}") . '" style="color:#0e455e;border-radius:5px;"><i class="fa fa-file-text"></i>Received Form</a>');
            array_push($b, '<a href="' . url("/admin/print-contract?is_pop=1&disbursement_id={$this->id}") . '" style="color:#0e455e;border-radius:5px;"><i class="fa fa-file-text"></i>Agreement Form</a>');
        }

        array_push($b,
                        '<a href="'.url("/admin/print_schedule?loan_id={$this->id}").'" data-remote="false" data-toggle="modal" data-target="#show-detail-modal" style="color:#0e455e;border-radius:5px;"><i class="fa fa-eye"></i>Schedule Form</a>',
                        '<a href="'.url("/admin/payment_pop?loan_id={$this->id}").'" data-remote="false" data-toggle="modal" data-target="#show-detail-modal" style="color:#0e455e;border-radius:5px;"><i class="fa fa-plus-circle"></i>Add Payment</a>',
                        '<a href="'.url("/admin/loan-write-off/create?is_frame=1&loan_id={$this->id}").'" data-remote="false" data-toggle="modal" data-target="#show-write-off-modal" style="color:#0e455e;border-radius:5px;"><i class="fa fa-plus-circle"></i>Write Off</a>',
                        '<a data-whatever="'.$this->id.'" data-toggle="modal" data-target="#exampleModal" class="btn btn-danger" style="color:#0e455e;border-radius:5px;"></i>Cancel Loan</a>'
                    );
                    
        return $client . S::getActionButton($b);

        /*if($loan_id>0 && $schedule_backup != null){
            $b = '<a href="'.url("/api/list-last-payment?payment_id={$last_payment_id}").'" data-remote="false" data-toggle="modal" data-target="#show-detail-modal" class="btn btn-xs btn-danger">Roll Back</a>';
        }*/
    }


    public static function boot()
    {
        parent::boot();

        static::creating(function($row)
        {
            $userid = auth()->user()->id;
            $row->created_by = $userid;
            $row->updated_by = $userid;
        });
        static::updating(function($row)
        {
            $userid = auth()->user()->id;
            $row->updated_by = $userid;
        });


        static::addGlobalScope('disbursement_status', function (Builder $builder) {
            $builder->where('disbursement_status', 'Activated');
        });
    }
}
