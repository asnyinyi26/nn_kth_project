<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class LoanCalculate extends Model
{
   protected $table = 'loan_disbursement_calculate';

   public function disbursements(){
       return $this->belongsTo(Loan::class,'disbursement_id');
   }

    public static function boot()
    {
        parent::boot();

        static::creating(function($row)
        {
            if(auth()->check()) {
                $userid = auth()->user()->id;
                $row->created_by = $userid;
                $row->updated_by = $userid;
            }
        });

        static::updating(function($row)
        {
            if(auth()->check()) {
                $userid = auth()->user()->id;
                $row->updated_by = $userid;
            }
        });
    }
}
