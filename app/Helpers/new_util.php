<?php

function companyReportPart()
{
    return env('COMPANY_REPORT_PART', 'company.mkt');
}

function RemoveSpecialChar($value){
    $result  = preg_replace('/[^a-zA-Z0-9_ -]/s','',$value);

    return $result;
}

function RemoveChar($value){
    $result  = preg_replace('/[^0-9]/s','',$value);

    return $result;
}

function getEnvCountry()
{
    return env('COUNTRY', 'MM');
}
