<?php

namespace App\Http\Controllers\M;

use App\Models\AccountChart;
use App\Models\AccountSection;
use App\Models\ClientU;
use App\Models\LoanApi;
use App\Models\LoanPaymentU;
use App\SecurityLogin;
use Carbon\Carbon;
use Dotenv\Validator;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class PaymentController extends Controller
{

    public function getCash(){
        //$row = C

        $row = AccountChart::join('account_sections','account_sections.id','=','account_charts.section_id')

            ->where('section_id',10)
            ->selectRaw('account_charts.*,account_sections.title')
            ->paginate(10);


        return [
            'rows_cash'=>$row
        ];
    }

    public function getLoanCalculate(Request $request){
        $user = SecurityLogin::decrypt($request->user_id);

        return view('partials.mobile.loan-calculate.loan-calculate');
    }


    public function  getLoanPayment(Request $request){
        $user_id = $request->user_id;
        $loan = LoanApi::where('loan_officer_id',$user_id)->get();
        $arr = [];


        if ($loan != null) {
            foreach ($loan as $r) {
                $arr[$r->id] = $r->id;
            }
        }

        $date = Carbon::now()->format('Y-m-d');
        $rows = LoanPaymentU::where(function ($query) use ($arr) {
            if (is_array($arr)) {
                if (count($arr) > 0) {
                    return $query->whereIn('disbursement_id',$arr);
                }
            }

        })/*->whereDate('payment_date',$date)*/
        ->where('paid_by','loan-officer')
        ->get();


        $arr_loan = [];
        foreach ($rows as $row){

            $client = ClientU::find($row->client_id);
            $loan = LoanApi::find($row->disbursement_id);

            $arr_loan[]=[
                'client_name'=>$client != null?$client->name:'',
                'loan_number'=>$loan != null?$loan->disbursement_number:'',
                'payment_number'=>$row->payment_number,
                'payment_id'=>$row->id,
                'paid_by'=>$row->paid_by,
                'amount'=>$row->total_payment,
            ];
        }



        return [
            'rows_loan_payment'=>$arr_loan,
        ];
    }


    public function confirmPayment(Request $request){

        $payment_id = $request->payment_id;
        $m = LoanPaymentU::find($payment_id);
        if ($m != null){
            $m->paid_by='client';
            if ($m->save()){
                return response($m);
            }
            else{
                return [
                    'id'=>0
                ];
            }
        }
        else{
            return [
                'id'=>0
            ];
        }
    }





}
