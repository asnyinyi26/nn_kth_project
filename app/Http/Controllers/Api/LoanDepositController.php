<?php

namespace App\Http\Controllers\Api;

use App\Models\LoanCharge;
use App\Models\LoanCompulsory;
use App\Models\Loan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanDepositController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        $charge = LoanCharge::where('charge_type', 1)->where('status','Yes')->get();
        $compulsory = LoanCompulsory::where('compulsory_product_type_id', 1)->where('status','Yes')->get();



        $arr = [];
        if ($charge!=null){

            foreach ($charge as $r){
                $arr[$r->loan_id] = $r->loan_id;
            }
        }
        if ($compulsory!=null){

            foreach ($compulsory as $r){
                $arr[$r->loan_id] = $r->loan_id;
            }
        }

        if ($search_term)
        {

            $results = Loan::join('loan_charge','loan_charge.loan_id','loans.id')
                ->join('loan_compulsory','loan_compulsory.loan_id','loans.id')
                ->where('loans.deposit_paid', 'No')
                ->where(function ($q){
                    $q->where('loan_charge.charge_type','1')
                       ->orwhere('loan_compulsory.compulsory_product_type_id','1');

                })
                ->where('loans.disbursement_status', 'Approved')
                ->where('loans.disbursement_number', 'LIKE', '%'.$search_term.'%')
                ->selectRaw('distinct loans.*')
                 ->paginate(100);
        }
        else
        {
            $results = Loan::join('loan_charge','loan_charge.loan_id','loans.id')
                ->join('loan_compulsory','loan_compulsory.loan_id','loans.id')
                ->where('loans.deposit_paid', 'No')
                ->where(function ($q){
                    $q->where('loan_charge.charge_type','1')
                        ->orwhere('loan_compulsory.compulsory_product_type_id','1');

                })
                ->where('loans.disbursement_status', 'Approved')
                ->where('loans.disbursement_number', 'LIKE', '%'.$search_term.'%')
                ->selectRaw('distinct loans.*')
                ->paginate(100);
        }

        return $results;
    }

    public function show($id)
    {
        return Loan::find($id);
    }
}
