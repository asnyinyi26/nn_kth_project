<?php

namespace App\Http\Controllers\Api;


use App\Models\CenterLeader;
use App\Models\Client;
use App\Models\ClientApi;
use App\Models\ClientU;
use App\Models\CompulsorySavingTransaction;
use App\Models\Loan;
use App\Models\LoanCalculate;
use App\Models\LoanCompulsory;
use App\Models\LoanPayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            if (companyReportPart() == 'company.bolika'){
                $results = ClientU::orWhere('name', 'LIKE', '%'.$search_term.'%')
                    ->orWhere('name_other', 'LIKE', '%'.$search_term.'%')
                    ->orWhere('primary_phone_number', 'LIKE', '%'.$search_term.'%')
                    ->orWhere('nrc_number', 'LIKE', '%'.$search_term.'%')
                    ->orWhere('client_number', 'LIKE', '%'.$search_term.'%')
                    ->selectRaw("id,CONCAT(COALESCE(client_number,''),'-', name ,' ', name_other) as client_number")
                    ->paginate(100);
            }else{
                $results = ClientU::orWhere('name', 'LIKE', '%'.$search_term.'%')
                    ->orWhere('name_other', 'LIKE', '%'.$search_term.'%')
                    ->orWhere('primary_phone_number', 'LIKE', '%'.$search_term.'%')
                    ->orWhere('nrc_number', 'LIKE', '%'.$search_term.'%')
                    ->orWhere('client_number', 'LIKE', '%'.$search_term.'%')
                    ->selectRaw("id,CONCAT(COALESCE(client_number,''),'-',COALESCE(name,name_other)) as client_number")
                    ->paginate(100);
            }


        }
        else
        {
            if (companyReportPart() == 'company.bolika'){
                $results = ClientU::selectRaw("id,CONCAT(COALESCE(client_number,''),'-', name ,' ', name_other) as client_number")
                    ->paginate(10);
            }else{
                $results = ClientU::selectRaw("id,CONCAT(COALESCE(client_number,''),'-',COALESCE(name,name_other)) as client_number")
                    ->paginate(10);
            }



        }

        return $results;
    }

    public function show($id)
    {
        return Client::find($id);
    }
//if(res.) {
//
//$('[name="client_nrc_number"]').val(res.);
//$('[name="client_phone"]').val(res.);
//$('.c_image').prop('src', '{{ asset('/') }}' + '/' + res.photo_of_client);
//}

//}
    public function showG($id)
    {
        $m =  Client::find($id);
        if($m != null){


            $group_loans = $m->group_loans;
            $loan_officer = $m->officer_name;
            $branch = $m->branch_name;

            $g = null;
           if($group_loans != null){
               $g = isset($group_loans[0])?$group_loans[0]:null;
           }

           $center = CenterLeader::find($m->center_leader_id);

            $saving_amount = CompulsorySavingTransaction::where('customer_id',$m->id)->sum('amount');
           return [
               'id' => $m->id ,
               'photo_of_client' => $m->photo_of_client ,
               'nrc_number' => $m->nrc_number ,
               'client_number' => $m->client_number ,
               'name' => $m->name ,
               'name_other' => $m->name_other ,
               'you_are_a_group_leader' => $m->you_are_a_group_leader,
               'you_are_a_center_leader' => $m->you_are_a_center_leader,
               'loan_officer_id' => optional($loan_officer)->id ??'0',
               'center_id' => optional($center)->id ??'0',
               'center_name' => optional($center)->title ??'',
               'loan_officer_name' => optional($loan_officer)->name??'' ,
               'primary_phone_number' => $m->primary_phone_number ,
               'group_id' => optional($g)->id??'0' ,
               'group_code' => optional($g)->group_code??'' ,
               'group_name' => optional($g)->group_name??'' ,
               'branch_id' => $m->branch_id,
               'branch_name' => optional($branch)->title,
               'saving_amount' => $saving_amount
           ];
        }
    }


    public function  getClient(Request $request){
        $client_id = $request->client_id;
        $client = ClientApi::where('id',$client_id)->first();

        return $client;


    }

    public function  getLoan(Request $request){
        $loan_disbursement_id = $request->loan_disbursement_id;

        $m = Loan::find($loan_disbursement_id);

        if($m != null){
            $c = ClientApi::find($m->client_id);
            if($c != null){
                return response()->json([
                    'error' => 0,
                    'client_id' => $c->id,
                    'loan_id' => $loan_disbursement_id,
                    'disbursement_number' => $m->disbursement_number,
                    'client_number' => $c->client_number,
                    'name' => $c->name,
                    'nrc_number' => $c->nrc_number,
                ]);
            }
        }

        return response()->json(['error' => 1]);


    }

}
