<?php

namespace App\Http\Controllers\Api;

use App\Models\AccountChart;
use App\Models\CompulsoryAccrueInterests;
use App\Models\CompulsorySavingTransaction;
use App\Models\Loan;
use App\Models\LoanCompulsory;
use App\Models\PaidDisbursement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AccrueInterestCompulsory extends Controller
{


    public function index(Request $request)
    {

        $date_now = date('Y-m-d');
        ////$end_of_month = $date_now;
        $end_of_month = self::getLastDayMonth($date_now);

        if($date_now == $end_of_month){
            $loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->get();

            if($loan_compulsory){
                foreach ($loan_compulsory as $saving){
                    $saving_accrue_int = CompulsorySavingTransaction::where('loan_compulsory_id', $saving->id)
                        ->where('train_type','accrue-interest')->orderBy('tran_date', 'DESC')->first();
                    $accr_date = $saving_accrue_int->tran_date;
                    if($accr_date != $date_now){
                        $total_principle = optional($saving_accrue_int)->total_principle ? $saving_accrue_int->total_principle : 0;
                        $inetrest_rate = $saving->interest_rate / 100 ;
                        $saving_interest_amt = $total_principle * $inetrest_rate;

                        $accrue_no = CompulsoryAccrueInterests::getSeqRef('accrue-interest');

                        $accrue_interrest = New CompulsoryAccrueInterests();
                        $accrue_interrest->compulsory_id = $saving->compulsory_id;
                        $accrue_interrest->loan_compulsory_id = $saving->id;
                        $accrue_interrest->loan_id = $saving->loan_id;
                        $accrue_interrest->client_id = $saving->client_id;
                        $accrue_interrest->train_type = 'accrue-interest';
                        $accrue_interrest->tran_id_ref = $saving->loan_id;
                        $accrue_interrest->tran_date = date('Y-m-d');
                        $accrue_interrest->reference = $accrue_no;
                        $accrue_interrest->amount = $saving_interest_amt;
                        //$accrue_interrest->seq = '';
                        if($accrue_interrest->save()){
                            CompulsoryAccrueInterests::accAccurInterestCompulsory($accrue_interrest);
                        }

                    }
                }
                echo "Success";
            }
        }
    }

    public static function getLastDayMonth($date)
    {
        $sql = "SELECT LAST_DAY('{$date}') as d";
        $d = DB::select($sql);
        if (count($d) > 0) {
            return $d[0]->d;
        } else {
            return null;
        }
    }

    public static function getMonth($date)
    {
        $sql = "SELECT MONTH('{$date}') as d";
        $d = DB::select($sql);
        if (count($d) > 0) {
            return $d[0]->d;
        } else {
            return null;
        }
    }
    public static function getYear($date)
    {
        $sql = "SELECT Year('{$date}') as y";
        $d = DB::select($sql);
        if (count($d) > 0) {
            return $d[0]->y;
        } else {
            return null;
        }
    }

    public static function dateDiff($f_date, $t_date)
    {
        $sql = "SELECT DATEDIFF('{$t_date}', '{$f_date}') as d";
        $d = DB::select($sql);
        if (count($d) > 0) {
            return $d[0]->d;
        } else {
            return null;
        }
    }


    public function savingInterestMKT(Request $request)
    {

        $date_now = date('Y-m-d');
        ////$end_of_month = $date_now;
        $end_of_month = self::getLastDayMonth($date_now);

        if($date_now == $end_of_month){
            //$loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->get();
            $loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->simplePaginate(100);
            $n = ($loan_compulsory->currentPage()+1);

            if($loan_compulsory){
                foreach ($loan_compulsory as $saving){
                    $saving_accrue_int = CompulsorySavingTransaction::where('loan_compulsory_id', $saving->id)
                        ->where('train_type','accrue-interest')->orderBy('tran_date', 'DESC')->first();
                    $accr_date = $saving_accrue_int->tran_date;
                    if($accr_date != $date_now){
                        $total_principle = optional($saving_accrue_int)->total_principle ? $saving_accrue_int->total_principle : 0;
                        $inetrest_rate = $saving->interest_rate / 100 ;
                        $saving_interest_amt = $total_principle * $inetrest_rate;

                        $accrue_no = CompulsoryAccrueInterests::getSeqRef('accrue-interest');

                        $accrue_interrest = New CompulsoryAccrueInterests();
                        $accrue_interrest->compulsory_id = $saving->compulsory_id;
                        $accrue_interrest->loan_compulsory_id = $saving->id;
                        $accrue_interrest->loan_id = $saving->loan_id;
                        $accrue_interrest->client_id = $saving->client_id;
                        $accrue_interrest->train_type = 'accrue-interest';
                        $accrue_interrest->tran_id_ref = $saving->loan_id;
                        $accrue_interrest->tran_date = date('Y-m-d');
                        $accrue_interrest->reference = $accrue_no;
                        $accrue_interrest->amount = $saving_interest_amt;
                        //$accrue_interrest->seq = '';
                        if($accrue_interrest->save()){
                            CompulsoryAccrueInterests::accAccurInterestCompulsory($accrue_interrest);
                        }

                    }
                }
            }

            if($loan_compulsory->hasMorePages()) {
                return '<head>
                <meta http-equiv=\'refresh\' content=\'1; URL=' . url('api/savingInterestMKT?page=' . $n) . '\'>
                </head><h1>Wait ...('.$n.')</h1>';
            }else{
                return '<h1>OK</h1>';
            }
        }
    }

    public function savingInterestMKT09(Request $request)
    {

        $date_now = date('2019-09-30');
        $end_of_month = date('2019-09-30');

        if($date_now == $end_of_month){
            //$loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->get();
            $loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->simplePaginate(100);
            $n = ($loan_compulsory->currentPage()+1);

            if($loan_compulsory){
                foreach ($loan_compulsory as $saving){
                    $saving_accrue_int = CompulsorySavingTransaction::where('loan_compulsory_id', $saving->id)
                        ->where('train_type','accrue-interest')->orderBy('tran_date', 'DESC')->first();
                    $accr_date = $saving_accrue_int->tran_date;
                    if($accr_date != $date_now){
                        $total_principle = optional($saving_accrue_int)->total_principle ? $saving_accrue_int->total_principle : 0;
                        $inetrest_rate = $saving->interest_rate / 100 ;
                        $saving_interest_amt = $total_principle * $inetrest_rate;

                        $accrue_no = CompulsoryAccrueInterests::getSeqRef('accrue-interest');

                        $accrue_interrest = New CompulsoryAccrueInterests();
                        $accrue_interrest->compulsory_id = $saving->compulsory_id;
                        $accrue_interrest->loan_compulsory_id = $saving->id;
                        $accrue_interrest->loan_id = $saving->loan_id;
                        $accrue_interrest->client_id = $saving->client_id;
                        $accrue_interrest->train_type = 'accrue-interest';
                        $accrue_interrest->tran_id_ref = $saving->loan_id;
                        $accrue_interrest->tran_date = date('Y-m-d');
                        $accrue_interrest->reference = $accrue_no;
                        $accrue_interrest->amount = $saving_interest_amt;
                        //$accrue_interrest->seq = '';
                        if($accrue_interrest->save()){
                            CompulsoryAccrueInterests::accAccurInterestCompulsory($accrue_interrest);
                        }

                    }
                }
            }

            if($loan_compulsory->hasMorePages()) {
                return '<head>
                <meta http-equiv=\'refresh\' content=\'1; URL=' . url('api/savingInterestMKT09?page=' . $n) . '\'>
                </head><h1>Wait ...('.$n.')</h1>';
            }else{
                return '<h1>OK</h1>';
            }
        }
    }

    public function savingInterestMKT10(Request $request)
    {

        $date_now = date('2019-10-31');
        $end_of_month = date('2019-10-31');

        if($date_now == $end_of_month){
            //$loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->get();
            $loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->simplePaginate(100);
            $n = ($loan_compulsory->currentPage()+1);

            if($loan_compulsory){
                foreach ($loan_compulsory as $saving){
                    $saving_accrue_int = CompulsorySavingTransaction::where('loan_compulsory_id', $saving->id)
                        ->where('train_type','accrue-interest')->orderBy('tran_date', 'DESC')->first();
                    $accr_date = $saving_accrue_int->tran_date;
                    if($accr_date != $date_now){
                        $total_principle = optional($saving_accrue_int)->total_principle ? $saving_accrue_int->total_principle : 0;
                        $inetrest_rate = $saving->interest_rate / 100 ;
                        $saving_interest_amt = $total_principle * $inetrest_rate;

                        $accrue_no = CompulsoryAccrueInterests::getSeqRef('accrue-interest');

                        $accrue_interrest = New CompulsoryAccrueInterests();
                        $accrue_interrest->compulsory_id = $saving->compulsory_id;
                        $accrue_interrest->loan_compulsory_id = $saving->id;
                        $accrue_interrest->loan_id = $saving->loan_id;
                        $accrue_interrest->client_id = $saving->client_id;
                        $accrue_interrest->train_type = 'accrue-interest';
                        $accrue_interrest->tran_id_ref = $saving->loan_id;
                        $accrue_interrest->tran_date = date('Y-m-d');
                        $accrue_interrest->reference = $accrue_no;
                        $accrue_interrest->amount = $saving_interest_amt;
                        //$accrue_interrest->seq = '';
                        if($accrue_interrest->save()){
                            CompulsoryAccrueInterests::accAccurInterestCompulsory($accrue_interrest);
                        }

                    }
                }
            }

            if($loan_compulsory->hasMorePages()) {
                return '<head>
                <meta http-equiv=\'refresh\' content=\'1; URL=' . url('api/savingInterestMKT10?page=' . $n) . '\'>
                </head><h1>Wait ...('.$n.')</h1>';
            }else{
                return '<h1>OK</h1>';
            }
        }
    }


    public function savingInterestDefualt(Request $request)
    {

        //$date_now = '2019-11-30';
        $date_now = date('Y-m-d');
        $current_month = self::getMonth($date_now);
        $current_year = self::getYear($date_now);
        $end_of_month = self::getLastDayMonth($date_now);

        //$date_now = '2019-09-30';
        ////$end_of_month = $date_now;


        if($date_now == $end_of_month){
            ////$loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->get();
            $loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->simplePaginate(100);
            $n = ($loan_compulsory->currentPage()+1);

            if($loan_compulsory){
                foreach ($loan_compulsory as $saving){

                    $loans = Loan::find($saving->loan_id);
                    $paid_disburse = PaidDisbursement::where('contract_id',$saving->loan_id)->first();

                    $paid_disburse_date = $paid_disburse->paid_disbursement_date;
                    $paid_disburse_month = self::getMonth($paid_disburse_date);
                    $paid_disburse_year = self::getYear($paid_disburse_date);

                    $inetrest_rate = $saving->interest_rate / 100 ;
                    $saving_amount = $saving->principles-0;

                    if($current_month == $paid_disburse_month && $current_year == $paid_disburse_year){
                        $countday = self::dateDiff($paid_disburse_date,$end_of_month);
                        $saving_interest_amt = (($inetrest_rate * $saving_amount)  / 30 ) * $countday;

                    } else{
                        $saving_interest_amt = ($inetrest_rate * $saving_amount);

                    }

                    $accrue_no = CompulsoryAccrueInterests::getSeqRef('accrue-interest');

                    $accrue_interrest = New CompulsoryAccrueInterests();
                    $accrue_interrest->compulsory_id = $saving->compulsory_id;
                    $accrue_interrest->loan_compulsory_id = $saving->id;
                    $accrue_interrest->loan_id = $saving->loan_id;
                    $accrue_interrest->client_id = $saving->client_id;
                    $accrue_interrest->train_type = 'accrue-interest';
                    $accrue_interrest->tran_id_ref = $saving->loan_id;
                    $accrue_interrest->tran_date = $date_now;
                    $accrue_interrest->reference = $accrue_no;
                    $accrue_interrest->amount = $saving_interest_amt;
                    //dd($accrue_interrest);
                    if($accrue_interrest->save()){
                        CompulsoryAccrueInterests::accAccurInterestCompulsory($accrue_interrest);
                    }

                }

            }

            if($loan_compulsory->hasMorePages()) {
                return '<head>
                <meta http-equiv=\'refresh\' content=\'1; URL=' . url('api/savingInterestDefualt?page=' . $n) . '\'>
                </head><h1>Wait ...('.$n.')</h1>';
            }else{
                return '<h1>OK</h1>';
            }
        }
    }

    public function savingInterestAngkorMigrate(Request $request)
    {

        //$date_now = '2019-11-30';
        $date_now = date('Y-m-d');
        $current_month = self::getMonth($date_now);
        $current_year = self::getYear($date_now);
        $end_of_month = self::getLastDayMonth($date_now);

        ////$end_of_month = $date_now;
        if($date_now == $end_of_month){


            $loan_compulsory = LoanCompulsory::where('compulsory_status','Active')->simplePaginate(50);
            $n = ($loan_compulsory->currentPage()+1);


            if($loan_compulsory){
                foreach ($loan_compulsory as $saving){
                    //dd($saving);
                    $loans = Loan::find($saving->loan_id);
                    $paid_disburse = PaidDisbursement::where('contract_id',$saving->loan_id)->first();

                    $paid_disburse_date = $paid_disburse->paid_disbursement_date;
                    $paid_disburse_month = self::getMonth($paid_disburse_date);
                    $paid_disburse_year = self::getYear($paid_disburse_date);

                    $inetrest_rate = $saving->interest_rate / 100 ;
                    $saving_amount = $saving->principles-0;

                    $countday = self::dateDiff($paid_disburse_date,$end_of_month);
                    $saving_interest_amt = (($inetrest_rate * $saving_amount)  / 30 ) * $countday;



                    $accrue_no = CompulsoryAccrueInterests::getSeqRef('accrue-interest');

                    $accrue_interrest = New CompulsoryAccrueInterests();
                    $accrue_interrest->compulsory_id = $saving->compulsory_id;
                    $accrue_interrest->loan_compulsory_id = $saving->id;
                    $accrue_interrest->loan_id = $saving->loan_id;
                    $accrue_interrest->client_id = $saving->client_id;
                    $accrue_interrest->train_type = 'accrue-interest';
                    $accrue_interrest->tran_id_ref = $saving->loan_id;
                    $accrue_interrest->tran_date = $date_now;
                    $accrue_interrest->reference = $accrue_no;
                    $accrue_interrest->amount = $saving_interest_amt;
                    //dd($accrue_interrest);
                    if($accrue_interrest->save()){
                        CompulsoryAccrueInterests::accAccurInterestCompulsory($accrue_interrest);
                    }

                }
            }


            if($loan_compulsory->hasMorePages()) {
                return '<head>
                <meta http-equiv=\'refresh\' content=\'1; URL=' . url('api/savingInterestAngkorMigrate?page=' . $n) . '\'>
                </head><h1>Wait ...('.$n.')</h1>';
            }else{
                return '<h1>OK</h1>';
            }
        }
    }


}
