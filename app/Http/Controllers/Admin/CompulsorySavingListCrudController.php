<?php

namespace App\Http\Controllers\Admin;

use App\Models\Branch;
use App\Models\CenterLeader;
use App\Models\Client;
use App\Models\CompulsoryProduct;
use App\Models\CompulsorySavingList;
use App\Models\Loan2;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CompulsorySavingListRequest as StoreRequest;
use App\Http\Requests\CompulsorySavingListRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class CompulsorySavingListCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CompulsorySavingListCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\CompulsorySavingList');
        $this->crud->disableResponsiveTable();
        $this->crud->setListView('partials.loan_disbursement.composery_list_total');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/compulsorysavinglist');
        $this->crud->setEntityNameStrings('compulsorysavinglist', 'compulsory_saving_lists');
        $this->crud->enableExportButtons();

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */


        $this->crud->denyAccess(['create', 'delete','update']);
        // TODO: remove setFromDb() and manually define Fields and Columns
       //$this->crud->setFromDb();
        $this->crud->addClause('LeftJoin', 'loans', function ($join) {
            $join->on('loans.id', '=', 'loan_compulsory.loan_id');
        });

        $this->crud->addClause('LeftJoin', 'branches', function ($join) {
            $join->on('branches.id', '=', 'loans.branch_id');
        });

        $this->crud->addClause('selectRaw', 'loan_compulsory.*,loans.disbursement_number,
                                        loans.branch_id as branch_id,
                                        loans.center_leader_id as center_leader_id ,
                                        loans.loan_officer_id,
                                        loan_compulsory.client_id,
                                        loan_compulsory.compulsory_number,
                                        loan_compulsory.product_name,
                                        loan_compulsory.saving_amount,
                                        loan_compulsory.charge_option,
                                        loan_compulsory.principles,
                                        loan_compulsory.interest_rate,
                                        loan_compulsory.cash_withdrawal,
                                        branches.title as branch_title
                                       ');



        $this->crud->addFilter([ // select2_ajax filter
            'name' => 'center_leader_id',
            'type' => 'select2_ajax',
            'label'=> 'Center',
            'placeholder' => 'Pick a center'
        ],
            url('api/center-option'), // the ajax route
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'center_leader_id', $value);
            });


        $this->crud->addFilter([ // select2_ajax filter
            'name' => 'compulsory_number',
            'type' => 'select2_ajax',
            'label'=> _t("Saving ID"),
            'placeholder' => 'Pick a Saving ID'
        ],
            url('api/saving-option'), // the ajax route
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'compulsory_number', $value);
            });

            $this->crud->addFilter([ // simple filter
                'type' => 'text',
                'name' => 'client_id',
                'label'=> _t("Client Name")
            ],
            false,
                function($value) { // if the filter is active
                    $this->crud->addClause('join', 'clients', 'loan_compulsory.client_id', 'clients.id');
                    $this->crud->addClause('where', 'clients.name', 'LIKE', "%$value%");
                    $this->crud->addClause('orWhere', 'name_other', 'LIKE', '%'.$value.'%');
                }
            );
        // $this->crud->addFilter([ // select2_ajax filter
        //     'name' => 'client_id',
        //     'type' => 'select2_ajax',
        //     'label'=> _t("Client Name"),
        //     'placeholder' => 'Pick a Client'
        // ],
        //     url('api/client-option'), // the ajax route
        //     function($value) { // if the filter is active
        //         $this->crud->addClause('where', 'loan_compulsory.client_id', $value);
        //     });


        $this->crud->addFilter([ // select2_ajax filter
            'name' => 'branch_id',
            'type' => 'select2_ajax',
            'label'=> _t("Branch Name"),
            'placeholder' => 'Pick a Branch'
        ],
            url('api/branch-option'), // the ajax route
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'loans.branch_id', $value);

            });

//        $this->crud->addClause('selectRaw', '
//
//                                        loans.branch_id,
//                                        loans.id,
//                                        loans.center_leader_id,
//                                        loans.loan_officer_id,
//                                        loans.client_id,
//                                        loan_compulsory.compulsory_number,
//                                        loan_compulsory.client_id,
//                                        loan_compulsory.product_name,
//                                        loan_compulsory.saving_amount,
//                                        loan_compulsory.charge_option,
//                                        loan_compulsory.interest_rate,
//                                        loan_compulsory.interest_rate,
//                                        loan_compulsory.principles,
//
//                                       ');


//        $this->crud->addFilter([ // simple filter
//            'type' => 'text',
//            'name' => 'branch_id',
//            'label' => _t("branch_id")
//        ],
//            false,
//            function ($value) { // if the filter is active
//                $this->crud->addClause('where', function ($q) use ($value) {
//                    return $q->orWhere('branch_id', 'LIKE', "%{$value}%");
//                });
//            });



        $this->crud->addColumn([
            'name' => 'compulsory_number',
            'label' => 'Account No',
//            'type' => 'closure',
//            'function' => function($entry) {
//                 $product = CompulsoryProduct::find($entry->compulsory_id);
//                 return optional($product)->code;
//            }
        ]);


        $this->crud->addColumn([
            'name' => 'client_number',
            'label' => 'Client ID',
            'type' => 'closure',
            'function' => function($entry) {
                $client = Client::where('id',optional($entry)->client_id)->first();
                return optional($client)->client_number;
            }
        ]);
        $this->crud->addColumn([
            'name' => 'client_en',
            'label' => 'Client Name(En)',
            'type' => 'closure',
            'function' => function($entry) {
                $client = Client::where('id',optional($entry)->client_id)->first();
                return optional($client)->name_other;
            }
        ]);

        $this->crud->addColumn([
            'name' => 'client_mm',
            'label' => 'Client Name(MM)',
            'type' => 'closure',
            'function' => function($entry) {
                $client = Client::where('id',optional($entry)->client_id)->first();
                return optional($client)->name_other;
            }
        ]);


        $this->crud->addColumn([
            'name' => 'branch_id',
            'label' => 'Branch',
            'type' => 'closure',
            'function' => function($entry) {
                $branch = Branch::where('id',optional($entry)->branch_id)->first();
                return optional($branch)->title;
            }
        ]);

       $this->crud->addColumn([
            'name' => 'product_name',
            'label' => _t('Name'),
        ]);

        $this->crud->addColumn([
            'name' => 'saving_amount',
            'label' => _t('Saving Amount'),
        ]);

        $this->crud->addColumn([
            'name' => 'charge_option',
            'label' => 'Charge Option',
            'type' => 'closure',
            'function' => function($entry) {

                $charge_option= array(
                    '1' => 'Fixed Amount',
                    '2' => 'Of Loan amount',
                    /*'3' => 'Of Principle amount',
                    '4' => 'Of Interest amount',
                    '5' => 'Of Principle + Interest amount',
                    '6' => 'Of Remaining Balance',*/
                );

                return  $charge_option[optional($entry)->charge_option] ;
            }
        ]);


        $this->crud->addColumn([
            'name' => 'interest_rate',
            'label' => _t('Interest Rate'),
        ]);

        $this->crud->addColumn([
            'name' => 'principles',
            'label' => _t('Deposits'),
        ]);
        $this->crud->addColumn([
            'name' => 'interests',
            'label' => _t('Interests'),
        ]);

        $this->crud->addColumn([
            'name' => 'cash_withdrawal',
            'label' => _t('Cash Withdrawal'),
        ]);
        $this->crud->addColumn([
            'name' => 'available_balance',
            'label' => _t('Available Balance'),
        ]);
        $this->crud->addColumn([
            'label' => _t("Center"), // Table column heading
            'type' => "select",
            'name' => 'center_leader_id', // the column that contains the ID of that connected entity;
            'entity' => 'center_leader',  // the method that defines the relationship in your Model
            'attribute' => "title",   // foreign key attribute that is shown to user
            'model' => "App\\Models\\CompulsorySavingList", // foreign key model
        ]);


        $this->crud->addColumn([
            'name' => 'compulsory_status',
            'label' => _t('Status'),
            'type' => 'closure',
            'function' => function($entry) {

                if ($entry->compulsory_status=="Pending"){
                    return '<button class="btn btn-warning btn-xs " style="width: 100px">'.$entry->compulsory_status.'</button>';
                }
                elseif ($entry->compulsory_status=="Active"){
                    return '<button class="btn btn-info btn-xs" style="width: 100px">'.$entry->compulsory_status.'</button>';
                }
                elseif ($entry->compulsory_status=="Completed"){
                    return '<button class="btn btn-success btn-xs" style="width: 100px">'.$entry->compulsory_status.'</button>';
                }

            }

        ]);



        // add asterisk for fields that are required in CompulsorySavingListRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->setPermissions();


    }


    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'compulsory-saving-list';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }

        // Allow create access


        /*
        if (_can2($this,'create-'.$fname)) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if (_can2($this,'update-'.$fname)) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if (_can2($this,'delete-'.$fname)) {
            $this->crud->allowAccess('delete');
        }


        if (_can2($this,'clone-'.$fname)) {
            $this->crud->allowAccess('clone');
        }

        */

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function saving_Options(Request $request) {
        $term = $request->input('term');
        $options = CompulsorySavingList::where('compulsory_number', 'like', '%'.$term.'%')
            ->get()->pluck('compulsory_number', 'compulsory_number');
        return $options;
    }

    public function branch_Options(Request $request)
    {
        $rows = Loan2::selectRaw('loans.id, branches.title')
            ->leftJoin('branches','branches.id','=','loans.branch_id')->get();
        return $rows;


    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
