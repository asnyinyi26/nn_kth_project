<?php

namespace App\Http\Controllers\admin;

use App\Models\LoanProduct;
use App\Models\PaidDisbursement;
use Illuminate\Http\Request;
use Backpack\CRUD\CrudPanel;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Support\Facades\DB;

/**
 * Class LoanOutstandingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class LoanOutstandingReportController extends CrudController
{
    public function setup()
    {
        // $param = request()->param;
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        $this->crud->setModel('App\Models\LoanOutstanding');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/report/loan-outstanding');
        $this->crud->setEntityNameStrings('Loan Outstanding Report', 'Loan Outstanding Report');
        $this->crud->setListView('partials.reports.loan.loan-report');

//        $this->crud->denyAccess(['update']);
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        // $this->crud->addFilter([ // select2_ajax filter
        //     'name' => 'client_number',
        //     'type' => 'select2_ajax',
        //     'label'=> 'Client ID',
        // ],
        //     url('api/client-id-option'), // the ajax route
        //     function($value) { // if the filter is active
        //         $this->crud->addClause('where', 'client_id', $value);
        // });

        $this->crud->addFilter([ // simple filter
            'type' => 'text',
            'name' => 'disbursement_number',
            'label'=> 'Loan Number'
        ],
            false,
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'disbursement_number', $value);
            }
        );

    //    $this->crud->addFilter([
    //         'name' => 'name',
    //         'type' => 'select2_ajax',
    //         'label'=> 'Client Name',
    //     ],
    //         url('api/client-option'),
    //         function($value) {
    //             $this->crud->addClause('whereHas', 'client_name', function($query) use($value) {
    //                     $query->where('name', 'LIKE', '%'.$value.'%');
    //             });
    //         }
    //     );


        /*

        $this->crud->addFilter([ // select2_ajax filter
            'name' => 'client_id',
            'type' => 'select2_ajax',
            'label'=> 'Client',
            'placeholder' => 'Pick a Client'
        ],
        url('api/client-option'), // the ajax route
        function($value) { // if the filter is active
            $this->crud->addClause('where', 'client_id', $value);
        });

        */


        $this->crud->addFilter([ // simple filter
            'type' => 'text',
            'name' => 'client_id',
            'label'=> 'Client'
        ],
            false,
            function($value) { // if the filter is active
                $this->crud->addClause('join', 'clients', 'client_id', 'clients.id');
                $this->crud->addClause('where', 'clients.name', 'LIKE', "%$value%");
                $this->crud->addClause('orWhere', 'clients.client_number', 'LIKE', "%$value%");
                $this->crud->addClause('orWhere', 'name_other', 'LIKE', '%'.$value.'%');
            }
        );

        if(companyReportPart() == "company.moeyan"){
            $this->crud->addFilter([ // daterange filter
                'type' => 'date_range',
                'name' => 'from_to',
                'label'=> 'Date'
            ],
                false,
                function($value) { // if the filter is active, apply these constraints
                    $dates = json_decode($value);
            });
        }else{
            $this->crud->addFilter([ // daterange filter
                'type' => 'date_range',
                'name' => 'from_to',
                'label'=> 'Date'
            ],
                false,
                function($value) { // if the filter is active, apply these constraints
                    $dates = json_decode($value);
                    $this->crud->addClause('join', 'paid_disbursements','contract_id', 'loans.id');
                    $this->crud->addClause('where', 'paid_disbursement_date', '>=', $dates->from);
                    $this->crud->addClause('where', 'paid_disbursement_date', '<=', $dates->to . ' 23:59:59');
            });
        }

        $this->crud->addFilter([ // select2_ajax filter
            'name' => 'loan_officer_id',
            'type' => 'select2_ajax',
            'label'=> 'Loan officer',
            'placeholder' => 'Pick a Loan officer'
        ],
            url('api/loan-officer-option'), // the ajax route
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'loan_officer_id', $value);
        });

        $this->crud->addFilter([ // select2_ajax filter
            'name' => 'nrc_number',
            'type' => 'text',
            'label'=> 'NRC Number',
        ],
        url('api/client-option'), // the ajax route
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'client_id', $value);
        });

        $this->crud->addFilter([ // Branch select2_ajax filter
            'name' => 'branch_id',
            'type' => 'select2_ajax',
            'label'=> 'Branch',
            'placeholder' => 'Select Branch'
        ],
        url('/api/branch-option'), // the ajax route
        function($value) { // if the filter is active
            $this->crud->addClause('whereHas', 'branch_name', function($query) use($value) {
                $query->where('loans.branch_id', $value);
            });
        });

        $this->crud->addFilter([ // Center select2_ajax filter
            'name' => 'center_id',
            'type' => 'select2_ajax',
            'label'=> 'Center',
            'placeholder' => 'Select Center'
        ],
        url('/api/center-option'), // the ajax route
        function($value) { // if the filter is active
            $this->crud->addClause('whereHas', 'center_name', function($query) use($value) {
                $query->where('center_leader_id', $value);
            });
        });

        $this->crud->addFilter([ // select2_ajax filter
            'name' => 'loan_production_id',
            'type' => 'select2_ajax',
            'label'=> 'Loan Type',
            'placeholder' => 'Pick a Loan Product'
        ],
            url('api/loan-product-option'), // the ajax route
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'loan_production_id', $value);
            });



        $this->crud->addColumn([
            'label' => _t("disburse date"), // Table column heading
            'name' => 'disburse_date',
            'type' => 'closure',
            'function' => function ($entry) {
                $disburse = PaidDisbursement::where('contract_id',$entry->id)->first();
                return optional($disburse)->paid_disbursement_date;
            }
        ]);



        $this->crud->addColumn([
            'name' => 'client_number',
            'label' => 'Client ID',
            'type' => 'closure',
            'function' => function ($entry) {
                $client_id = $entry->client_id;
                return optional(\App\Models\Client::find($client_id))->client_number;
            }
        ]);






        $this->crud->addColumn([
            'name' => 'disbursement_number',
            'label' => 'Loan Number',
        ]);


        $this->crud->addColumn([
            'label' => _t("Name"), // Table column heading
            'type' => 'closure',
            'name' => 'name', // the column that contains the ID of that connected entity;
            'function' => function ($entry) {
                $client_id = $entry->client_id;
                return optional(\App\Models\Client::find($client_id))->name;
            }
        ]);

        $this->crud->addColumn([
            'label' => _t("Other Name"), // Table column heading
            'type' => "closure",
            'name' => 'name_other', // the column that contains the ID of that connected entity;
            'function' => function ($entry) {
                $client_id = $entry->client_id;
                return optional(\App\Models\Client::find($client_id))->name_other;
            }
        ]);
        $this->crud->addColumn([
            'name' => 'nrc_number',
            'label' => 'Nrc Number',
            'type' => 'closure',
            'function' => function ($entry) {
                $client_id = $entry->client_id;
                return optional(\App\Models\Client::find($client_id))->nrc_number;
            }
        ]);


        $this->crud->addColumn([
            'label' => _t("Branch"), // Table column heading
            'type' => "select",
            'name' => 'branch_id', // the column that contains the ID of that connected entity;
            'entity' => 'branch_name', // the method that defines the relationship in your Model
            'attribute' => "title", // foreign key attribute that is shown to user
            'model' => "App\\Models\\Loan", // foreign key model
        ]);



        $this->crud->addColumn([
            'label' => _t("Center"), // Table column heading
            'type' => "select",
            'name' => 'center_leader_id', // the column that contains the ID of that connected entity;
            'entity' => 'center_name',  // the method that defines the relationship in your Model
            'attribute' => "title",   // foreign key attribute that is shown to user
            'model' => "App\\Models\\Loan", // foreign key model
        ]);

        $this->crud->addColumn([
            'label' => _t("Co Name"), // Table column heading
            'type' => "select",
            'name' => 'loan_officer_id', // the column that contains the ID of that connected entity
            'entity' => 'officer_name', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\\User", // foreign key model
        ]);
        $this->crud->addColumn([
            'name' => 'loan_production_id',
            'label' => 'Loan Type',
            'type' => 'closure',
            'function' => function ($entry) {
                $loan_pro = LoanProduct::find($entry->loan_production_id);
                return optional($loan_pro)->name;
            }
        ]);

        $this->crud->addColumn([
            'label' => _t("Loan Amount"), // Table column heading
            'name' => 'loan_amount',
            'type' => 'number'
        ]);

        if(\companyReportPart() == "company.quicken"){

            $this->crud->addColumn([
                'label' => _t("Total Interest"),
                'name' => 'total_interest',
                'type' => 'closure',
                'function' => function ($entry) {
                    $total_interest = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->sum('interest_s');
                    return numb_format($total_interest, 0);
                }
            ]);
            
            $this->crud->addColumn([
                'label' => _t("Principle Target"), // Table column heading
                'name' => 'principle_repayment',
                'type' => 'number'
            ]);

            $this->crud->addColumn([
                'label' => _t("Interest Target"), // Table column heading
                'name' => 'interest_repayment',
                'type' => 'number'
            ]);

            $this->crud->addColumn([
                'label' => _t("Principle Repay"),
                'name' => 'principle_p',
                'type' => 'closure',
                'function' => function ($entry) {
                    $principal_p = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->sum('principal_p');
                    return numb_format($principal_p, 0);
                }
            ]);

            $this->crud->addColumn([
                'label' => _t("Interest Repay"),
                'name' => 'interest_p',
                'type' => 'closure',
                'function' => function ($entry) {
                    $interest_p = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->sum('interest_p');
                    return numb_format($interest_p, 0);
                }
            ]);

            $this->crud->addColumn([
                'label' => _t("Principle Outstanding"), // Table column heading
                'name' => 'principle_outstanding',
                'type' => 'closure',
                'function' => function ($entry) {
                    $principal_p = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->sum('principal_p');
                    $principle_out = optional($entry)->loan_amount - $principal_p;
                    return numb_format($principle_out, 0);
                }
            ]);

            $this->crud->addColumn([
                'label' => _t("Interest Outstanding"), // Table column heading
                'name' => 'interest_outstanding',
                'type' => 'closure',
                'function' => function ($entry) {
                    $loan_c = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->select(DB::raw('SUM(interest_s) As total_interest, SUM(interest_p) As interest_p'))->first();
                    $interest_out = $loan_c->total_interest - $loan_c->interest_p;
                    return numb_format($interest_out, 0);
                }
            ]);
        }
        else if(\companyReportPart() == "company.moeyan"){
            $this->crud->addColumn([
                'label' => _t("Principle Repay"),
                'name' => 'principle_p',
                'type' => 'closure',
                'function' => function ($entry) {
                    isset($_REQUEST['from_to']) ? $from_to = $_REQUEST['from_to'] : $from_to = null;
                    if($from_to){
                        $dates = json_decode($from_to);
                        $principal_p = \App\Models\LoanCalculate::where([['disbursement_id' , $entry->id], ['date_s', '<=',$dates->to. ' 23:59:59']])->sum('principal_p');
                    }else{
                        $principal_p = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->sum('principal_p');
                    }
                    return numb_format($principal_p, 0);
                }
            ]);

            $this->crud->addColumn([
                'label' => _t("Interest Repay"),
                'name' => 'interest_p',
                'type' => 'closure',
                'function' => function ($entry) {
                    isset($_REQUEST['from_to']) ? $from_to = $_REQUEST['from_to'] : $from_to = null;
                    if($from_to){
                        $dates = json_decode($from_to);
                        $interest_p = \App\Models\LoanCalculate::where([['disbursement_id' , $entry->id], ['date_s', '<=',$dates->to. ' 23:59:59']])->sum('interest_p');
                    }else{
                        $interest_p = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->sum('interest_p');
                    }
                    return numb_format($interest_p, 0);
                }
            ]);

            $this->crud->addColumn([
                'label' => _t("Principal Outstanding"), // Table column heading
                'name' => 'principal_outstanding',
                'type' => 'closure',
                'function' => function ($entry) {
                    isset($_REQUEST['from_to']) ? $from_to = $_REQUEST['from_to'] : $from_to = null;
                    if($from_to){
                        $dates = json_decode($from_to);
                        $principal_p = \App\Models\LoanCalculate::where([['disbursement_id' , $entry->id], ['date_s', '<=',$dates->to. ' 23:59:59']])->sum('principal_p');
                        $pay_his = \App\Models\PaymentHistory::where([['loan_id' , $entry->id], ['payment_date', '<=',$dates->to. ' 23:59:59']])->select('owed_balance')->latest('id')->first();
                    }else{
                        $principal_p = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->sum('principal_p');
                        $pay_his = \App\Models\PaymentHistory::where('loan_id' , $entry->id)->select('owed_balance')->latest('id')->first();
                    }
                    $principal_out = optional($entry)->loan_amount - ($principal_p + optional($pay_his)->owed_balance);
                    return numb_format($principal_out, 0);
                }
            ]);

            $this->crud->addColumn([
                'label' => _t("Interest Outstanding"), // Table column heading
                'name' => 'interest_outstanding',
                'type' => 'closure',
                'function' => function ($entry) {
                    isset($_REQUEST['from_to']) ? $from_to = $_REQUEST['from_to'] : $from_to = null;
                    $total_interest = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->sum('interest_s');
                    if($from_to){
                        $dates = json_decode($from_to);
                        $interest_p = \App\Models\LoanCalculate::where([['disbursement_id' , $entry->id], ['date_s', '<=',$dates->to. ' 23:59:59']])->sum('interest_p');
                    }else{
                        $interest_p = \App\Models\LoanCalculate::where('disbursement_id' , $entry->id)->sum('interest_p');
                    }
                    $interest_out = $total_interest - $interest_p;
                    return numb_format($interest_out, 0);
                }
            ]);
        }
        else{
            $this->crud->addColumn([
                'label' => _t("Principle Repay"), // Table column heading
                'name' => 'principle_repayment',
                'type' => 'number'
            ]);

            $this->crud->addColumn([
                'label' => _t("Interest Repay"), // Table column heading
                'name' => 'interest_repayment',
                'type' => 'number'
            ]);

            $this->crud->addColumn([
                'label' => _t("Principle Outstanding"), // Table column heading
                'name' => 'principle_receivable',
                'type' => 'number'
            ]);

            $this->crud->addColumn([
                'label' => _t("Interest Outstanding"), // Table column heading
                'name' => 'interest_receivable',
                'type' => 'number'
            ]);
        }

        $this->crud->addColumn([
            'name' => 'total_interest',
            'label' => _t("Total Interest"), // Table column heading
            'type' => 'closure',
            'function' => function ($entry) {
                return optional($entry)->interest_repayment + optional($entry)->interest_receivable;
            }
        ]);

        $this->crud->addColumn([
            'label' => _t("Cycle"), // Table column heading
            'name' => 'cycle',
            'type' => 'closure',
            'function' => function ($entry) {
                //$cycle = \App\Models\Loan::where('client_id',$entry->client_id)->count('id');
                $cycle = \App\Models\LoanCycle::getLoanCycle($entry->client_id,$entry->loan_production_id,$entry->id);
                return $cycle??1;
            }
        ]);

        //$this->crud->enableDetailsRow();
        // $this->crud->allowAccess('disburse_details_row');
        $this->crud->denyAccess(['create', 'update', 'delete', 'clone']);

        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        $this->crud->setDefaultPageLength(10);
        $this->crud->removeAllButtons();
        $this->setPermissions();
    }

    public function setPermissions()
    {
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'loan-outstanding';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }
    }
    public function excel(Request $request)
    {


    }
}
