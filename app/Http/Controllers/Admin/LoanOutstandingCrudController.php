<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\Branch;
use App\Models\CompulsorySavingTransaction;
use App\Models\GeneralJournal;
use App\Models\GeneralJournalDetail;
use App\Models\Loan;
use App\Models\Loan2;
use App\Models\LoanCalculate;
use App\Models\LoanDeposit;
use App\Models\LoanPayment;
use App\Models\PaidDisbursement;
use App\Models\PaymentHistory;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LoanOutstandingRequest as StoreRequest;
use App\Http\Requests\LoanOutstandingRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class LoanOutstandingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class LoanOutstandingCrudController extends CrudController
{

    public function paymentPop(Request $request){

        //dd($request->all());
        $loan_id = $request->loan_id;
        $row = Loan::find($loan_id);
        //dd($row);
        return view ('partials.loan-payment.loan-payment-pop',['row'=>$row]);
    }

    public function showDetailsRow($id)
    {
        //$m_ = ProductSerial::where('product_id', $id)->get();
//        dd($m_);
        $row = Loan::find($id);
        // dd($row);
        return view('partials.loan_disbursement.loan_outstanding_payment', ['row' => $row]);
    }


    public function updateLoanStatus(Request $request)
    {
        $id = $request->id;
        $m = Loan::find($id);
        $m->status_note_approve = $request->status_note_approve;
        $m->status_note_date_approve = $request->status_note_date_approve;
        $m->disbursement_status = $request->disbursement_status;

        $m->save();
    }

    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\LoanOutstanding');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/loanoutstanding');
        $this->crud->setEntityNameStrings('loan activated', 'loan activated');
        $this->crud->disableResponsiveTable();
        $this->crud->setListView('partials.loan_disbursement.payment-loan');
        $this->crud->enableExportButtons();
        $this->crud->orderBy('loans.id','DESC');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */




        include('loan_inc.php');
        $this->crud->addClause('selectRaw', 'loans.*');
        $this->crud->enableDetailsRow();
        $this->crud->allowAccess('disburse_details_row');
        $this->crud->denyAccess(['create', 'update', 'delete', 'clone']);
        //$this->crud->addButtonFromModelFunction('line', 'addButtonCustom', 'addButtonCustom', 'beginning');
        // add asterisk for fields that are required in LoanOutstandingRequest
        $this->crud->addButtonFromModelFunction('line', 'addButtonCustom', 'addButtonCustom', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->setPermissions();

    }


    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'loan-outstanding';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }

        // Allow create access


        /*
        if (_can2($this,'create-'.$fname)) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if (_can2($this,'update-'.$fname)) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if (_can2($this,'delete-'.$fname)) {
            $this->crud->allowAccess('delete');
        }


        if (_can2($this,'clone-'.$fname)) {
            $this->crud->allowAccess('clone');
        }

        */

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }


    public function clientOptions(Request $request) {

        $term = $request->input('term');
        $options = Client::where('name', 'like', '%'.$term.'%')->get()->pluck('name', 'id');
        return $options;
    }
    public function view_payment(Request $request){
        $schedule_id = $request->schedule_id;

        $payments = PaymentHistory::where('schedule_id',$schedule_id)->get();
        //dd($payment);
        return view('partials.loan-payment.view-repayment',['payments'=>$payments]);
    }

    public function cancel_activated(Request $request){
       

        $id = $request->id;
        $m = Loan2::find($id);
        if($m != null){
            $m->disbursement_status = 'Canceled';
            $m->cancel_date = date('Y-m-d');
            $m->remark = $request->remark;
            if ($m->save()){
                $paid_disbursement=PaidDisbursement::where('contract_id',$id)->first();
                if ($paid_disbursement != null){
                    GeneralJournal::where('tran_id',$paid_disbursement->id)->where('tran_type','loan-disbursement')->delete();
                    GeneralJournalDetail::where('tran_id',$paid_disbursement->id)->where('tran_type','loan-disbursement')->delete();
                    CompulsorySavingTransaction::where('tran_id',$paid_disbursement->id)->where('train_type_ref','disbursement')->delete();
                }
                PaidDisbursement::where('contract_id',$id)->delete();
                $paid_dis_deposit=LoanDeposit::where('applicant_number_id',$id)->first();
                if ($paid_dis_deposit != null){
                    $m->deposit_paid = 'No';
                    $m->save();
                    GeneralJournal::where('tran_id',$paid_dis_deposit->id)->where('tran_type','loan-deposit')->delete();
                    GeneralJournalDetail::where('tran_id',$paid_dis_deposit->id)->where('tran_type','loan-deposit')->delete();
                    CompulsorySavingTransaction::where('tran_id',$paid_dis_deposit->id)->where('train_type','deposit')->delete();
                }
                LoanDeposit::where('applicant_number_id',$id)->delete();
                $loan_payment=LoanPayment::where('disbursement_id',$id)->get();
                foreach ($loan_payment as $p){
                    GeneralJournal::where('tran_id',$p->id)->where('tran_type','payment')->delete();
                    GeneralJournalDetail::where('tran_id',$p->id)->where('tran_type','payment')->delete();
                    CompulsorySavingTransaction::where('tran_id',$p->id)->where('train_type_ref','saving')->delete();
                }
                LoanPayment::where('disbursement_id',$id)->delete();


                LoanCalculate::where('disbursement_id',$id)
                    ->update([
                       'principal_p' => 0,
                        'interest_p' => 0,
                        'penalty_p' => 0,
                        'service_charge_p' => 0,
                        'total_p' => 0,
                        'balance_p' => 0,
                        'owed_balance_p' => 0,
                        'payment_status' => 'pending',
                        'over_days_p' => 0,
                        'principle_pd' => 0,
                        'interest_pd' => 0,
                        'total_pd' => 0,
                        'penalty_pd' => 0,
                        'payment_pd' => 0,
                        'service_pd' => 0,
                        'compulsory_pd' => 0,
                        'compulsory_p' => 0,
                        'count_payment' => 0,
                        'exact_interest' => 0,
                        'charge_schedule' => 0,
                        'compulsory_schedule' => 0,
                        'total_schedule' => 0,
                        'balance_schedule' => 0,
                        'penalty_schedule' => 0,
                    ]);



            }
        }

        
         return redirect()->back();

    }

}
