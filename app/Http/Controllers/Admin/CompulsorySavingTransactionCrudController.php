<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CompulsorySavingTransactionRequest as StoreRequest;
use App\Http\Requests\CompulsorySavingTransactionRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CompulsorySavingTransactionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CompulsorySavingTransactionCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\CompulsorySavingTransaction');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/compulsory-saving-transaction');
        $this->crud->setEntityNameStrings('compulsorysavingtransaction', 'compulsory_saving_transactions');
        $this->crud->disableResponsiveTable();
        $this->crud->setListView('partials.loan_disbursement.saving_transaction_total');
        $this->crud->enableExportButtons();


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();


        $this->crud->addColumn([
            'label' => _t("Customer"),
            'type' => "select",
            'name' => 'customer_id',
            'entity' => 'customer',
            'attribute' => "name",
            'model' => "App\\Models\\CompulsorySavingTransaction",
        ]);

        $this->crud->addColumn([
            'name' => 'train_type',
            'label' => _t('Tran Type'),
        ]);

        $this->crud->addColumn([
            'name' => 'amount',
            'label' => _t('Amount'),
        ]);

        $this->crud->addColumn([
            'name' => 'tran_date',
            'label' => _t('Date'),
            'type' => 'date',
        ]);


        // add asterisk for fields that are required in CompulsorySavingTransactionRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->setPermissions();
    }

    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'compulsory-saving-transaction';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }
/*
        // Allow create access
        if (_can2($this,'create-'.$fname)) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if (_can2($this,'update-'.$fname)) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if (_can2($this,'delete-'.$fname)) {
            $this->crud->allowAccess('delete');
        }

*/


//        if (_can2($this,'clone-'.$fname)) {
//            $this->crud->allowAccess('clone');
//        }

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
