<?php

namespace App\Http\Controllers\Admin;

use App\Models\AccountChart;
use App\Models\Branch;
use App\Models\Expense;
use App\Models\GeneralJournalDetail;
use App\Models\ReportAccounting;
use App\Models\JournalProfit;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ReportAccountingRequest as StoreRequest;
use App\Http\Requests\ReportAccountingRequest as UpdateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class ReportAccountingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ReportAccountingCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ReportAccounting');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/report-accounting');
        $this->crud->setEntityNameStrings('report-accounting', 'report_accounting');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addField([
            'name' => 'search_type',
            'label' => _t('Search By'),
            'type' => 'select_from_array',
            'options' => [ 'normal' => 'Normal', 'range' => 'Account Range'],
            'fake' => true,
            'attributes' => [
                'id' => 'search_range'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
        ]);

        $this->crud->addField([
            'label' => "Account",
            'type' => "select2_from_ajax_multiple",
            'name' => 'acc_chart_id',
            'entity' => 'acc_chart',
            'attribute' => "name",
            'model' => AccountChart::class,
            'data_source' => url("api/acc-chart"),
            'placeholder' => "Select a Account",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5 acc_normal'
            ]
        ]);

        $this->crud->addField([
            'name' => 'acc_chart_range',
            'label' => _t('account_range'),
            'type' => 'account',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-9'
            ],
        ]);

        $this->crud->addField([   // date_picker
            'name' => 'month',
            'type' => 'date_picker',
            'label' => 'Month',
            'default' => date('Y-m-d') ,
            // optional:
            'date_picker_options' => [
                'todayBtn' => true,
                'format' => 'yyyy-mm'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
        ]);

        $this->crud->addField(
            [ // select_from_array
                'name' => 'show_zero',
                'label' => "Show Zero",
                'type' => 'select_from_array',
                'options' => [
                    'No'=>'No',
                    'Yes'=>'Yes'
                ],
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-2'
                ]
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ]
        );
        $this->crud->addField([
            'label' => "Branch",
            'type' => "select2_from_ajax_multiple",
            'name' => 'branch_id',
            'entity' => 'category',
            'attribute' => "title",
            'model' => Branch::class,
            'data_source' => url("api/get-branch"),
            'placeholder' => "Select branch",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ]
        ]);
        $this->crud->addField([
            'name' => 'date_date_range', // a unique name for this field
            'start_name' => 'start_date', // the db column that holds the start_date
            'end_name' => 'end_date', // the db column that holds the end_date
            'label' => 'Select Date Range',
            'type' => 'date_range',
            // OPTIONALS
            'start_default' => Carbon::now()->startOfMonth()->format('Y-m-d'), // default value for start_date
            'end_default' => date('Y-m-d'), // default value for end_date
            'date_range_options' => [ // options sent to daterangepicker.js
                //'timePicker' => true,
                'locale' => ['format' => 'DD/MM/YYYY']
//                'locale' => ['format' => 'DD/MM/YYYY HH:mm']
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);




        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/account/main-script'
        ]);

        $this->crud->setCreateView('custom.create_report_account');


        // add asterisk for fields that are required in ReportAccountingRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->setPermissions();

    }


    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'report-accounting';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if (_can2($this,'create-'.$fname)) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if (_can2($this,'update-'.$fname)) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if (_can2($this,'delete-'.$fname)) {
            $this->crud->allowAccess('delete');
        }


        if (_can2($this,'clone-'.$fname)) {
            $this->crud->allowAccess('clone');
        }

    }


    public function index()
    {
        return redirect('admin/report-accounting/create');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function cash_transaction(Request $request){
        /*$start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;
        $show_zero = $request->show_zero;
        $branch_id=$request->branch_id;
        //dd($start_date);
        $cash= GeneralJournalDetail::selectRaw('j_detail_date,dr,cr,journal_id,acc_chart_id,description,branch_id')
        ->where('section_id', 10)
        ->where(function ($query) use ($acc_chart_id){
            if ($acc_chart_id != null) {
                if (is_array($acc_chart_id)) {
                    if (count($acc_chart_id) > 0) {
                        $query->whereIn('acc_chart_id', $acc_chart_id);
                    }
                }
            }
        })

        ->where(function ($q) use ($branch_id){
            if($branch_id != null || is_array($branch_id)){
                return $q->whereIn('branch_id',$branch_id);
            }
        })
            ->where(function ($q) use ($start_date,$end_date){
                if ($start_date && $end_date){
                    $q->whereBetween('j_detail_date',[$start_date,$end_date]);
                }
            })->get();

        return view('partials.reports.account.cash_transaction',['cash'=>$cash]);*/
//        return view('partials.reports.account.cash_transaction');

        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $branch_id = $request->branch_id;
        
        $arr_acc = [];
        $arr_begin = [];
        $arr_leger = [];
        
        $acc_chart_id = [];
        $acc_chart_id = $this->getAccounts($request);
        if($branch_id != null) {
            $beg_leger = ReportAccounting::getBeginCashTransaction($start_date, $end_date, $branch_id, $acc_chart_id);
            $gen_leger = ReportAccounting::getCashTransaction($start_date, $end_date, $branch_id, $acc_chart_id);
            if($beg_leger != null){
                foreach ($beg_leger as $b) {
                    $arr_acc[$b->acc_chart_id] = $b->acc_chart_id;
                    $arr_begin[$b->acc_chart_id] = $b->amt??0;
                }
            }

            if($gen_leger != null){

                foreach ($gen_leger as $k=>$b) {
                    $arr_acc[$b->acc_chart_id] = $b->acc_chart_id;
                    $arr_leger[$b->acc_chart_id][$k] = $b;
                }
            }



        }else{
            return 'Please select branch';
        }

        return view('partials.reports.account.cash_transaction',['start_date' => $start_date, 'end_date' => $end_date,
            'arr_acc'=>$arr_acc,'arr_begin'=>$arr_begin,'arr_leger'=>$arr_leger, 'acc_chart_id' => $acc_chart_id,'branches' =>$branch_id]);
    }

    public function accountList(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $show_zero = $request->show_zero;
        $branch_id = $request->branch_id;

        $acc_chart_id = [];
        $acc_chart_id = $this->getAccounts($request);
        $rows = AccountChart::where(function ($query) use ($acc_chart_id){
            if($acc_chart_id != null){
                if(is_array($acc_chart_id)){
                    if(count($acc_chart_id)>0){
                        $query->whereIn('id',$acc_chart_id);
                    }
                }
            }

        })->get();

        $getAccountBalAll = ReportAccounting::getAccountBalAll($acc_chart_id,$start_date,$end_date,[],false,$branch_id);
        $bals = [];
        $branches= [];
        if($getAccountBalAll != null){
            
            foreach ($getAccountBalAll as $r){
                $bals[$r->acc_chart_id][$r->branch_id] = ($r->t_dr??0) - ($r->t_cr??0) ;
//                $bals[$r->acc_chart_id] = ($r->t_dr??0) - ($r->t_cr??0) ;

                $branches[$r->branch_id] = $r->branch_id;
            }

        }
        
        return view('partials.reports.account.account-list',['rows'=>$rows,
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,
            'branches' => $branches
            ]);

    }

    public function trialBalance(Request $request){
        $branch_id = $request->branch_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = [];
        $acc_chart_id = $this->getAccounts($request);
        $rows = AccountChart::where(function ($query) use ($acc_chart_id){
            if($acc_chart_id != null){
                if(is_array($acc_chart_id)){
                    if(count($acc_chart_id)>0){
                        $query->whereIn('id',$acc_chart_id);
                    }
                }
            }
        })->get();

        $getAccountBalAll = ReportAccounting::getAccountBalAllB($branch_id,$acc_chart_id,$start_date,$end_date);

        $bals = [];
        $branches = [];
        //dd($getAccountBalAll);
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->acc_chart_id][$r->branch_id] = ($r->t_dr??0) - ($r->t_cr??0) ;
                $branches[$r->branch_id] = $r->branch_id;
            }

        }

        return view('partials.reports.account.trial-balance',['rows'=>$rows,
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,'branches'=>$branches]);

    }


    public function profitLoss(Request $request){
        //dd($request);
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $branch_id = $request->branch_id;

        $acc_chart_id = [];
        $acc_chart_id = $this->getAccounts($request);
        $getAccountBalAll = ReportAccounting::getAccountBalAll($acc_chart_id,$start_date,$end_date,[40,50,60,70,80],false,$branch_id);

        //dd($getAccountBalAll);
        $bals = [];
        $branches = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->section_id][$r->acc_chart_id][$r->branch_id] = ($r->t_dr??0) - ($r->t_cr??0) ;
//                $bals[$r->section_id][$r->acc_chart_id] = ($r->t_dr??0) - ($r->t_cr??0) ;

                $branches[$r->branch_id] = $r->branch_id;
            }
        }


        return view('partials.reports.account.profit-loss',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,
            'branches' => $branches
        ]);

    }

    public function balanceSheet(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $branch_id = $request->branch_id;

        $acc_chart_id = [];
        $acc_chart_id = $this->getAccounts($request);

        $getAccountBalAll = ReportAccounting::getAccountBalAll($acc_chart_id,$end_date,null,
            [10,12,14,16,18,20,22,24,26,30],false,$branch_id);

        $getRetainedEarningBegin = ReportAccounting::getRetainedEarning($start_date,null,true,$branch_id);
        $profit = ReportAccounting::getRetainedEarning($start_date,$end_date,false,$branch_id);

        $bals = [];
        $branchs = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                /*if(isset($bals[$r->section_id][$r->acc_chart_id]))
                $bals[$r->section_id][$r->acc_chart_id] = (($r->t_dr??0) - ($r->t_cr??0));
                else $bals[$r->section_id][$r->acc_chart_id] = 0;*/
                $bals[$r->section_id][$r->acc_chart_id][$r->branch_id] = (($r->t_dr??0) - ($r->t_cr??0));
                $branchs[$r->branch_id] = $r->branch_id;
            }
        }

        $returnEarningBeg = [];
        $arr_profit = [];

        if($getRetainedEarningBegin != null){
            foreach ($getRetainedEarningBegin as $e){
                $returnEarningBeg[$e->branch_id] = $e->bal??0;
            }
        }
        if($profit != null){
            foreach ($profit as $p){
                $arr_profit[$p->branch_id] = $p->bal;
            }
        }

        return view('partials.reports.account.balance-sheet',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,
            'retainedEarningBegin'=> $returnEarningBeg,
            'profit'=> $arr_profit,'branchs'=>$branchs
        ]);

    }

    public function transactionDetailByAccount(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        $getTransDetails = ReportAccounting::getTransactionDetail($acc_chart_id,$start_date,$end_date);


        return view('partials.reports.account.transaction-detail-by-acc',[
            'start_date'=>$start_date,'end_date'=>$end_date,
            'rows'=> $getTransDetails
        ]);

    }
    public function profitLossDetail(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        $getTransDetails = ReportAccounting::getTransactionDetail($acc_chart_id,$start_date,$end_date,[40,50,60,70,80]);

        $bals = [];
        if($getTransDetails != null){
            foreach ($getTransDetails as $r){
                $bals[$r->section_id][$r->acc_chart_id][$r->id] = $r;
            }

        }

        return view('partials.reports.account.profit-loss-detail',[
            'start_date'=>$start_date,'end_date'=>$end_date,
            'bals'=> $bals
        ]);

    }
    public function profitLossByJob(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        $getAccountBalAll = ReportAccounting::getAccountBalAllByJob($acc_chart_id,$start_date,$end_date,[40,50,60,70,80]);

        //dd($getAccountBalAll);
        $bals = [];
        $jobs = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->section_id][$r->acc_chart_id][$r->job_id>0?$r->job_id:'NA'] = ($r->t_dr??0) - ($r->t_cr??0) ;
                $jobs[$r->job_id>0?$r->job_id:'NA'] = $r->job_id;
            }

        }

        //dd($jobs,$bals);
        return view('partials.reports.account.profit-loss-by-job',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,'jobs'=>$jobs]);

    }
    public function profitLossByClass(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        $getAccountBalAll = ReportAccounting::getAccountBalAllByClass($acc_chart_id,$start_date,$end_date,[40,50,60,70,80]);

        //dd($getAccountBalAll);
        $bals = [];
        $jobs = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->section_id][$r->acc_chart_id][$r->class_id>0?$r->class_id:'NA'] = ($r->t_dr??0) - ($r->t_cr??0) ;
                $jobs[$r->class_id>0?$r->class_id:'NA'] = $r->class_id;
            }

        }
        //dd($jobs,$bals);
        return view('partials.reports.account.profit-loss-by-class',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,'jobs'=>$jobs]);

    }
    public function cashStatement(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;
        $branch_id = $request->branch_id;
        //dd("hello world");
        //dd($branch_id);

        $acc_chart_id = [];
        $acc_chart_id = $this->getAccounts($request);
        if($branch_id != null) {
            $cash_statement = ReportAccounting::getCashStatement($start_date, $end_date, false, $branch_id, $acc_chart_id);
            $type = [];
            $bal_type = [];
            $branch = [];
            if ($cash_statement != null) {
                foreach ($cash_statement as $r) {
                    $type[$r->tran_type ?? 'N/A'] = $r->tran_type ?? 'N/A';
                    //$branch[$r->branch_id??'N/A'] = $r->branch_id??'N/A'  ;
                    $bal_type[$r->branch_id][$r->acc_chart_id ?? 0][$r->tran_type ?? 'N/A'] = $r->bal ?? 0;
                }
            }
            return view('partials.reports.account.cash-statement',[
                'start_date'=>$start_date,'end_date'=>$end_date,'type' => $type,'bal_type'=>$bal_type,'branches'=>$branch_id,
                'acc_chart_id'=>$acc_chart_id
                ]);
        }else{
            return '';
        }
        //return  ($bal_type);
    }
    public function cashStatementDetail(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $branch_id = $request->branch_id;

        $acc_chart_id = [];
        $acc_chart_id = $this->getAccounts($request);

        if($branch_id != null) {
            $cash_statement = ReportAccounting::getCashStatementDetail($start_date, $end_date, false, $branch_id, $acc_chart_id);
            $bals = [];
            if ($cash_statement != null) {
                foreach ($cash_statement as $r) {
                    $bals[$r->branch_id][$r->section_id][$r->acc_chart_id][$r->id] = $r;
                }

            }
            return view('partials.reports.account.cash-statement-detail', [
                'start_date' => $start_date, 'end_date' => $end_date, 'bals' => $bals,'branches'=>$branch_id,
                'acc_chart_id' => $acc_chart_id
                ]);
        }else{
            return '';
        }

    }
    public function expense_pop($id){
        $row = Expense::find($id);
        if ($row != null) {
            return view('partials.reports.account.expense-list-pop', ['row' => $row]);
        } else {
            return 'No data';
        }
    }
    public function  general_leger(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $branch_id = $request->branch_id;

        $arr_acc = [];
        $arr_begin = [];
        $arr_leger = [];

        if($branch_id == null){
            $branch_id = 1;
        }

        $acc_chart_id = [];
        $acc_chart_id = $this->getAccounts($request);
        // if($branch_id != null) {
        $beg_leger = ReportAccounting::getBeginGeneralLeger($start_date, $end_date, $branch_id, $acc_chart_id);
        $gen_leger = ReportAccounting::getGeneralLeger($start_date, $end_date, $branch_id, $acc_chart_id);

        if($beg_leger != null){
            foreach ($beg_leger as $b) {
                $arr_acc[$b->acc_chart_id] = $b->acc_chart_id;
                $arr_begin[$b->acc_chart_id] = $b->amt??0;
            }
        }

            if($gen_leger != null){

            foreach ($gen_leger as $k=>$b) {
                $arr_acc[$b->acc_chart_id] = $b->acc_chart_id;
                $arr_leger[$b->acc_chart_id][$k] = $b;
            }
        }


        // }else{
        //     return 'Please select branch';
        // }

        return view('partials.reports.account.general-leger',['start_date' => $start_date, 'end_date' => $end_date,
            'arr_acc'=>$arr_acc,'arr_begin'=>$arr_begin,'arr_leger'=>$arr_leger, 'acc_chart_id' => $acc_chart_id,'branch_id'=>$branch_id]);
    }

    public function getAccounts($request){
        $search_type = $request->search_type;
        if($search_type == "range"){
            $acc_chart_id = [];
            $from_acc = $request->from_acc;
            $to_acc = $request->to_acc;
            $acc_charts = AccountChart::whereBetween('code', [$from_acc, $to_acc])->get();
            foreach($acc_charts as $acc_chart){
                array_push($acc_chart_id, $acc_chart->id);
            }
            return $acc_chart_id;
        }else{
            return $request->acc_chart_id;
        }
    }
}
