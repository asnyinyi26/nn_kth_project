<?php

namespace App\Http\Controllers\Admin;

use App\Models\AccountChart;
use App\Models\AccountChartExternal;
use App\Models\Branch;
use App\Models\FrdAccSetting;
use App\Models\ReportAccounting;
use App\Models\ReportSRD;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ReportAccountingRequest as StoreRequest;
use App\Http\Requests\ReportAccountingRequest as UpdateRequest;
use Illuminate\Http\Request;

/**
 * Class ReportAccountingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ReportSRDCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ReportSRD');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/report-account-external');
        $this->crud->setEntityNameStrings('Report External', 'Report External');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

      /*  $this->crud->addField([
            'label' => "Account",
            'type' => "select2_from_ajax_multiple",
            'name' => 'acc_chart_id',
            'entity' => 'acc_chart',
            'attribute' => "external_acc_code",
            'model' => AccountChartExternal::class,
            'data_source' => url("api/acc-chart-external"),
            'placeholder' => "Select a Account",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ]
        ]);*/

       /* $this->crud->addField([   // date_picker
            'name' => 'month',
            'type' => 'date_picker',
            'label' => 'Month',
            'default' => date('Y-m-d') ,
            // optional:
            'date_picker_options' => [
                'todayBtn' => true,
                'format' => 'yyyy-mm'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-5'
            ],
        ]);

        $this->crud->addField(
            [ // select_from_array
                'name' => 'show_zero',
                'label' => "Show Zero",
                'type' => 'select_from_array',
                'options' => [
                    'No'=>'No',
                    'Yes'=>'Yes'
                ],
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-2'
                ]
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ]
        );*/

        $this->crud->addField([
            'label' => "Branch",
            'type' => "select2_from_ajax_multiple",
            'name' => 'branch_id',
            // 'entity' => 'category',
            'attribute' => "title",
            'model' => Branch::class,
            'data_source' => url("api/get-branch"),
            'placeholder' => "Select branch",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);

        $this->crud->addField([   // date_picker
            'name' => 'start_date',
            'type' => 'date_picker2',
            'label' => 'Start Date',
//            'default' => date('Y-m-d') ,
            // optional:
            'date_picker_options' => [
                'todayBtn' => true,
//                'format' => 'yyyy-mm'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
        ]);


        $this->crud->addField([   // date_picker
            'name' => 'end_date',
            'type' => 'date_picker2',
            'label' => 'End Date',
//            'default' => date('Y-m-d') ,
            // optional:
            'date_picker_options' => [
                'todayBtn' => true,
//                'format' => 'yyyy-mm'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
        ]);


        /*
                $this->crud->addField([
                    'name' => 'date_date_range', // a unique name for this field
                    'start_name' => 'start_date', // the db column that holds the start_date
                    'end_name' => 'end_date', // the db column that holds the end_date
                    'label' => 'Select Date Range',
                    'type' => 'date_range',
                    // OPTIONALS
                    'start_default' => '2015-03-28', // default value for start_date
                    'end_default' => date('Y-m-d'), // default value for end_date
                    'date_range_options' => [ // options sent to daterangepicker.js
                        //'timePicker' => true,
                        'locale' => ['format' => 'DD/MM/YYYY']
        //                'locale' => ['format' => 'DD/MM/YYYY HH:mm']
                    ],
                    'wrapperAttributes' => [
                        'class' => 'form-group col-md-9'
                    ],
                ]);*/




        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/account-external/main-script'
        ]);

        $this->crud->setCreateView('custom.create_report_account_external');


        // add asterisk for fields that are required in ReportAccountingRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->setPermissions();

    }


    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'report-accounting';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if (_can2($this,'create-'.$fname)) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if (_can2($this,'update-'.$fname)) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if (_can2($this,'delete-'.$fname)) {
            $this->crud->allowAccess('delete');
        }


        if (_can2($this,'clone-'.$fname)) {
            $this->crud->allowAccess('clone');
        }

    }


    public function index()
    {
        return redirect('admin/report-account-external/create');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function accountList(Request $request){

//        dd($request->all());
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;
        $show_zero = $request->show_zero;

        $rows = AccountChartExternal::where(function ($query) use ($acc_chart_id){
            if($acc_chart_id != null){
                if(is_array($acc_chart_id)){
                    if(count($acc_chart_id)>0){
                        $query->whereIn('id',$acc_chart_id);
                    }
                }
            }

        })->get();

        $getAccountBalAll = ReportSRD::getAccountBalAll($acc_chart_id);
        $bals = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->external_acc_id] = ($r->t_dr??0) - ($r->t_cr??0) ;
            }

        }

        return view('partials.reports.account-external.account-list',['rows'=>$rows,
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals]);

    }

    public function trialBalance(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        $rows =  AccountChartExternal::where(function ($query) use ($acc_chart_id){
            if($acc_chart_id != null){
                if(is_array($acc_chart_id)){
                    if(count($acc_chart_id)>0){
                        $query->whereIn('id',$acc_chart_id);
                    }
                }
            }

        })->get();

        $getAccountBalAll = ReportSRD::getAccountBalAll($acc_chart_id,$end_date);
        $bals = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->external_acc_id] = ($r->t_dr??0) - ($r->t_cr??0) ;
            }

        }

        return view('partials.reports.account-external.trial-balance',['rows'=>$rows,
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals]);

    }


    public function profitLoss(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        $getAccountBalAll = ReportSRD::getAccountBalAll($acc_chart_id,$start_date,$end_date,[40,50,60,70,80]);

        //dd($getAccountBalAll);
        $bals = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->section_id][$r->external_acc_id] = ($r->t_dr??0) - ($r->t_cr??0) ;
            }

        }


        return view('partials.reports.account-external.profit-loss',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals]);

    }


    public function profitLossMKT(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $branch_id = $request->branch_id;

        return view('partials.reports.account-external.profit-loss-mkt',[
            'start_date'=>$start_date,'end_date'=>$end_date,'branch_id'=>$branch_id]);

    }

    public function profitLoss_(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;
        $branch_id = $request->branch_id;


        $getAccountBalAll = ReportSRD::getAccountBalAll($acc_chart_id,$start_date,$end_date,[40,50,60,70,80]);

        //dd($getAccountBalAll);
        $bals = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->section_id][$r->external_acc_id] = ($r->t_dr??0) - ($r->t_cr??0) ;
            }

        }

        return view('partials.reports.account-external.profit-loss',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,'branch_id'=>$branch_id]);

    }

    public function balanceSheet(Request $request){

//        dd($request->all());
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        $getAccountBalAll = ReportSRD::getAccountBalAll($acc_chart_id,$end_date,null,
            [10,12,14,16,18,20,22,24,26,30]);

        $getRetainedEarningBegin = ReportSRD::getRetainedEarning($start_date,null,true);
        $profit = ReportSRD::getRetainedEarning($start_date,$end_date,false);



        $bals = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->section_id][$r->external_acc_id] = ($r->t_dr??0) - ($r->t_cr??0) ;
            }
        }

        return view('partials.reports.account-external.balance-sheet',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,
            'retainedEarningBegin'=> (optional($getRetainedEarningBegin)->bal) ,
            'profit'=> (optional($profit)->bal)
        ]);

    }




    public function balanceSheetMKT(Request $request){

//        dd($request->all());
        $start_date = $request->start_date;

        $month = $request->month;
        $branch_id = $request->branch_id;

        /*$getAccountBalAll = ReportSRD::getAccountBalAll($acc_chart_id,$end_date,null,
            [10,12,14,16,18,20,22,24,26,30]);

        $getRetainedEarningBegin = ReportSRD::getRetainedEarning($start_date,null,true);
        $profit = ReportSRD::getRetainedEarning($start_date,$end_date,false);



        $bals = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->section_id][$r->external_acc_id] = ($r->t_dr??0) - ($r->t_cr??0) ;
            }
        }*/

        return view('partials.reports.account-external.balance-sheet-mkt',[
            'start_date'=>$start_date,'branch_id'=>$branch_id
        ]);

    }

    public function transactionDetailByAccount(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        $getTransDetails = ReportSRD::getTransactionDetail($acc_chart_id,$start_date,$end_date);


        return view('partials.reports.account-external.transaction-detail-by-acc',[
            'start_date'=>$start_date,'end_date'=>$end_date,
            'rows'=> $getTransDetails
        ]);

    }
    public function profitLossDetail(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        $getTransDetails = ReportSRD::getTransactionDetail($acc_chart_id,$start_date,$end_date,[40,50,60,70,80]);

        $bals = [];
        if($getTransDetails != null){
            foreach ($getTransDetails as $r){
                $bals[$r->section_id][$r->acc_chart_id][$r->id] = $r;
            }

        }

        return view('partials.reports.account-external.profit-loss-detail',[
            'start_date'=>$start_date,'end_date'=>$end_date,
            'bals'=> $bals
        ]);

    }
    public function profitLossByJob(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        $getAccountBalAll = ReportSRD::getAccountBalAllByJob($acc_chart_id,$start_date,$end_date,[40,50,60,70,80]);

        //dd($getAccountBalAll);
        $bals = [];
        $jobs = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->section_id][$r->acc_chart_id][$r->job_id>0?$r->job_id:'NA'] = ($r->t_dr??0) - ($r->t_cr??0) ;
                $jobs[$r->job_id>0?$r->job_id:'NA'] = $r->job_id;
            }

        }

        //dd($jobs,$bals);
        return view('partials.reports.account-external.profit-loss-by-job',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,'jobs'=>$jobs]);

    }
    public function profitLossByClass(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        $getAccountBalAll = ReportSRD::getAccountBalAllByClass($acc_chart_id,$start_date,$end_date,[40,50,60,70,80]);

        //dd($getAccountBalAll);
        $bals = [];
        $jobs = [];
        if($getAccountBalAll != null){
            foreach ($getAccountBalAll as $r){
                $bals[$r->section_id][$r->acc_chart_id][$r->class_id>0?$r->class_id:'NA'] = ($r->t_dr??0) - ($r->t_cr??0) ;
                $jobs[$r->class_id>0?$r->class_id:'NA'] = $r->class_id;
            }

        }

        //dd($jobs,$bals);
        return view('partials.reports.account-external.profit-loss-by-class',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals,'jobs'=>$jobs]);

    }
    public function cashStatement(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        $cash_statement = ReportSRD::getCashStatement($start_date,$end_date,false);

        $type = [];
        $bal_type = [];

        if($cash_statement != null){
            foreach ($cash_statement as $r){
                $type[$r->tran_type??'N/A'] = $r->tran_type??'N/A'  ;
                $bal_type[$r->external_acc_id??0][$r->tran_type??'N/A'] = $r->bal??0  ;
            }
        }

        //dd($bal_type);

        return view('partials.reports.account-external.cash-statement',[
            'start_date'=>$start_date,'end_date'=>$end_date,'type' => $type,'bal_type'=>$bal_type]);

    }

    public function cashStatementDetail(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        $cash_statement = ReportSRD::getCashStatementDetail($start_date,$end_date,false);

        $bals = [];
        if($cash_statement != null){
            foreach ($cash_statement as $r){
                $bals[$r->section_id][$r->acc_chart_id][$r->id] = $r;
            }

        }
        return view('partials.reports.account-external.cash-statement-detail',[
            'start_date'=>$start_date,'end_date'=>$end_date,'bals'=>$bals]);

    }




    public function portfolioOutreach(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        return view('partials.reports.account-external.portfolio-outreach',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }
    public function tenLargestLoan(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        return view('partials.reports.account-external.ten-largest-loan',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }
    public function tenLargestDeposit(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        return view('partials.reports.account-external.ten-largest-deposit',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }
    public function marketConduct(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;


        return view('partials.reports.account-external.market-conduct',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }



    /*
     *
     * Route::get('/external-monthly-progress', 'Admin\ReportSRDCrudController@monthlyProgress');
Route::get('/external-saving', 'Admin\ReportSRDCrudController@tenLargestLoan');
Route::get('/external-donor-lender-information', 'Admin\ReportSRDCrudController@saving');
Route::get('/external-staff-information', 'Admin\ReportSRDCrudController@staffInformation');
Route::get('/external-loan-type', 'Admin\ReportSRDCrudController@loanType');
Route::get('/external-section-wise', 'Admin\ReportSRDCrudController@sectionWise');
Route::get('/external-section-wise-outstanding', 'Admin\ReportSRDCrudController@sectionWiseOutstanding');
     */
    public function prudentialIndicator(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        return view('partials.reports.account-external.prudential-indicator',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }

    public function monthlyProgress(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        return view('partials.reports.account-external.monthly-progress',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }
/*
 * Route::get('/external-monthly-progress', 'Admin\ReportSRDCrudController@monthlyProgress');
Route::get('/external-saving', 'Admin\ReportSRDCrudController@saving');
Route::get('/external-donor-lender-information', 'Admin\ReportSRDCrudController@donorLenderInformation');
Route::get('/external-staff-information', 'Admin\ReportSRDCrudController@staffInformation');
Route::get('/external-loan-type', 'Admin\ReportSRDCrudController@loanType');
Route::get('/external-section-wise', 'Admin\ReportSRDCrudController@sectionWise');
Route::get('/external-section-wise-outstanding', 'Admin\ReportSRDCrudController@sectionWiseOutstanding');
 */

    public function saving(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        return view('partials.reports.account-external.saving',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }

    public function donorLenderInformation(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        return view('partials.reports.account-external.donor-lender-information',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }

    public function staffInformation(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        return view('partials.reports.account-external.staff-information',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }

    public function loanType(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        return view('partials.reports.account-external.loan-type',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }

    public function sectionWise(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        return view('partials.reports.account-external.section-wise',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }

    public function sectionWiseOutstanding(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $month = $request->month;
        $acc_chart_id = $request->acc_chart_id;

        return view('partials.reports.account-external.section-wise-outstanding',[
            'start_date'=>$start_date,'end_date'=>$end_date]);

    }





}
