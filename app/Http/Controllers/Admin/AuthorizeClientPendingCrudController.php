<?php

namespace App\Http\Controllers\Admin;

use App\Models\ClientPending;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AuthorizeClientPendingRequest as StoreRequest;
use App\Http\Requests\AuthorizeClientPendingRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class AuthorizeClientPendingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AuthorizeClientPendingCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\AuthorizeClientPending');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/authorize-client-pending');
        $this->crud->setEntityNameStrings(_t('Authorize Client Pending'),_t('Authorize Client Pending'));



        $this->crud->enableDetailsRow();
        $this->crud->allowAccess('details_row');

        $this->crud->addFilter([
            'name'=>'id',
            'label' => '',
            'type'=>'script',
            //'css'=>asset(''),
            'js' => 'show_detail.authorize-client-pending-script',
        ]);


        $this->crud->addColumn([
            'name' => 'name',
            'label' => _t('name'),
        ]);

        $this->crud->addColumn([
            'name' => 'nrc_number',
            'label' => _t('NRC number'),
        ]);
        $this->crud->addColumn([
            'name' => 'phone_1',
            'label' => _t('Phone 1'),
        ]);

        $this->crud->addColumn([
            'name' => 'phone_2',
            'label' => _t('Phone 2'),
        ]);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        // add asterisk for fields that are required in AuthorizeClientPendingRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->setPermissions();

    }


    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'authorize-client-pending';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }



        /*
                if (_can2($this,'clone-'.$fname)) {
                    $this->crud->allowAccess('clone');
                }*/
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function showDetailsRow($id)
    {
        $this->crud->hasAccessOrFail('details_row');

        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $this->crud->getCurrentEntryId() ?? $id;

        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package



        ///dd($this->data);
        return view('show_detail.authorize-client-pending', $this->data);
    }

    public function authorize_client(Request $request){
        $client_id = $request->client_id;
        $status = $request->status;
        $note = $request->note;
        $date = $request->date;


        $m = ClientPending::find($client_id);

        if ($m != null){
            $m->status=$status;
            $m->approved_by=auth()->user()->id??0;
            $m->status=$status;
            $m->check_date=$date;
            $m->note=$note;

            if ($m->save()){
                return [
                    'error'=>0
                ];
            }

            return [
                'error'=>1
            ];


        }
    }
}
