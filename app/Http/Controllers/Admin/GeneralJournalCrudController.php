<?php

namespace App\Http\Controllers\Admin;


use App\Models\AccountChart;
use App\Models\BranchU;
use App\Models\GeneralJournal;
use App\Models\GeneralJournalDetail;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Auth;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\GeneralJournalRequest as StoreRequest;
use App\Http\Requests\GeneralJournalRequest as UpdateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\UserBranch;

/**
 * Class GeneralJournalCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class GeneralJournalCrudController extends CrudController
{
    public function index()
    {

        $this->crud->hasAccessOrFail('list');
        $this->crud->setOperation('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? mb_ucfirst($this->crud->entity_name_plural);
        $g_journals = GeneralJournal::orderBy('id','desc')->paginate(3);
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        //return view($this->crud->getListView(), $this->data);

        //return view('partials.account.list-general-journal',['g_journals'=>$g_journals]);
        return redirect('api/search-general-journal');
    }
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\GeneralJournal');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/general-journal');
        $this->crud->setEntityNameStrings('General Journal', 'General Journal');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $branch_id = session('s_branch_id');
        $br = BranchU::find($branch_id);

        $this->crud->addColumn([
            'name' => 'date_general',
            'label' => _t('Date'),
            'type' => 'date'
        ]);
        $this->crud->addColumn([
            'name' => 'reference_no',
            'label' => _t('Reference No'),
        ]);

        //$this->crud->setFromDb();

       /* $this->crud->addField([
            'name' => 'reference_no',
            'label' => _t('Reference No'),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],

        ]);*/


        $this->crud->addField([
            'name' => 'reference_no',
            'label' => 'Reference No',
            'default' => GeneralJournal::getSeqRef(),
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],

        ]);

        $this->crud->addField([
            'name' => 'date_general',
            'label' => _t('Date'),
            'type' => 'date_picker',
            'default' => date('Y-m-d'),
            'date_picker_options' => [
                'todayBtn' => true,
                'format' => 'yyyy-mm-dd',
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],

        ]);

        $this->crud->addField([
            'label'             => _t('currency'),
            'type'              => "_currency",
            'name'              => 'currency_id',
            'entity'            => 'currency',
            'attribute'         => "currency_name",
            'symbol'            => "currency_symbol",
            'exchange_rate'     => "exchange_rate",
            'model'             => "App\\Models\\Currency",
            'placeholder'       => "",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class'         => 'form-group col-md-4'
            ],
            // 'pivot' => true,
        ]);

        $BranchU = BranchU::all();
        $arr_bu = [];
        if($BranchU != null) {
            foreach ($BranchU as $bb) {
                $arr_bu[$bb->id] = $bb->code . '-' . $bb->title;
            }
        }

        $this->crud->addField([
            'name' => 'branch_id',
            'type' => 'select2_from_array',
            'options' => $arr_bu,
            'default' =>  optional($br)->id,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ]
            // 'value' =>  optional($br)->id
        ]);


        $this->crud->addField([
            'name' => 'note',
//            'type' => 'text',
            'label' => _t('Note'),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],

        ]);

        $this->crud->addField([
            'label' => _t("Chart Account"),
            'type' => "select2_from_ajax_coa",
            'name' => 'acc_chart_id',
            'entity' => 'acc_chart',
            'attribute' => "name",
            'model' => "App\\Models\\AccountChart",
            'data_source' => url("api/acc-chart"),
            'placeholder' => "Select a Chart",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ]
        ]);

        $this->crud->addField([
            'name' => 'script-general-journal',
            'type' => 'view',
            'view' => 'partials/account/script-general-journal'
        ]);


        // add asterisk for fields that are required in GeneralJournalRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->setPermissions();
    }
    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'general-journal';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }

        // Allow create access


        if (_can2($this,'create-'.$fname)) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if (_can2($this,'update-'.$fname)) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if (_can2($this,'delete-'.$fname)) {
            $this->crud->allowAccess('delete');
        }

    }
    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        GeneralJournal::save_detail($this->crud->entry, $request);
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        GeneralJournal::save_detail($this->crud->entry, $request);
        return $redirect_location;
    }

    public function add_detail(Request $request)
    {

       // dd($request->all());
        $acc_chart_id = $request->acc_chart_id;
        $row = AccountChart::find($acc_chart_id);
        if($row != null){
            return view('partials/account/add-detail',['acc_chart'=>$row]);
        } else {
            return '';
        }
    }

    public function g_j($start_date,$end_date,$reference_no,$client_id,$frd_acc_code,$acc_code,$branch_id){

        return  GeneralJournal::leftJoin('general_journal_details', 'general_journals.id', '=', 'general_journal_details.journal_id')
            ->where(function ($query) use ($start_date, $end_date) {
                if ($start_date != null && $end_date == null) {
                    return $query->whereDate('general_journal_details.j_detail_date','<=',$start_date);
                } else if ($start_date != null && $end_date != null) {
                    return $query->whereDate('general_journal_details.j_detail_date','>=',$start_date)
                        ->whereDate('general_journal_details.j_detail_date','<=',$end_date);
                }

            })
                ->where(function ($query) use ($reference_no){
                    if($reference_no != null){
                        return $query->where('general_journals.reference_no',$reference_no);
                    }
                })

            ->where(function ($query) use ($client_id){
                if($client_id >0){
                    return $query->where('general_journal_details.name',$client_id);
                }
            })->where(function ($query) use ($frd_acc_code){
                if($frd_acc_code != null){
                    return $query->where('general_journal_details.external_acc_chart_id',$frd_acc_code);
                }
            })->where(function ($query) use ($acc_code){
                if($acc_code >0){
                    return $query->where('general_journal_details.acc_chart_id',$acc_code);
                }
            })->where(function ($query) use ($branch_id){
                if($branch_id >0){
                    return $query->where('general_journal_details.branch_id',$branch_id);
                }
            })
            ->selectRaw('general_journal_details.journal_id as id')
            ->orderBy('general_journal_details.journal_id','desc')
            ->groupBy('general_journal_details.journal_id');
    }

    public function list_detail(Request $request){
        $user_id = Auth::user()->id;
        //dd(Auth::user()->branches);
        $branches = \App\User::find($user_id)->user_branch()->where('user_id',$user_id)->get()->first() ;
        if($branches != NULL && companyReportPart() == 'company.mkt'){
            $branch = \App\Models\Branch::where('id',$branches->branch_id)->get()->first();
        }
        //dd($branch->title);
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $reference_no = $request->reference_no;
        $client_id = $request->client_id-0;
        $frd_acc_code = $request->frd_acc_code-0;
        $acc_code = $request->acc_code-0;
        $branch_id = $request->branch_id-0;
        if($branches != NULL && companyReportPart() == 'company.mkt'){
            if(empty($request->branch_id)){
                $branch_id = $branch->id;
            }
        }


        $pag=10;

        if (session('general-journal-page') != null){
            $pag=session('general-journal-page');
        }
        

        if ($pag ==0 ){
            $g_detail = $this->g_j($start_date,$end_date,$reference_no,$client_id,$frd_acc_code,$acc_code,$branch_id)->get();
        }else{
            $g_detail = $this->g_j($start_date,$end_date,$reference_no,$client_id,$frd_acc_code,$acc_code,$branch_id)->paginate($pag);
        }

        if ($pag > 0){
            $g_detail->appends(Input::except('page'));
        }

            /*$g_detail->appends([
                'start_date'=>$start_date,
                'reference_no'=>$reference_no,
                'client_id' => $client_id,
                'frd_acc_code' => $frd_acc_code,
                'acc_code' =>$acc_code
            ]);
            */
            $data = null;
            if($g_detail != null){
                //if($g_detail->count()>0){
                    $arr = [];
                    foreach ($g_detail as $r){
                        $arr[] = $r->id;
                    }
                    if(count($arr)>0){
                        $data = GeneralJournal::leftJoin('general_journal_details', 'general_journals.id', '=', 'general_journal_details.journal_id')
                            ->whereIn('general_journals.id',$arr)
                            ->orderBy('general_journals.id','desc')
                            ->get();
//                        dd($data);
                    }
               // }
            }

            //dd($g_detail);
        return view('partials.account.list-general-journal-e',['rows'=>$g_detail,'data'=>$data,'pag'=>$pag]);

    }
    /*public function search_journal(Request $request){
        $start_date = $request->start_date;
        $end_date= $request->end_date;
        $reference_no = $request->reference_no;
        $client_id = $request->client_id-0;
        $frd_acc_code = $request->frd_acc_code-0;
        $acc_code = $request->acc_code-0;
        $branch_id = $request->branch_id-0;

        $pag=10;

        if (session('general-journal-page') != null){
            $pag=session('general-journal-page');
        }

        if ($pag ==0 ){
            $g_detail = $this->g_j($start_date,$end_date,$reference_no,$client_id,$frd_acc_code,$acc_code,$branch_id)->get();
        }else{
            $g_detail = $this->g_j($start_date,$end_date,$reference_no,$client_id,$frd_acc_code,$acc_code,$branch_id)->paginate($pag);
        }

        /*$g_detail = GeneralJournal::leftJoin('general_journal_details', 'general_journals.id', '=', 'general_journal_details.journal_id')
                    ->where(function ($query) use ($start_date, $end_date) {
                        if ($start_date != null && $end_date == null) {
                            return $query->whereDate('general_journal_details.j_detail_date','<=',$start_date);
                        } else if ($start_date != null && $end_date != null) {
                            return $query->whereDate('general_journal_details.j_detail_date','>=',$start_date)
                                ->whereDate('general_journal_details.j_detail_date','<=',$end_date);
                        }

                    })
                    ->where(function ($query) use ($reference_no){
                        if($reference_no != null){
                            return $query->where('general_journals.reference_no',$reference_no);
                        }
                    })->where(function ($query) use ($client_id){
                        if($client_id >0){
                            return $query->where('general_journal_details.name',$client_id);
                        }
                    })->where(function ($query) use ($frd_acc_code){
                        if($frd_acc_code != null){
                            return $query->where('general_journal_details.external_acc_chart_id',$frd_acc_code);
                        }
                    })->where(function ($query) use ($acc_code){
                        if($acc_code >0){
                            return $query->where('general_journal_details.acc_chart_id',$acc_code);
                        }
                    })
            ->orderBy('general_journals.id','desc')
            ->paginate($pag);

       // return view('partials.account.journal-list-search',['g_journals'=>$g_detail]);

    }*/
    public function delete_journal($id){
        GeneralJournal::where('id',$id)->delete();
        GeneralJournalDetail::where('journal_id',$id)->delete();
        return redirect()->back();
    }

    public function update_g_journal_pag($pag){
        session(['general-journal-page'=>$pag]);

        return redirect('api/search-general-journal');
    }

}
