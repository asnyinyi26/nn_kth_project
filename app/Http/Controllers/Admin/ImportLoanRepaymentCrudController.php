<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportLoanRepayment;
use App\Imports\ImportloanRepayment;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ImportLoanRepaymentRequest as StoreRequest;
use App\Http\Requests\ImportLoanRepaymentRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Maatwebsite\Excel\Facades\Excel;
use Session;

/**
 * Class ImportLoanRepaymentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ImportLoanRepaymentCrudController extends CrudController
{

    public function index()
    {
        return redirect('admin/import-loan-repayment/create');
    }
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ImportLoanRepayment');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/import-loan-repayment');
        $this->crud->setEntityNameStrings('import-loan-repayment', 'import_loan_repayments');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addField([
            'name' => 'export-import',
            'type' => 'view',
            'view' => 'partials/payment/script-export-import',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-10'
            ],
        ]);

        // add asterisk for fields that are required in ImportLoanRepaymentRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        if ($request->hasFile('open_detail_file')){
            Excel::import(new ImportloanRepayment(), $request->file('open_detail_file'));
            Session::flash('message','Successfully Imported');
        }
        return redirect()->back();
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function download_excel()
    {

        return Excel::download(new ExportLoanRepayment(),'import-loan-repayment-'.date('Y-m-d H:s').".xlsx",\Maatwebsite\Excel\Excel::XLSX);
    }
}
