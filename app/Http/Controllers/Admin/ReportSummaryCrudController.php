<?php

namespace App\Http\Controllers\Admin;

use App\Models\Branch;
use App\Models\CenterLeader;
use App\Models\GroupLoan;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ReportSummaryRequest as StoreRequest;
use App\Http\Requests\ReportSummaryRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class ReportSummaryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ReportSummaryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ReportSummary');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/report-summary');
        $this->crud->setEntityNameStrings('reportsummary', 'report_summaries');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->addField([
            'label' => "Branch",
            'type' => "select2_from_ajax_multiple",
            'name' => 'branch_id',
            // 'entity' => 'category',
            'attribute' => "title",
            'model' => Branch::class,
            'data_source' => url("api/get-branch"),
            'placeholder' => "Select branch",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ]
        ]);
        $this->crud->addField([
            'name' => 'center_id',
            'type' => 'select2_from_ajax_multiple',
            'label' => 'Center ID',
            'attribute' => "title",
            'model' => CenterLeader::class,
            'data_source' => url("api/get-center-leader"),
            'placeholder' => "Select center",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ]
        ]);
        $this->crud->addField([
            'name' => 'group_id',
            'type' => 'select2_from_ajax_multiple',
            'label' => 'Group ID',
            'attribute' => "group_code",
            'model' => GroupLoan::class,
            'data_source' => url("api/get-group-id"),
            'placeholder' => "Select Group",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ]
        ]);

        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/summary-report/main-script'
        ]);
        $this->crud->setCreateView('custom.create_summary_report');


        // add asterisk for fields that are required in ReportSummaryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function index()
    {
        return redirect('admin/summary-report/create');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function borrower_wise(Request $request)
    {
        return view('partials.reports.summary-report.borrow_wise');
    }
    public function center_wise(Request $request)
    {
        return view('partials.reports.summary-report.center_wise');
    }
    public function outstanding(Request $request)
    {
        return view('partials.reports.summary-report.outstanding');
    }
}
