<?php

namespace App\Http\Controllers\Admin;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OfficerCollectionExport;

/**
 * Class PaidDisbursementCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OfficerCollectionReportController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\LoanPayment');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/report/officer-collection');
        $this->crud->setEntityNameStrings('C.O Collection Report', 'C.O Collection Report');
        $this->crud->enableExportButtons();
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->addClause('whereHas', 'loan_disbursement', function($query) {
            $query->where('disbursement_number', '!=', null);
        });

        $this->crud->addFilter([ // Branch select2_ajax filter
            'name' => 'branch_id',
            'type' => 'select2_ajax',
            'label'=> 'Branch',
            'placeholder' => 'Select Branch'
        ],
        url('/api/branch-option'), // the ajax route
        function($value) { // if the filter is active
            $this->crud->addClause('whereHas', 'loan_disbursement', function($query) use($value) {
                $query->where('branch_id', $value);
            });
        });

        $this->crud->addFilter([ // Center select2_ajax filter
            'name' => 'center_id',
            'type' => 'select2_ajax',
            'label'=> 'Center',
            'placeholder' => 'Select Center'
        ],
        url('/api/center-option'), // the ajax route
        function($value) { // if the filter is active
            $this->crud->addClause('whereHas', 'loan_disbursement', function($query) use($value) {
                $query->where('center_leader_id', $value);
            });
        });

        $this->crud->addFilter([ // Loan Officer select2_ajax filter
            'name' => 'loan_officer_id',
            'type' => 'select2_ajax',
            'label'=> 'Loan Officer',
            'placeholder' => 'Select Loan Officer'
        ],
        url('/api/loan-officer-option'), // the ajax route
        function($value) { // if the filter is active
            $this->crud->addClause('whereHas', 'loan_disbursement', function($query) use($value) {
                $query->where('loan_officer_id', $value);
            });
        });

        $this->crud->addFilter([ // daterange filter
            'type' => 'date_range_blank',
            'name' => 'from_to',
            'label'=> 'Date'
        ],
        false,
        function($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('where', 'payment_date', '>=', $dates->from);
            $this->crud->addClause('where', 'payment_date', '<=', $dates->to . ' 23:59:59');
        });

        $this->crud->addColumn([
            // 1-n relationship
            'label' => _t('Client'),
            'name' => 'client_id',
            'type' => "select",
            'entity' => 'client_name', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\LoanPayment", // foreign key model
        ]);

        $this->crud->addColumn([
            'label' => _t('Loan Number'),
            'name' => 'disbursement_id',
            'type' => "select",
            'entity' => 'loan_disbursement', // the method that defines the relationship in your Model
            'attribute' => "disbursement_number", // foreign key attribute that is shown to user
            'model' => "App\Models\LoanPayment", // foreign key model
        ]);

        $this->crud->addColumn([
            'label' => _t('Date'),
            'name' => 'payment_date',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'label' => _t('Principle Collection'),
            'name' => 'principle',
            'type' => 'number'
        ]);

        $this->crud->addColumn([
            'label' => _t('Interest Collection'),
            'name' => 'interest',
            'type' => 'number'
        ]);

        $this->crud->addColumn([
            'label' => _t('Service Fee'),
            'name' => 'other_payment',
            'type' => 'number'
        ]);

        $this->crud->addColumn([
            'label' => _t('Penalty Collection'),
            'name' => 'penalty_amount',
            'type' => 'number'
        ]);

        $this->crud->addColumn([
            'label' => _t('Total Collection'),
            'name' => 'total_payment',
            'type' => 'number'
        ]);

        $this->crud->disableResponsiveTable();
        $this->crud->setDefaultPageLength(10);
        $this->crud->setListView('partials.loan_disbursement.officer-collection');
        $this->crud->removeAllButtons();

        $this->setPermissions();
    }

    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'officer-collection';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }
    }

    public function excel(Request $request)
    {
        return Excel::download(new OfficerCollectionExport("partials.loan-payment.officer-collection-list", $request->all()), 'CO_Collection_Report_'.date("d-m-Y_H:i:s").'.xlsx');
    }
}
