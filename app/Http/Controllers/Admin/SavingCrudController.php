<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\IDate;
use App\Helpers\MFS;
use App\Helpers\UnitDay;
use App\Models\Client;
use App\Models\Branch;
use App\Models\CenterLeader;
use App\Models\CompulsorySavingTransaction;
use App\Models\GroupLoan;
use App\Models\Guarantor;
use App\Models\LoanCharge;
use App\Models\LoanCompulsory;
use App\Models\Loan;
use App\Models\LoanCalculate;
use App\Models\LoanProduct;
use App\Models\Saving;
use App\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SavingRequest as StoreRequest;
use App\Http\Requests\SavingRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class LoanCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */


class SavingCrudController extends CrudController
{
    public function printSchedule(Request $request){

        //dd($request->all());
        $loan_id = $request->loan_id;
        $row = Loan::find($loan_id);

        if(companyReportPart() == 'company.moeyan'){
            return view ('partials.loan_disbursement.print_schedule_moeyan',['row'=>$row]);
        }
        else{
            return view ('partials.loan_disbursement.print_schedule',['row'=>$row]);
        }
    }

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Saving');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/saving');
        $this->crud->setEntityNameStrings('saving', 'saving');
        $this->crud->orderBy('saving.id','DESC');



        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

//        $this->crud->addColumn([
//            'name' => 'create_at',
//            'label' => 'Account Number',
//            'type' => 'closure',
//            'function' => function ($entry) {
//                $client_id = $entry->client_id;
//                //dd($client_id);
//                return optional(Client::find($client_id))->client_number;
//            }
//        ]);




        $this->crud->disableResponsiveTable();


        $this->crud->addField(
            [
                'label' => _t('Client ID'),
                'type' => "select2_from_ajax_client",
                'name' => 'client_id', // the column that contains the ID of that connected entity
                'entity' => 'client_name', // the method that defines the relationship in your Model
                'attribute' => "client_number", // foreign key attribute that is shown to user
                'model' => "App\\Models\\Client", // foreign key model
                'data_source' => url("api/get-client"), // url to controller search function (with /{id} should return model)
                'placeholder' => _t("Select a client code"), // placeholder for the select
                'minimum_input_length' => 0, // minimum characters to type before querying results
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-4 client_id'
                ],
            //    'tab' => _t('Client'),
                'tab' => _t('Account'),
            ]
        );

        $this->crud->addField([
            'label' => _t('Client nrc'),
            'name' => 'client_nrc_number',
            'type' => 'text_read',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
          //  'tab' => _t('Client'),
//            'location_group' => 'General',
        ]);

        $this->crud->addField([
            'label' => _t('Client Name'),
            'name' => 'client_name',
            'type' => 'text_read',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
          //  'tab' => _t('Client'),
            'tab' => _t('Account'),
//            'location_group' => 'General',
        ]);


//        $this->crud->addField([
//            'label' => _t('Client phone'),
//            'name' => 'client_phone',
//            'type' => 'text_read',
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-3'
//            ],
//           // 'tab' => _t('Client'),
//            'tab' => _t('Account'),
////            'location_group' => 'General',
//        ]);
//        $this->crud->addField([
//            'label' => _t('saving_amount'),
//            'name' => 'available_balance',
//            'type' => 'text_read',
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-3'
//            ],
//            //'tab' => _t('Client'),
//            'tab' => _t('Account'),
//        ]);


        $this->crud->addField([
            'label' => _t('Branch'),
            'type' => "select2_from_ajax",
            'default' => session('s_branch_id'),
            'name' => 'branch_id', // the column that contains the ID of that connected entity
            'entity' => 'branch_name', // the method that defines the relationship in your Model
            'attribute' => "title", // foreign key attribute that is shown to user
            'model' => "App\\Models\\Branch", // foreign key model
            'data_source' => url("api/get-branch"), // url to controller search function (with /{id} should return model)
            'placeholder' => _t("Select a branch"), // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
//            'location_group' => 'General',
        ]);


        $this->crud->addField([
            'label' => _t('Center Name'),
            'type' => "select2_from_ajax_center",
            'name' => 'center_id', // the column that contains the ID of that connected entity
            'entity' => 'center_leader_name', // the method that defines the relationship in your Model
            'attribute' => "title", // foreign key attribute that is shown to user
            'model' => "App\\Models\\CenterLeader", // foreign key model
            'data_source' => url("/api/get-center-leader-name"), // url to controller search function (with /{id} should return model)
            'placeholder' => _t("Select a center leader name"), // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
//            'location_group' => 'General',
        ]);
//
//        $this->crud->addField([
//            'label' => _t('Applicant Name'),
//            'name' => 'disbursement_name',
//            'type' => 'text',
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
//            'tab' => _t('Account'),
//        ]);


        $this->crud->addField([
            'label' => _t('Loan Officer Name'),
            'type' => "select2_from_ajax_loan_officer",
            'name' => 'loan_officer_id', // the column that contains the ID of that connected entity
            'entity' => 'officer_name', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\\User", // foreign key model
            'data_source' => url("api/get-user"), // url to controller search function (with /{id} should return model)
            'placeholder' => _t("Select a officer"), // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
//            'location_group' => 'General',
        ]);


        $this->crud->addField([
            'label' => _t('Saving Product'),
            'type' => "select2_from_ajax",
            'name' => 'saving_product_id', // the column that contains the ID of that connected entity
            'entity' => 'saving_product', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\\Models\\SavingProduct", // foreign key model
            'data_source' => url("api/get-saving-product"), // url to controller search function (with /{id} should return model)
            'placeholder' => _t("Select a Saving product"), // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'wrapperAttributes' => [
                //'class' => 'form-group col-md-4 loan_product'
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
//            'location_group' => 'General',
        ]);


        $this->crud->addField([
            'label' => _t('Apply date'),
            'name' => 'apply_date',
            'type' => 'date_picker_event',
            'script' => 'change',
            'default' => date('Y-m-d'),
            'date_picker_options' => [
                'format' => 'yyyy-mm-dd',
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
//            'location_group' => 'General',
        ]);

        $this->crud->addField([
            'label' => _t('First Deposit Date'),
            'name' => 'first_deposit_date',
            'type' => 'date_picker_event2',
            'default' => date('Y-m-d'),
            'date_picker_options' => [
                'format' => 'yyyy-mm-dd',
            ],
            'script' => 'change',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
//            'location_group' => 'General',
        ]);

        $this->crud->addField([
            'label' => _t('Saving Type'),
            'name' => 'saving_type',
            'attributes' => [
               // 'placeholder' => 'Loan Term',
            ], // change the HTML attributes of your input
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'type' => 'enum',
            'tab' => _t('Account'),
        ]);

        $this->crud->addField([
            'label' => _t('Plan Type'),
            'name' => 'plan_type',
            'attributes' => [
               // 'placeholder' => 'Loan Term',
            ], // change the HTML attributes of your input
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'type' => 'enum',
            'tab' => _t('Account'),
        ]);

        /* principal */

        $this->crud->addField([
            'label' => _t('Expectation Amount'),
            'name' => 'expectation_amount',
            'type' => 'number2',
            'attributes' => [

            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
        ]);

        $this->crud->addField([
            'label' => _t('Fixed Deposit Amount'),
            'name' => 'fixed_payment_amount',
            'type' => 'number2',
            'attributes' => [

            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
        ]);

        /* loan */
        $this->crud->addField([
            'label' => _t('Saving Term'),
            'name' => 'saving_term',
            'attributes' => [
                // 'placeholder' => 'Loan Term',
            ], // change the HTML attributes of your input
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'type' => 'enum',
            'tab' => _t('Account'),
        ]);

        $this->crud->addField([
            'label' => _t('Saving Term Value'),
            'name' => 'term_value',
            'type' => 'number2',
            'attributes' => [
                // 'placeholder' => 'Saving Term Value',
            ], // change the HTML attributes of your input
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
        ]);

        $this->crud->addField([
            'label' => _t('Duration Deposit'),
            'name' => 'payment_term',
            'type' => 'enum',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
        ]);



        $this->crud->addField([
            'label' => _t('Duration Interest Calculate'),
            'name' => 'duration_interest_calculate',
            'attributes' => [
             //   'placeholder' => 'Loan Term',
            ], // change the HTML attributes of your input
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'type' => 'enum',
            'tab' => _t('Account'),
        ]);

        $this->crud->addField([
            'label' => _t('Duration Interest Compound'),
            'name' => 'duration_interest_compound',
            'attributes' => [
            //    'placeholder' => 'Loan Term',
            ], // change the HTML attributes of your input
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'type' => 'enum',
            'tab' => _t('Account'),
        ]);

        $this->crud->addField([
            'label' => _t('Duration Interest Compound'),
            'name' => 'duration_interest_compound',
            'attributes' => [
                //   'placeholder' => 'Loan Term',
            ], // change the HTML attributes of your input
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'type' => 'enum',
            'tab' => _t('Account'),
        ]);



        /* interest rate */
        $this->crud->addField([
            'label' => _t('Interest Rate'),
            'name' => 'interest_rate',
            'type' => 'number2',
            'attributes' => [
                // 'placeholder' => 'Interest Rate Default',
            ], // change the HTML attributes of your input
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
        ]);



        $this->crud->addField([
            'label' => _t('Interest Rate Period'),
            'name' => 'interest_rate_period',
            'type' => 'enum',
            'allows_null' => false, // change the HTML attributes of your input
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'tab' => _t('Account'),
        ]);

//        $this->crud->addField([
//            'label' => _t('Loan Amount'),
//            'name' => 'loan_amount',
//            'type' => 'number2',
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
//            'tab' => _t('Account'),
////            'location_group' => 'General',
//        ]);
//
//        $this->crud->addField([
//            'label' => _t('interest_rate'),
//            'name' => 'interest_rate',
//            'type' => 'number2',
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
//            'tab' => _t('Account'),
////            'location_group' => 'General',
//        ]);
//
//        $this->crud->addField([
//            'label' => _t('interest_rate_period'),
//            'name' => 'interest_rate_period',
//            'type' => 'enum',
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
//            'tab' => _t('Account'),
//        ]);
//
//
//        $this->crud->addField([
//            'label' => _t('Loan Term'),
//            'name' => 'loan_term',
//            'type' => 'enum',
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
//            'tab' => _t('Account'),
//        ]);
//
//        $this->crud->addField([
//            'label' => _t('Loan Term Value'),
//            'name' => 'loan_term_value',
//            'type' => 'number',
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
//            'tab' => _t('Account'),
//        ]);
//
//
//        $this->crud->addField([
//            'label' => _t('repayment_term'),
//            'name' => 'repayment_term',
//            'type' => 'enum',
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
//            'tab' => _t('Account'),
////            'location_group' => 'General',
//        ]);
//
//
////        $this->crud->addField([
////            'label' => _t('currency'),
////            'name' => 'currency',
////            'type' => 'enum',
////            'wrapperAttributes' => [
////                'class' => 'form-group col-md-4'
////            ],
////            'tab' => _t('Account'),
//////            'location_group' => 'General',
////        ]);
//
//
//        $this->crud->addField([
//            'label' => _t('Currency'),
//            'type' => 'select_not_null',
//            'name' => 'currency_id', // the db column for the foreign key
//            'entity' => 'currency_name', // the method that defines the relationship in your Model
//            'attribute' => 'currency_name', // foreign key attribute that is shown to user
//            'model' => "App\\Models\\Currency",
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
//            'tab' => _t('Account'),
////            'location_group' => 'General',
//        ]);
//
//
//        $this->crud->addField([
//            'label' => _t('Transaction Type'),
//            'type' => 'select_not_null',
//            'name' => 'transaction_type_id', // the db column for the foreign key
//            'entity' => 'transaction_type', // the method that defines the relationship in your Model
//            'attribute' => 'title', // foreign key attribute that is shown to user
//            'model' => "App\\Models\\TransactionType",
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
//            'tab' => _t('Account'),
////            'location_group' => 'General',
//        ]);
//        if(companyReportPart() == 'company.moeyan'){
//            $this->crud->addField([
//                'label' => _t('Interest Method'),
//                'name' => 'interest_method',
//                'attributes' => [
//                    'placeholder' => 'Interest Method',
//                ], // change the HTML attributes of your input
//                'wrapperAttributes' => [
//                    'class' => 'form-group col-md-4',
//                    'style' => 'display: none'
//                ],
//                'type' => 'enum_interest_method',
//                'tab' => _t('Account'),
//            ]);
//        }
//
//        $this->crud->addField([
//            'label' => _t('Group ID'),
//            'type' => "select2_from_ajax_group_loan",
//            'name' => 'group_loan_id', // the column that contains the ID of that connected entity
//            'entity' => 'group_loans', // the method that defines the relationship in your Model
//            'attribute' => "group_code", // foreign key attribute that is shown to user
//            'attribute1' => "group_name", // foreign key attribute that is shown to user
//            'model' => "App\\Models\\GroupLoan", // foreign key model
//            'data_source' => url("/api/get-group-loan"), // url to controller search function (with /{id} should return model)
//            'placeholder' => _t("Select Group Loan"), // placeholder for the select
//            'minimum_input_length' => 0, // minimum characters to type before querying results
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-3 group_loan'
//            ],
//            'tab' => _t('Account'),
//            'suffix' => true
//        ]);

        $this->crud->denyAccess(['delete', 'clone']);
        $this->crud->addClause('selectRaw', 'loans.*');
        // add asterisk for fields that are required in LoanRequest

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->crud->addButtonFromModelFunction('line', 'addButtonCustom', 'addButtonCustom', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model

        $this->setPermissions();

    }


    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'delete', 'clone']);

        $fname = 'loan';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if (_can2($this,'create-'.$fname)) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
       /* if (_can2($this,'update-'.$fname)) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if (_can2($this,'delete-'.$fname)) {
            $this->crud->allowAccess('delete');
        }*/


//        if (_can2($this,'clone-'.$fname)) {
//            $this->crud->allowAccess('clone');
//        }

    }

    public function store(StoreRequest $request)
    {

    }

    public function update(UpdateRequest $request)
    {


    }



}
