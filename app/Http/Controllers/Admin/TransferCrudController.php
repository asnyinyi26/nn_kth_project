<?php

namespace App\Http\Controllers\Admin;

use App\Models\Transfer;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TransferRequest as StoreRequest;
use App\Http\Requests\TransferRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class TransferCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TransferCrudController extends CrudController
{
    public function transferPop(Request $request){
        // dd($request->all());
        $row = Transfer::find($request->tran_id);
        return view ('partials.account.transfer-pop',['row'=>$row]);
    }
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Transfer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/transfer');
        $this->crud->setEntityNameStrings('transfer', 'transfers');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();


        $this->crud->orderBy('transfers.id','DESC');



        $this->crud->addColumn([
            'label' => _t("Reference No"),
            'name' => 'reference_no',
            
        ]);

        $this->crud->addColumn([
            'label' => _t("From Cash Account"),
            'type' => "select",
            'name' => 'from_cash_account_id',
            'entity' => 'from_cash_account',
            'attribute' => "name",
            'model' => "App\\Models\\AccountChart",
        ]);
        $this->crud->addColumn([
            'label' => _t("To Cash Account"),
            'type' => "select",
            'name' => 'to_cash_account_id',
            'entity' => 'to_cash_account',
            'attribute' => "name",
            'model' => "App\\Models\\AccountChart",
        ]);

        $this->crud->addColumn([
            'label' => _t('Transfer By'),
            'type' => 'select',
            'name' => 'transfer_by_id', // the db column for the foreign key
            'entity' => 'transfer_by', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\\User", // foreign key model
        ]);



        $this->crud->addColumn([
            'label' => _t('Receive By'),
            'type' => 'select',
            'name' => 'receive_by_id', // the db column for the foreign key
            'entity' => 'receive_by', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\\User", // foreign key model
        ]);


        $this->crud->addColumn([
            'label' => "Amount",
            'name' => 't_amount',
        ]);

        $this->crud->addColumn([
            'label' => "Date",
            'name' => 't_date',
            'type' => 'date',
        ]);

        $this->crud->addField([
            'name' => 'reference_no',
            'label' => _t('Reference No'),
            'value' => Transfer::getSeqRef(),
            'type' => 'number2',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 col-xs-12'
            ],
            'attributes' => [
                'readonly' => 'readonly',
            ]

        ]);

        $this->crud->addField([
            'label' => "Amount",
            'name' => 't_amount',
            'type' => 'number2',
            'default' => 0.00,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 col-xs-12'
            ],
            /*'prefix' => '<a href="" class="dak_blog">-</a>',
            'suffix' => '<a href="" class="plus_blog">+</a>'
            */
        ]);

        $this->crud->addField([
            'name' => 't_date',
            'label' => 'Date',
            'type' => 'date_picker',
            'default' => date('Y-m-d'),
            'date_picker_options' => [
                'todayBtn' => true,
                'format' => 'yyyy-mm-dd',
            ],
            'wrapperAttributes' => [
                'class' => 'form-group readonly  col-md-6  col-xs-12'
            ],

        ]);

        $this->crud->addField([
            'label' => _t('Transfer By'),
            'type' => 'select2_from_ajax',
            'name' => 'transfer_by_id', // the db column for the foreign key
            'entity' => 'transfer_by', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\\User", // foreign key model
            'data_source' => url("api/get-user"),
            'placeholder' => "Select a Shareholder ",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 col-xs-12'
            ]
        ]);

        $this->crud->addField([
            'label' => _t('Receive By'),
            'type' => 'select2_from_ajax',
            'name' => 'receive_by_id', // the db column for the foreign key
            'entity' => 'receive_by', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\\User", // foreign key model
            'data_source' => url("api/get-user"),
            'placeholder' => "Select a Shareholder ",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 col-xs-12'
            ]
        ]);

        
        $this->crud->addField([
            'label' => _t("From Cash Account"),
            'type' => "select2_from_ajax_coa",
            'name' => 'from_cash_account_id',
            'entity' => 'from_cash_account',
            'acc_type' => [10],
            'attribute' => "name",
            'model' => "App\\Models\\AccountChart",
            'data_source' => url("api/acc-chart"),
            'placeholder' => "Select a Chart",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 col-xs-12'
            ]
        ]);

        $this->crud->addField([
            'label' => _t("To Cash Account"),
            'type' => "select2_from_ajax_coa",
            'name' => 'to_cash_account_id',
            'entity' => 'to_cash_account',
            'acc_type' => [10],
            'attribute' => "name",
            'model' => "App\\Models\\AccountChart",
            'data_source' => url("api/acc-chart"),
            'placeholder' => "Select a Chart",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 col-xs-12'
            ]
        ]);


        $this->crud->addField([
            'label' => _t("From Branch"),
            'type' => "select2_from_ajax",
            'name' => 'from_branch_id',
            'entity' => 'from_branch',
            'attribute' => "title",
            'model' => "App\\Models\\Branch",
            'data_source' => url("api/get-branch"),
            'placeholder' => "Select Branch",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 col-xs-12'
            ]
        ]);

        $this->crud->addField([
            'label' => _t("To Branch"),
            'type' => "select2_from_ajax",
            'name' => 'to_branch_id',
            'entity' => 'to_branch',
            'attribute' => "title",
            'model' => "App\\Models\\Branch",
            'data_source' => url("api/get-branch"),
            'placeholder' => "Select Branch",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]
        ]);

        
        $this->crud->addField([
            'name' => 'description',
            'label' => 'Description',
            'type' => 'wysiwyg',
            'wrapperAttributes' => [
                'class' => 'form-group readonly  col-md-12  col-xs-12'
            ],

        ]);

        // add asterisk for fields that are required in TransferRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->crud->addButtonFromModelFunction('line', 'addButtonCustom', 'addButtonCustom', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model
        $this->setPermissions();
        $this->crud->enableExportButtons();

    }


    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'transfer';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if (_can2($this,'create-'.$fname)) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if (_can2($this,'update-'.$fname)) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if (_can2($this,'delete-'.$fname)) {
            $this->crud->allowAccess('delete');
        }


        if (_can2($this,'clone-'.$fname)) {
            $this->crud->allowAccess('clone');
        }

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        Transfer::accTransferFundTransaction($this->crud->entry);

        return redirect("/admin/transfer_pop?id={$this->crud->entry->id}");
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        Transfer::accTransferFundTransaction($this->crud->entry);
        return $redirect_location;
    }
}
