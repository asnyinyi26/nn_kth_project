<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
class DBTransaction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        \DB::beginTransaction();
        \DB::update('SET time_zone = ?', ['+07:00']);
        try {
            $response = $next($request);
        } catch (\Exception $e) {
            \DB::rollBack();
            //dd(1);
            throw $e;
        }
        if ($response instanceof Response && $response->getStatusCode() > 399) {
            \DB::rollBack();
            //dd(2);
        } else {
            \DB::commit();
            //dd(3);
        }

        return $response;
    }
}
