<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register','M\RegisterController@register');
Route::post('/login','M\LoginController@login');

Route::get('/get-client-near-by','M\ClientController@getClientNearBy');
Route::post('/get-client-near-by','M\ClientController@getClientNearBy');

Route::get('/get-client','M\ClientController@getClient');
Route::post('/get-client','M\ClientController@getClient');
Route::post('/store-client','M\ClientController@storeClient');
Route::post('/store-loan','M\LoanController@storeLoan');
Route::get('/get-branch','M\ClientController@getBranch');
Route::post('/get-branch','M\ClientController@getBranch');

Route::get('/get-center-leader','M\ClientController@getCenterLeader');
Route::post('/get-center-leader','M\ClientController@getCenterLeader');

Route::get('/get-guarantor','M\LoanController@getGuarantor');
Route::post('/get-guarantor','M\LoanController@getGuarantor');

Route::get('/get-product','M\LoanController@getProduct');
Route::post('/get-product','M\LoanController@getProduct');

Route::get('/get-loan-officer','M\ClientController@getLoanOfficer');
Route::post('/get-loan-officer','M\ClientController@getLoanOfficer');

Route::get('/get-service-charge','M\ClientController@getLoanOfficer');
Route::post('/get-service-charge','M\ClientController@getLoanOfficer');


Route::get('/get-lat-lng','M\ClientController@getLatLng');
Route::post('/get-lat-lng','M\ClientController@getLatLng');

Route::get('/get-compulsory','M\LoanController@getCompulsory');
Route::post('/get-compulsory','M\LoanController@getCompulsory');

Route::post('/get-group','M\LoanController@getGroup');
Route::get('/get-group','M\LoanController@getGroup');

Route::get('/myanmar-address-state', 'Api\AddressMyanmarController@state');
Route::post('/myanmar-address-state', 'Api\AddressMyanmarController@state');

Route::get('/myanmar-address-district', 'Api\AddressMyanmarController@district');
Route::post('/myanmar-address-district', 'Api\AddressMyanmarController@district');

Route::get('/myanmar-address-township', 'Api\AddressMyanmarController@township');
Route::post('/myanmar-address-township', 'Api\AddressMyanmarController@township');

Route::get('/myanmar-address-village', 'Api\AddressMyanmarController@village');
Route::post('/myanmar-address-village', 'Api\AddressMyanmarController@village');

Route::get('/myanmar-address-ward', 'Api\AddressMyanmarController@ward');
Route::post('/myanmar-address-ward', 'Api\AddressMyanmarController@ward');


Route::post('/store-client', 'M\ClientController@storeClient');
Route::get('/store-client', 'M\ClientController@storeClient');

Route::post('/store-guarantor', 'M\ClientController@storeGuarantor');
Route::get('/store-guarantor', 'M\ClientController@storeGuarantor');


Route::post('/loan-disbursement-pending', 'M\LoanController@loanDisbursePending');
Route::get('/loan-disbursement-pending', 'M\LoanController@loanDisbursePending');



Route::get('/get-survey', 'M\ClientController@getSurvey');
Route::post('/get-survey', 'M\ClientController@getSurvey');


Route::get('/get-owner-ship-farmland', 'M\ClientController@getOwnerShipFarmland');
Route::post('/get-owner-ship-farmland', 'M\ClientController@getOwnerShipFarmland');

Route::get('/get-owner-ship', 'M\ClientController@getOwnerShip');
Route::post('/get-owner-ship', 'M\ClientController@getOwnerShip');


Route::get('/get-transaction-type', 'M\LoanController@getTransactionType');
Route::post('/get-transaction-type', 'M\LoanController@getTransactionType');

Route::get('/get-group-loan', 'M\ClientController@getGroupLoan');
Route::post('/get-group-loan', 'M\ClientController@getGroupLoan');


Route::post('/get-disbursement-number', 'M\LoanController@getDisbursementNumber');
Route::post('/get-loan', 'M\LoanController@getLoan');
Route::get('/get-loan', 'M\LoanController@getLoan');

Route::get('/get-repayment', 'M\LoanController@getRepayment');
Route::post('/get-repayment', 'M\LoanController@getRepayment');


Route::post('/get-cash', 'M\PaymentController@getCash');
Route::get('/get-cash', 'M\PaymentController@getCash');
Route::get('/get-loan-calculate', 'M\PaymentController@getLoanCalculate');


Route::post('/get-payment-number', 'M\LoanController@getPaymentNumber');

Route::post('/get-payment-show', 'M\LoanController@payment');
Route::get('/get-payment-show', 'M\LoanController@payment');


Route::post('/summary-reports', 'M\LoanController@summaryReport');
Route::get('/summary-reports', 'M\LoanController@summaryReport');



Route::post('/disbursement-reports', 'M\LoanController@disbursementReport');
Route::get('/disbursement-reports', 'M\LoanController@disbursementReport');


Route::post('/store-due-payment', 'M\LoanController@storeDuePayment');
Route::get('/store-due-payment', 'M\LoanController@storeDuePayment');


Route::post('/client-confirm', 'M\LoanController@clientConfirm');
Route::get('/client-confirm', 'M\LoanController@clientConfirm');


Route::post('/get-loan-payment', 'M\PaymentController@getLoanPayment');
Route::get('/get-loan-payment', 'M\PaymentController@getLoanPayment');

Route::post('/confirm-payment', 'M\PaymentController@confirmPayment');
Route::get('/confirm-payment', 'M\PaymentController@confirmPayment');


Route::post('/store-client-pending', 'M\LoanController@apply_loan');
Route::get('/store-client-pending', 'M\LoanController@apply_loan');



Route::post('/get-branch-code', 'M\ClientController@getBranchCode');
Route::get('/get-branch-code', 'M\ClientController@getBranchCode');

Route::post('upload-client-image',function (Request $request) {
    return $request->all();
    $id = $request->id;

    $new_name = rand() . '.png';
    $f = "uploads/images/clients/{$new_name}";
    //$fnn = 'kh'.'/'.date('Y/m/d');


    if ($request->photo_client != null && $id>0) {
        $p = $request->photo_client;

       dd($request->all());
        //$new_name = rand() . '.png';
        return [
            'hhh'=>$p.'ddddddddd'
        ];
        if(\Illuminate\Support\Facades\Storage::disk('local_public')->put($f, $p)){
            $m =  \App\Models\ClientApi::find($id);
            if($m != null){
                $m->photo_client = $f;
                if($m->save()){
                    return [
                        'id'=>$m->id
                    ];
                }
            }
        };
    }

    if ($request->nrc_photo != null && $id>0) {
        $p1 = $request->nrc_photo;
        return [
            'hhh'=>$p1.'ddddddddd'
        ];
        if(\Illuminate\Support\Facades\Storage::disk('local_public')->put($f, $p1)){
            $m1 =  \App\Models\ClientApi::find($id);
            if($m1 != null){
                $m1->nrc_photo = $f;
                if($m1->save()){
                    return [
                        'id'=>$m1->id
                    ];
                }
            }
        };
    }
    //return ['id'=>0];
});



//Route::post('upload-page-image',function (Request $request) {
//    //return $request->all();
//    $id = $request->id;
//    $fnn = 'kh'.'/'.date('Y/m/d');
//    if ($request->photo != null && $id>0) {
//        $p = $request->photo;
//        $new_name = rand() . '.png';
//        $f = "uploads/pages/{$fnn}/{$new_name}";
//        if(Storage::disk('dropbox')->put($f, $p)){
//            $m =  \App\Models\TripPlaceApi::find($id);
//            if($m != null){
//                $arr = [];
//                $photo_album = $m->photo_album;
//                if($photo_album != null && is_string($photo_album)){
//                    if($photo_album != '') {
//                        $arr = json_decode($photo_album);
//                    }
//                }else if($photo_album != null && is_array($photo_album)){
//                    $arr = $photo_album;
//                }
//                $arr[] = $f;
//                $m->photo_album = $arr;
//                if($m->save()){
//                    return ['id'=>$m->id];
//                }
//            }
//        };
//    }
//    return ['id'=>0];
//});














