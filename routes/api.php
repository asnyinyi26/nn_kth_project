<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/product-category', 'Api\CategoryController@index');
Route::get('/product-category/{id}', 'Api\CategoryController@show');

Route::get('/account-section', 'Api\AccSectionController@index');
Route::get('/account-section/{id}', 'Api\AccSectionController@show');

Route::get('/account-chart', 'Api\AccChartController@index');
Route::get('/account-chart/{id}', 'Api\AccChartController@show');

Route::get('/acc-chart-profit', 'Api\AccChartProfitController@index');
Route::get('/acc-chart-profit/{id}', 'Api\AccChartProfitController@show');


Route::get('/account-cash', 'Api\AccCashController@index');
Route::get('/account-cash/{id}', 'Api\AccCashController@show');

Route::get('/acc-chart-expense', 'Api\AccChartExpenseController@index');
Route::get('/acc-chart-expense/{id}', 'Api\AccChartExpenseController@show');


Route::get('/account-sub-section', 'Api\AccSubSectionController@index');
Route::get('/account-sub-section/{id}', 'Api\AccSubSectionController@show');

Route::get('/unit', 'Api\UnitController@index');
Route::get('/unit/{id}', 'Api\UnitController@show');

//------------------------------------------------

Route::get('/get-loan-calculate', 'Admin\LoanCalculatorCrudController@getLoanCalculation');

Route::get('/accrue-interest', 'Api\AccrueInterestCompulsory@index');

//================================

Route::get('/myanmar-address-state', 'Api\AddressMyanmarController@state');
Route::get('/myanmar-address-district', 'Api\AddressMyanmarController@district');
Route::get('/myanmar-address-township', 'Api\AddressMyanmarController@township');
Route::get('/myanmar-address-village', 'Api\AddressMyanmarController@village');
Route::get('/myanmar-address-ward', 'Api\AddressMyanmarController@ward');

Route::get('repayment-order',function (){
    return roundNum(1254.999999);
    \App\Helpers\MFS::getRepaymentAccount(1,100,10,50,10,20,150);
});

Route::get('/update-activate-date', function (){
    $disburse = \App\Models\PaidDisbursement::all();
    if($disburse != null){
        foreach ($disburse as $dis){
            $loan = \App\Models\Loan2::find($dis->contract_id);
            if($loan != null){
                $loan->status_note_activated = $disburse->paid_disbursement_date;
                $loan->save();
            }
        }
    }
});
Route::get('/frd',function (){
   $amt = \App\Helpers\ACC::getFrdAccountAmount('pl-001');
   return $amt;
});
