<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('backpack::inc.head')
    <style>
        .sep-p {
            padding: 0;
            margin-top: -5px;
            margin-right: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
            height: 1px;
        }
        .help-block {
            display: none;
        }

        .callout.callout-danger {
            line-height: 30px;
        }

        .text-capitalize {
            font-family: Content !important;
            font-size: 16px;
        }

        small {
            font-family: Content !important;
        }

        .box-title {
            font-family: Content !important;
        }

        .text-capitalize-bar {
            font-family: Content !important;
            font-size: 16px;
            font-weight: bold;
        }

        .logo-lg {
            font-family: Moul;
            font-size: 18px !important;

        }

        .skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side {
            background-color: #434343;
        }




    </style>
</head>
<body class="hold-transition @if(!(request()->is_frame >0)) {{ config('backpack.base.skin') }} @endif sidebar-mini" style="font-family: 'Content', Titillium Web;">
	<script type="text/javascript">
		/* Recover sidebar state */
		(function () {
			if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
				var body = document.getElementsByTagName('body')[0];
				body.className = body.className + ' sidebar-collapse';
			}
		})();
	</script>
    <!-- Site wrapper -->
    <div class="wrapper">
    @if(!(request()->is_frame >0))
        @include('backpack::inc.main_header')
    @endif
    <!-- =============================================== -->
    @if(!(request()->is_frame >0))
    @section('sidebar')

            @include('backpack::inc.sidebar')

    @show
    @endif
      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
        <div class="{{ (request()->is_frame >0?'':'content-wrapper') }}">
        <!-- Content Header (Page header) -->
         @yield('header')

        <!-- Main content -->
        <section class="content">

          @yield('content')

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      @if(!(request()->is_frame >0))
      <footer class="main-footer text-sm clearfix">
        @include('backpack::inc.footer')
      </footer>
      @endif
    </div>
    <!-- ./wrapper -->


    @yield('before_scripts')
    @stack('before_scripts')

    @include('backpack::inc.scripts')
    @include('backpack::inc.alerts')

    @yield('after_scripts')
    @stack('after_scripts')

    <script>
        /* Store sidebar state */
        $('.sidebar-toggle').click(function(event) {
          event.preventDefault();
          if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
            sessionStorage.setItem('sidebar-toggle-collapsed', '');
          } else {
            sessionStorage.setItem('sidebar-toggle-collapsed', '1');
          }
        });

        // Set active state on menu element
        var current_url = "{{ Request::fullUrl() }}";
        var full_url = current_url+location.search;
        var $navLinks = $("ul.sidebar-menu li a");
        // First look for an exact match including the search string
        var $curentPageLink = $navLinks.filter(
            function() { return $(this).attr('href') === full_url; }
        );
        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();
        function round(value, exp) {
            if (typeof exp === 'undefined' || +exp === 0)
                return Math.round(value);

            value = +value;
            exp = +exp;

            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
                return NaN;

            // Shift
            value = value.toString().split('e');
            value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

            // Shift back
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
        }
        // If not found, look for the link that starts with the url
        if(!$curentPageLink.length > 0){
            $curentPageLink = $navLinks.filter(
                function() { return $(this).attr('href').startsWith(current_url) || current_url.startsWith($(this).attr('href')); }
            );
        }

        $curentPageLink.parents('li').addClass('active');
    </script>

    <script>

        $(function () {
            $('.help-block').each(function () {
                $(this).hide();
            });

            $('[number="number"]').keypress(function (event) {
                return isNumber(event, this);
            });


            $('[number="number"]').bind('paste', function() {
                var el = this;
                setTimeout(function() {
                    el.value = el.value.replace('[a-z] + [^0-9\s.]+|.(?!\d)','');
                    // el.value = el.value.replace(/[^0-9.]/g, '');
                    // el.value = el.value.replace(/\D/g, '');
                }, 0);
            });


            $('body').on('keypress','[number="number"]',function () {
                return isNumber(event, this);
            });

            // getNewNotifications();

        });

        function isNumber(evt, element) {

            var charCode = (evt.which) ? evt.which : event.keyCode;
            //console.log(charCode);
            if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode != 37 || $(element).val().indexOf('%') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function addCommas(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        $(function () {

           /* $('form').on('submit',function(e){
                var error = 0;
                $('.required').find(':input').each(function () {
                    var input = $(this).val();
                    if(input == ''){
                        var name = $(this).prop('name');

                        $(this).focus();
                        new PNotify({
                            title: ("Require"),
                            text: name + " is not empty!!",
                            type: "warning"
                        });
                        error = 1;
                        return false;
                    }
                });

                if(error>0){
                    e.preventDefault();
                    $('button').prop("disabled", false);
                    new PNotify({
                        title: ("Require"),
                        text: "* is not empty!!",
                        type: "warning"
                    });
                    return false;
                }
            });*/



            $('.change-branch-top').on('change',function () {
                var branch_id = $(this).val();

                $.ajax({
                    type: 'GET',
                    url: '{{url('/api/change-branch')}}',
                    data: {
                        branch_id: branch_id,
                    },
                    success: function (res) {
                        window.location.reload();
                    }

                });
            });

            //$('input:textbox').val("");

        });

    </script>

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>--}}
    <script src="{{ asset('vendor/adminlte') }}/plugins/jquery/jquery.jscroll.min.js" ></script>

    <script type="text/javascript">
    // $( document ).ready(function() {
    //     console.log( "document loaded" );
    //     $(".infinite-scroll").scroll(function() {
    //         // if($(window).scrollTop() + $(window).height() >= $("#product_list").height()) {
    //
    //         alert("hi");
    //         // }
    //     });
    // });
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            debug: true,
            autoTrigger: true,
            loadingHtml: '<!--<img class="center-block" src="/images/loading.gif" alt="Loading..." />-->',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'ul.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
                // $('div.jscroll-added').removeClass('menu');
                $("div.jscroll-added ul.menu>li>a").css({"padding": 0, "border-bottom": "none"});
                $("div.jscroll-added ul.infinite-scroll").css({"overflow": "hidden"});
                // window.scrollTo(0,0);
            }
        });
    });
</script>

    <!-- JavaScripts -->
    {{-- <script src="{{ mix('js/app.js') }}"></script> --}}
</body>
</html>
