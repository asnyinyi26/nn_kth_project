<?php $u_key = rand(1,100).time().rand(1,100); ?>
<div class="row" style="padding: 0 15px 0 15px">
<div class="form-group col-md-8" >
    <label>{{_t('product')}} / {{ _t('service') }}</label>
    @if(isset($field['prefix']) || isset($field['suffix']) || true) <div class="input-group"> @endif
        @if(isset($field['prefix'])) <div class="input-group-addon">{!! $field['prefix'] !!}</div> @endif
    <select   style="width: 100%"   id="product_select2_ajax_{{ $u_key }}" class="form-control" ></select>
        @if(isset($field['suffix']) || true) <div class="input-group-addon">
            <a href="" data-remote="false" data-toggle="modal" data-target="#show-create-product"><span class="glyphicon glyphicon-plus"></span></a>
        </div> @endif
        @if(isset($field['prefix']) || isset($field['suffix']) || true) </div> @endif
</div>

<div class="form-group col-md-2" >
    <label>{{_t('category')}}</label>
    <select   style="width: 100%"   id="category_select2_ajax_{{ $u_key }}" class="form-control" ></select>
</div>


<div class="form-group col-md-2" >
    <label>{{_t('group')}}</label>
    <select   style="width: 100%"   id="group_select2_ajax_{{ $u_key }}" class="form-control" ></select>
</div>
</div>
{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    <div class="modal fade" id="show-create-product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-x">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{_t('Inventory')}}</h4>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="315" src="" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
        <style>
            .modal-dialog-x {
                width: 95%;
                padding: 0;
            }

            .modal-content {
                width: 98%;
            }
        </style>
    @if($field['no_script']=='yes')
    <!-- include select2 css-->
    <link href="{{ asset('vendor/adminlte/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('vendor/adminlte/plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    {{-- allow clear --}}
    @endif
    <style type="text/css">
        .select2-selection__clear::after {
            content: ' {{ trans('backpack::crud.clear') }}';
        }
    </style>

    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
    <!-- include select2 js-->
    @if($field['no_script']=='yes')
        <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
    @endif
    @endpush

@endif

<!-- include field specific select2 js-->
@push('crud_fields_scripts')
<script>

    jQuery(document).ready(function($) {
        // trigger select2 for each untriggered select2 box
        $("#group_select2_ajax_{{ $u_key }}").each(function (i, obj) {
            if (!$(obj).hasClass("select2-hidden-accessible"))
            {

                $(obj).select2({
                    theme: 'bootstrap',
                    multiple: false,
                    placeholder: "{{ _t('select group') }}",
                    minimumInputLength: 0,
                    allowClear: true,
                    ajax: {
                        url: "{{ url("api/product-group") }}",
                        dataType: 'json',
                        quietMillis: 250,
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            var result = {
                                results: $.map(data.data, function (item) {
                                    return {
                                        text: item['name'],
                                        id: item['id']
                                    }
                                }),
                                more: data.current_page < data.last_page
                            };

                            return result;
                        },
                        cache: true
                    },
                })
                {{-- allow clear --}}

                .on('select2:unselecting', function(e) {
                    $(this).val('').trigger('change');
                    // console.log('cleared! '+$(this).val());
                    e.preventDefault();
                });

            }
        });

        $("#category_select2_ajax_{{ $u_key }}").each(function (i, obj) {
            if (!$(obj).hasClass("select2-hidden-accessible"))
            {

                $(obj).select2({
                    theme: 'bootstrap',
                    multiple: false,
                    placeholder: "{{ _t('select category') }}",
                    minimumInputLength: 0,
                    allowClear: true,
                    ajax: {
                        url: "{{ url("api/product-category") }}",
                        dataType: 'json',
                        quietMillis: 250,
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            var result = {
                                results: $.map(data.data, function (item) {
                                    return {
                                        text: item['name'],
                                        id: item['id']
                                    }
                                }),
                                more: data.current_page < data.last_page
                            };

                            return result;
                        },
                        cache: true
                    },
                })
                {{-- allow clear --}}

                .on('select2:unselecting', function(e) {
                    $(this).val('').trigger('change');
                    // console.log('cleared! '+$(this).val());
                    e.preventDefault();
                });

            }
        });

        $("#product_select2_ajax_{{ $u_key }}").each(function (i, obj) {
            if (!$(obj).hasClass("select2-hidden-accessible"))
            {
                //var warehouse_id_{{ $u_key }} = $('[name="warehouse_id"]').val();
                $(obj).select2({
                    theme: 'bootstrap',
                    multiple: false,
                    placeholder: "{{ _t('select product') }}",
                    minimumInputLength: 0,
                    allowClear: true,
                    ajax: {
                        url: "{{ url("api/product") }}",
                        dataType: 'json',
                        quietMillis: 250,
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page,
                                group_id: $("#group_select2_ajax_{{ $u_key }}").val(),
                                category_id: $("#category_select2_ajax_{{ $u_key }}").val(),
                                //warehouse_id:  ($('[name="warehouse_id"]').length > 0?$('[name="warehouse_id"]').val():0)
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            var result = {
                                results: $.map(data.data, function (item) {
                                    return {
                                        text: item['sku'] +'-'+ item['product_name'],
                                        id: item['id']
                                    }
                                }),
                                more: data.current_page < data.last_page
                            };

                            return result;
                        },
                        cache: true
                    },
                })
                {{-- allow clear --}}

                .on('select2:unselecting', function(e) {
                    $(this).val('').trigger('change');
                    // console.log('cleared! '+$(this).val());
                    e.preventDefault();
                })
                .on('change', function(e) {
                    var product_id =  $(this).val();
                    if(product_id>0) {
                        @if(isset($field['script_run']))
                            {{ $field['script_run'] }}(product_id);
                        @endif
                        $(this).val('');
                        $("#product_select2_ajax_{{ $u_key }}").val('');
                    }
                    e.preventDefault();
                })

                ;

            }
        });
    });
</script>
<script>
    $("#show-create-product").on("show.bs.modal", function(e) {
        var link = '{{url('admin/product-inventory/create?is_frame=1')}}';
        $(this).find('iframe').attr('src',link);
    });
</script>
@endpush
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
