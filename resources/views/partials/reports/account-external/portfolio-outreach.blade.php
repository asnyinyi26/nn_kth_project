<div id="DivIdToPrint">
    @include('partials.reports.header',
    ['report_name'=>'Portfolio And Outreach','from_date'=>$start_date,'to_date'=>$end_date,'use_date'=>1])

    <table class="table-data" id="table-data">
        <tbody>

        </tbody>
    </table>

    <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="1284"
           style="width:963.0pt;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt">
        <tbody>
        <tr style="mso-yfti-irow:4;height:15.8pt">
            <td width="44" nowrap="" valign="bottom"
                style="width:32.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="50" nowrap="" valign="bottom"
                style="width:37.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="80" nowrap="" valign="bottom"
                style="width:59.9pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" colspan="4" valign="bottom"
                style="width:42.7pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" colspan="2" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" colspan="2" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt; height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="75" nowrap="" style="width:56.4pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="150" nowrap="" colspan="2"
                style="text-align: right;width:112.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt">
                <p class="MsoNormal"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:-439pt;mso-char-indent-count:5.0;line-height:normal"><span
                            style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Date (:dd-mmm-<span class="SpellE">yyyy</span>:)</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="33" nowrap="" style="width:24.4pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt; height:15.8pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height: normal"><span style="font-size:9.0pt;font-family:'Times New Roman',serif; mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span></p>
            </td>
        </tr>


        <tr style="mso-yfti-irow:5;height:15.8pt">
            <td width="44" nowrap="" valign="bottom"
                style="width:32.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="50" nowrap="" valign="bottom"
                style="width:37.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="80" nowrap="" valign="bottom"
                style="width:59.9pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" colspan="4" valign="bottom"
                style="width:42.7pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" colspan="2" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" colspan="2" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="75" nowrap="" style="width:56.4pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="150" nowrap="" colspan="2"
                style="text-align: right;width:112.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt">
                <p class="MsoNormal"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:-439pt;mso-char-indent-count:5.0;line-height:normal">
                    <span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Reporting Period:</span>
                </p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="33" nowrap=""
                style="width:24.4pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal">
                    <span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span>
                </p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:6;height:12.4pt">
            <td width="44" nowrap="" valign="bottom"
                style="width:32.8pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="50" nowrap="" valign="bottom"
                style="width:37.8pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="80" nowrap="" valign="bottom"
                style="width:59.9pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="114" nowrap="" colspan="5" valign="top"
                style="width:85.5pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (1%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="4" valign="top"
                style="width:85.1pt;border:none; border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (10%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (50%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (75%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (100%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (50%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (100%)</span>
                </p>
            </td>
            <td width="75" nowrap="" valign="bottom"
                style="width:56.4pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="72" nowrap="" valign="bottom" style="width:.75in;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="78" nowrap="" valign="bottom"
                style="width:58.8pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="33" nowrap="" valign="bottom"
                style="width:24.4pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
        </tr>


        <tr style="mso-yfti-irow:7;height:28.3pt">
            <td width="44" rowspan="2" valign="top"
                style="width:32.8pt;border:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;mso-border-alt:solid windowtext .5pt; mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">No.</span></b>
                </p>
            </td>
            <td width="50" rowspan="2" valign="top"
                style="width:37.8pt;border-top:solid windowtext 1.0pt;border-left:none;border-bottom:solid black 1.0pt;border-right:solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Region</span></b>
                </p>
            </td>
            <td width="80" rowspan="2" valign="top"
                style="width:59.9pt;border-top:solid windowtext 1.0pt;border-left:none;border-bottom:solid black 1.0pt;border-right:solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Loan Outstanding</span></b>
                </p>
            </td>
            <td width="114" colspan="5" valign="top"
                style="width:85.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Current</span></b>
                </p>
            </td>
            <td width="113" colspan="4" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Sub-Standard (1-30) Days *NPL</span></b>
                </p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Watch (31 - 60) Days *NPL</span></b>
                </p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Doubtful (61 - 90) Days *NPL</span></b>
                </p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt; mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt; padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Loss <span
                                    class="GramE">( Over</span> 91 ) Days *NPL</span></b></p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Rescheduled 1 Time</span></b>
                </p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Rescheduled 2 Time</span></b></p>
            </td>
            <td width="75" valign="top" style="width:56.4pt;border-top:solid windowtext 1.0pt;border-left:none;border-bottom:solid black 1.0pt;border-right:none;mso-border-left-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt; padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Total *NPL</span></b></p>
            </td>
            <td width="72" valign="top" style="width:.75in;border-top:solid windowtext 1.0pt; border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;border-right:none;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:28.3pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;
 font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';
  mso-bidi-language:KHM">Loan Loss Reserve</span></b></p>
            </td>
            <td width="78" valign="top" style="width:58.8pt;border-top:solid windowtext 1.0pt;
  border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;
  border-right:none;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:28.3pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal"><b><span style="font-size:9.0pt;
  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';
  mso-bidi-language:KHM">Total *NPL / Loan Outstanding</span></b></p>
            </td>
            <td width="90" colspan="2" valign="top" style="width:67.2pt;border:solid windowtext 1.0pt;
  border-right:solid black 1.0pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:
  28.3pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal"><b><span style="font-size:9.0pt;
  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';
  mso-bidi-language:KHM">Write Off</span></b></p>
            </td>
        </tr>


        <tr style="mso-yfti-irow:8;height:39.8pt">
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="57" colspan="4" valign="top" style="width:42.7pt;border-top:none; border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" colspan="2" valign="top" style="width:42.8pt;border-top:none; border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" colspan="2" valign="top" style="width:42.3pt;border-top:none;  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt;  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';  mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none;  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none;  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="75" style="width:56.4pt;border:none;border-bottom:solid black 1.0pt;  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;  mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:39.8pt"></td>
            <td width="72" style="width:.75in;border-top:none;border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;border-right:none;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:39.8pt"></td>
            <td width="78" style="width:58.8pt;border-top:none;border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;border-right:none;mso-border-top-alt:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:39.8pt"></td>
            <td width="57" valign="top" style="width:42.8pt;border:solid windowtext 1.0pt; border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt: solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding: 0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="33" valign="top" style="width:24.4pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
        </tr>



        {{--no 1--}}

        <tr style="mso-yfti-irow:9;height:22.45pt">
            <td width="44" nowrap="" style="width:32.8pt;border:solid windowtext 1.0pt; border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt: solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding: 0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp; </span>1 </span></p>
            </td>
            <td width="50" nowrap="" style="width:37.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height: normal"><span style="font-size:9.0pt;font-family:'Times New Roman',serif; mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span></p>
            </td>
            <td width="80" nowrap="" style="width:59.9pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;</span></p>
            </td>
            <td width="57" nowrap="" colspan="4" style="width:42.7pt;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span></p>
            </td>
            <td width="57" nowrap="" colspan="2" style="width:42.8pt;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;</span></p>
            </td>
            <td width="56" nowrap="" colspan="2" style="width:42.3pt;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;</span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;</span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;</span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:  KHM">&nbsp;</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM">&nbsp;</span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM">&nbsp;</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM">&nbsp;</span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM">&nbsp;</span></p>
            </td>
            <td width="75" nowrap="" style="width:56.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM"><span style="mso-spacerun:yes">&nbsp; </span></span></p>
            </td>
            <td width="72" nowrap="" style="width:.75in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM"><span style="mso-spacerun:yes">&nbsp; </span></span></p>
            </td>
            <td width="78" nowrap="" style="width:58.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM">&nbsp;</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM">&nbsp;</span></p>
            </td>
            <td width="33" nowrap="" style="width:24.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM">&nbsp;</span></p>
            </td>
        </tr>


        <tr style="mso-yfti-irow:24;height:22.45pt">
            <td width="44" nowrap="" style="width:32.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span></p>
            </td>
            <td width="50" nowrap="" style="width:37.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal"><b><span style="font-size:9.0pt;
  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';
  mso-bidi-language:KHM">Total</span></b></p>
            </td>
            <td width="80" nowrap="" style="width:59.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" colspan="4" style="width:42.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" colspan="2" style="width:42.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;</span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;
  </span></span></b></p>
            </td>
            <td width="56" nowrap="" colspan="2" style="width:42.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span><span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;
  </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="75" nowrap="" style="width:56.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></p>
            </td>
            <td width="72" nowrap="" style="width:.75in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></p>
            </td>
            <td width="78" nowrap="" style="width:58.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="33" nowrap="" style="width:24.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
        </tr>


        </tbody>
    </table>

</div>
