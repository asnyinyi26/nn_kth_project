<div id="DivIdToPrint">
    @include('partials.reports.header',
    ['report_name'=>'Balance Sheet','from_date'=>$start_date,'to_date'=>$end_date,'use_date'=>1])

    <table class="table-data" id="table-data">
        <tbody>

        </tbody>
    </table>

    <div class="panel-body" style="padding: 0px !important;">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 col-p-6">&nbsp;</div>
                    <div class="col-md-4 col-p-4">
                        <p class="text-right">
                            <span style="font-family:&quot;Times New Roman&quot;,serif;">Date (dd-mmm-yyyy) :</span>
                        </p>
                    </div>
                    <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;">dd-mm-yyyy</div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6 col-p-6">&nbsp;</div>
                    <div class="col-md-4 col-p-4">
                        <p class="text-right">
                            <span style="font-family:&quot;Times New Roman&quot;,serif;">Reporting Period:</span>
                        </p>
                    </div>
                    <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;">mm</div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center" rowspan="2" style="border: solid windowtext 1.0pt;width:1%;">
                        No.
                    </th>
                    <th class="text-center" rowspan="2" style="border: solid windowtext 1.0pt;width:25%;">
                        Region
                    </th>
                    <th class="text-center" colspan="2" style="border: solid windowtext 1.0pt;width:11%;">
                        Compulsory Saving
                    </th>
                    <th class="text-center" colspan="2" style="border: solid windowtext 1.0pt;width:10%;">
                        Voluntary  Saving
                    </th>
                    <th class="text-center" colspan="2" style="border: solid windowtext 1.0pt;width:12%;">
                        Total
                    </th>
                </tr>
                <tr>
                    <th class="text-center" style="border: solid windowtext 1.0pt;width:11%;">
                        Number of Clients
                    </th>
                    <th class="text-center" style="border: solid windowtext 1.0pt;width:11%;">
                        Amount
                    </th>
                    <th class="text-center" style="border: solid windowtext 1.0pt;width:11%;">
                        Number of Clients
                    </th>
                    <th class="text-center" style="border: solid windowtext 1.0pt;width:11%;">
                        Amount
                    </th>
                    <th class="text-center" style="border: solid windowtext 1.0pt;width:11%;">
                        Number of Clients
                    </th>
                    <th class="text-center" style="border: solid windowtext 1.0pt;width:11%;">
                        Amount
                    </th>
                </tr>
                </thead>
                <tbody>

                <tr >
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">1</td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">

                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        0
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        0
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">0</td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">0</td>
                </tr>

                <tr>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        2
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                </tr>
                <tr>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        3
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                </tr>
                <tr>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        4
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                </tr>
                <tr>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        5
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                </tr>
                <tr>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        6
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                </tr>
                <tr>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        7
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                </tr>
                <tr>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        8
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                </tr>
                <tr>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        9
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                </tr>
                <tr>
                    <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                        10
                    </td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                    <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <th style="border: solid windowtext 1.0pt;"></th>
                    <th class="text-center" style="border: solid windowtext 1.0pt;">Total</th>
                    <th class="text-right" style="border: solid windowtext 1.0pt;">0</th>
                    <th class="text-right" style="border: solid windowtext 1.0pt;">0</th>
                    <th class="text-right" style="border: solid windowtext 1.0pt;">-</th>
                    <th class="text-right" style="border: solid windowtext 1.0pt;">-</th>
                    <th class="text-right" style="border: solid windowtext 1.0pt;">0</th>
                    <th class="text-right" style="border: solid windowtext 1.0pt;">0</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 col-p-6">&nbsp;</div>
                    <div class="col-md-4 col-p-4">
                        <p class="text-right">
                            <span style="font-family:&quot;Times New Roman&quot;,serif;">Prepared by (Name/Signature) :</span>
                        </p>
                    </div>
                    <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;">&nbsp;</div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6 col-p-6">&nbsp;</div>
                    <div class="col-md-4 col-p-4">
                        <p class="text-right">
                            <span style="font-family:&quot;Times New Roman&quot;,serif;">Checked by (Name/Signature):</span>
                        </p>
                    </div>
                    <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;"> &nbsp;</div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6 col-p-6">&nbsp;</div>
                    <div class="col-md-4 col-p-4">
                        <p class="text-right">
                            <span style="font-family:&quot;Times New Roman&quot;,serif;">Approved by (Name/Signature):</span>
                        </p>
                    </div>
                    <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;"> &nbsp;</div>
                </div>
            </div>
        </div>
    </div>


</div>
