<div id="DivIdToPrint">
    @include('partials.reports.header',
    ['report_name'=>'Balance Sheet','from_date'=>$start_date,'to_date'=>$end_date,'use_date'=>1])

    <table class="table-data" id="table-data">
        <tbody>

        </tbody>
    </table>


        <table>
                <thead class="no-border">
                <tr class="text-center no-border">
                    <th colspan="5" class="text-right no-border"></th>
                    <th class="text-right no-border">Date (dd-mmm-yyyy) :</th>
                    <th class="text-left no-border">
                        <div style="border-bottom: 1px solid #333;">dd-mm-yyyy</div>
                    </th>
                </tr>
                <tr class="text-center no-border">
                    <th colspan="5" class="text-right no-border"></th>
                    <th class="text-right no-border">Reporting Period :</th>
                    <th class="text-left no-border">
                        <div style="border-bottom: 1px solid #333;">mm</div>
                    </th>
                </tr>
                <tr class=" border table-data" id="table-data">
                    <th class="text-center border" style="width:1%;">No.</th>
                    <th class="text-center border" style="width:22%;">Depositors' Name</th>
                    <th class="text-center border" style="width:11%;">Compulsory Saving</th>
                    <th class="text-center border" style="width:10%;">Voluntary  Saving</th>
                    <th class="text-center border" style="width:15%;">Customer's Deposits (Compulsory Saving + Voluntary Saving)</th>
                    <th class="text-center border" style="width:12%;">Compulsory Saving Over Customer's Deposits (%)</th>
                    <th class="text-center border" style="width:12%;">Voluntary  Saving Over Customer's Deposits (%)</th>
                </tr>
                </thead>
                <tbody class=" border table-data" id="table-data">
                <tr>
                    <td class="text-center border">1</td>
                    <td></td>
                    <td class="text-right border">
                        0
                    </td>
                    <td class="text-right border">
                        -
                    </td>
                    <td class="text-right border">
                        0
                    </td>
                    <td class="text-right border">0%</td>
                    <td class="text-right border">0%</td>
                </tr>

 <tr>
                    <td class="text-center border">2</td>
                    <td></td>
                    <td class="text-right border">
                        0
                    </td>
                    <td class="text-right border">
                        -
                    </td>
                    <td class="text-right border">
                        0
                    </td>
                    <td class="text-right border">0%</td>
                    <td class="text-right border">0%</td>
                </tr>

 <tr>
                    <td class="text-center border">3</td>
                    <td></td>
                    <td class="text-right border">
                        0
                    </td>
                    <td class="text-right border">
                        -
                    </td>
                    <td class="text-right border">
                        0
                    </td>
                    <td class="text-right border">0%</td>
                    <td class="text-right border">0%</td>
                </tr>


                </tbody>
                <tfoot>
                <tr class=" border table-data" id="table-data">
                    <th class="text-center border"></th>
                    <th class="text-center border">Total</th>
                    <th class="text-right border">0</th>
                    <th class="text-right border">-</th>
                    <th class="text-right border">0</th>
                    <th class="text-right border">
                        0%
                    </th>
                    <th class="text-right border">
                        0%
                    </th>
                </tr>
                <tr class="text-center no-border">
                    <th colspan="4" class="text-right no-border"></th>
                    <th colspan="2" class="text-right no-border">Prepared by (Name/Signature) :</th>
                    <th class="text-center no-border">
                        <div style="border-bottom: 1px solid #333;">&nbsp;</div>
                    </th>
                </tr>
                <tr class="text-center no-border">
                    <th colspan="4" class="text-right no-border"></th>
                    <th colspan="2" class="text-right no-border">Checked by (Name/Signature) :</th>
                    <th class="text-center no-border">
                        <div style="border-bottom: 1px solid #333;">&nbsp;</div>
                    </th>
                </tr>
                <tr class="text-center no-border">
                    <th colspan="4" class="text-right no-border"></th>
                    <th colspan="2" class="text-right no-border">Approved by (Name/Signature) :</th>
                    <th class="text-center no-border">
                        <div style="border-bottom: 1px solid #333;">&nbsp;</div>
                    </th>
                </tr>
                </tfoot>
        </table>

</div>
