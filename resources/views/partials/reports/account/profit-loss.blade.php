<div class="modal fade" id="show-detail-modal" tabindex="-1" role="dialog" aria-labelledby="show-detail-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="DivIdToPrintPop">
        
      </div>
      <div class="action" style="float: right;">
        <button type="button" onclick="printDiv() " style="cursor: pointer; background: #0b58a2;padding: 10px 20px; color: #fff; margin-bottom: 10px;">PRINT</button>
    </div>
    </div>
  </div>
</div>
<style>
a{
    text-decoration:underline;
    cursor: pointer;
}
</style>
<input type="hidden" id="start_date" value="{{$start_date}}">
<input type="hidden" id="end_date" value="{{$end_date}}">
<div id="DivIdToPrint">
@if($bals != null)
    <?php

    ?>
    @php
        $today = date('Y-m-d');
    @endphp
    @include('partials.reports.header',
    ['report_name'=>'Profit Loss','from_date'=>$start_date,'to_date'=>$end_date,'use_date'=>1])
    <div>{{$today}}</div>
    @foreach($branches as $branch)
        <div><span class="pull-right" style="margin-bottom:4px;">{{\App\Models\Branch::find($branch)->title}},</span></div>
    @endforeach
    <table border="1" class="table-data" id="table-data">
        <thead>
        <th></th>
        <?php $sp = count($branches) +1; ?>
        @foreach($branches as $branch)
            <th>{{ optional(\App\Models\Branch::find($branch))->title  }}</th>
        @endforeach
        </thead>

        <tbody>

        {{--Ordinary Income--}}
            <tr>
                <td colspan="{{count($branches)+1}}" style="color: blue;"><b>Ordinary Income / Expense</b></td>
            </tr>

            @php
                //$total_income += ($bal??0);
                $total_income = [];
            @endphp

            @if(isset($bals[40]))
                @if(count($bals[40])>0)
                    <tr>
                        <td colspan="{{count($branches)+1}}" style="padding-left: 30px"><b>Income</b></td>
                    </tr>

                    @forelse($bals[40] as $acc_id => $bal)

                        <tr>
                            <td style="padding-left: 60px;"> 
                            {{-- <a class="general-leg40" id="{{ $acc_id }}" data-toggle="modal" data-target="#show-detail-modal"> --}}
                            {{ optional(\App\Models\AccountChart::find($acc_id))->code."-".optional(\App\Models\AccountChart::find($acc_id))->name }}
                                {{-- </a> --}}
                            </td>
                            @foreach($branches as $b_id)
                                <?php

                                if(!isset($total_income[$b_id])) $total_income[$b_id] = 0;

                                    $b = isset($bal[$b_id])?($bal[$b_id]??0):0;
                                    $total_income[$b_id] += ($b??0);

                                ?>
                                <td style="text-align: right">
                                    {{-- <a  class="general-leg40" id="{{ $acc_id }}" data-toggle="modal" data-target="#show-detail-modal"> --}}
                                        {{ number_format(-$b??0,2) }}
                                    {{-- </a> --}}
                                </td>
                            @endforeach

{{--                            <td style="text-align: right">{{ number_format(-$bal??0,2) }}</td>--}}
                        </tr>
                        @endforeach


                        <tr>
                            <td style="padding-left: 30px"><b>Total Income</b></td>
                            @foreach($branches as $b_id)
                                {{--<td style="text-align: right">{{ number_format(-$b??0,2) }}</td>--}}
                                <td style="text-align: right;padding-right: 10px"><u>{{ number_format(-$total_income[$b_id],2) }}</u></td>

                            @endforeach
                        </tr>



                @endif
            @endif



                        {{--Cost of Goods Sold--}}


                        @php
                            $total_cogs = [];
                            $gross_profit = [];
                        @endphp
                        @if(isset($bals[50]))
                            @if(count($bals[50])>0)
                                <tr>
                                    <td colspan="{{count($branches)+1}}" style="padding-left: 30px"><b>Cost of Goods Sold</b></td>
                                </tr>
                                @forelse($bals[50] as $acc_id => $bal)

                                    <tr>
                                        <td style="padding-left: 60px;">
                                        {{-- <a class="general-leg50" id="{{ $acc_id }}" data-toggle="modal" data-target="#show-detail-modal" style="text-decoration:underline;"> --}}
                                        {{ optional(\App\Models\AccountChart::find($acc_id))->code."-".optional(\App\Models\AccountChart::find($acc_id))->name }}
                                        {{-- </a> --}}
                                        </td>

                                        @foreach($branches as $b_id)
                                            <?php
                                            if(!isset($total_cogs[$b_id])) $total_cogs[$b_id] = 0;
                                            $b = isset($bal[$b_id])?($bal[$b_id]??0):0;
                                            $total_cogs[$b_id] += ($b??0);
                                            ?>
                                            <td style="text-align: right"><a  class="general-leg50" id="{{ $acc_id }}" data-toggle="modal" data-target="#show-detail-modal">{{ number_format($b??0,2) }}</a></td>
                                        @endforeach


                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td style="padding-left: 30px"><b>Total Cost of Goods Sold</b></td>
                                        @foreach($branches as $b_id)
                                            <td style="text-align: right;padding-right: 10px"><u>{{ number_format($total_cogs[$b_id],2) }}</u></td>
                                        @endforeach
                                    </tr>
                                    @endif
                                    @endif


                                    <tr>

                                        <?php
                                        $n=0;
                                        ?>
                                        @foreach($branches as $b_id)

                                            <?php
                                                if(!isset($gross_profit[$b_id])) $gross_profit[$b_id] = 0;
                                                $gross_profit[$b_id] = ($total_income[$b_id]??0) + ($total_cogs[$b_id]??0);
                                                $n++;
                                            ?>
                                            @if ($n > 1)
                                                <td style="color: green;padding-left: 20px"><b>{{ $gross_profit[$b_id] < 0?'Gross Profit':'Net Loss' }}</b> <u>{{ $gross_profit[$b_id] < 0?number_format(-$gross_profit[$b_id],2):number_format($gross_profit[$b_id],2) }}</u></td>
                                            @else
                                                <td style="color: green;padding-left: 20px"><b>{{ $gross_profit[$b_id] < 0?'Gross Profit':'Net Loss' }}</b> </td>
                                                <td style="color: green;text-align: right;padding-right: 20px"><u>{{ $gross_profit[$b_id] < 0?number_format(-$gross_profit[$b_id],2):number_format($gross_profit[$b_id],2) }}</u></td>
                                            @endif

                                        @endforeach
                                    </tr>



                                    {{--Close Cost of good sold--}}



                                    {{--Expense--}}

                                    @php
                                        $total_expense = [];
                                        $net_ordinary = [];
                                    @endphp
                                    @if(isset($bals[60]))
                                        @if(count($bals[60])>0)
                                            <tr>
                                                <td colspan="{{count($branches)+1}}" style="padding-left: 30px"><b>Expense</b></td>
                                            </tr>
                                            @forelse($bals[60] as $acc_id => $bal)

                                                <tr>
                                                <td style="padding-left: 60px;">
                                                {{-- <a class="general-leg60" id="{{ $acc_id }}" data-toggle="modal" data-target="#show-detail-modal" style="text-decoration:underline;"> --}}
                                                {{ optional(\App\Models\AccountChart::find($acc_id))->code."-".optional(\App\Models\AccountChart::find($acc_id))->name }}
                                                    {{-- </a> --}}
                                                </td>
                                                    @foreach($branches as $b_id)
                                                        <?php
                                                        if(!isset($total_expense[$b_id])) $total_expense[$b_id] = 0;
                                                        $b = isset($bal[$b_id])?($bal[$b_id]??0):0;
                                                        $total_expense[$b_id] += $b;
                                                        ?>
                                                        <td style="text-align: right">
                                                            {{-- <a  class="general-leg60" id="{{ $acc_id }}" data-toggle="modal" data-target="#show-detail-modal"> --}}
                                                                {{ number_format($b??0,2) }}
                                                            {{-- </a> --}}
                                                        </td>
                                                    @endforeach
                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <td style="padding-left: 30px"><b>Total Expense</b></td>
                                                    @foreach($branches as $b_id)
                                                        <td style="text-align: right;padding-right: 10px"><u>{{ number_format($total_expense[$b_id],2) }}</u></td>
                                                    @endforeach
                                                </tr>
                                                @endif
                                                @endif


                                                <?php
                                                $n=0;
                                                ?>


                                                <tr>
                                                    @foreach($branches as $b_id)

                                                        @php
                                                            if(!isset($net_ordinary[$b_id])) $net_ordinary[$b_id] = 0;
                                                            $net_ordinary[$b_id] = ($gross_profit[$b_id]??0) + ($total_expense[$b_id]??0);
                                                            $n++;
                                                        @endphp
                                                        @if ($n > 1)
                                                            <td style="color: blue;"><b>{{ $gross_profit[$b_id] < 0?'Net Ordinary':'Net Loss Ordinary' }}</b> <u>{{ $net_ordinary[$b_id] < 0?number_format(-$net_ordinary[$b_id],2):number_format($net_ordinary[$b_id],2) }}</u></td>
                                                        @else
                                                            <td style="color: blue;"><b>{{ $gross_profit[$b_id] < 0?'Net Ordinary':'Net Loss Ordinary' }}</b></td>
                                                            <td style="color: blue;text-align: right;padding-right: 30px"><u>{{ $net_ordinary[$b_id] < 0?number_format(-$net_ordinary[$b_id],2):number_format($net_ordinary[$b_id],2) }}</u></td>
                                                        @endif
                                                    @endforeach

                                                </tr>

                                                {{--Close Expense--}}



                                                {{--Total Other Income--}}


                                                @php
                                                    $total_other_income = [];
                                                @endphp


                                                @if(isset($bals[70]))
                                                    @if(count($bals[70])>0)
                                                        <tr>
                                                            <td colspan="{{count($branches)+1}}" style="color: #006f6d;"><b>Other Income</b></td>
                                                        </tr>
                                                        @forelse($bals[70] as $acc_id => $bal)
                                                            @php
                                                                //$total_other_income += ($bal??0);
                                                            @endphp
                                                            <tr>
                                                            <td style="padding-left: 60px;">
                                                            {{-- <a class="general-leg70" id="{{ $acc_id }}" data-toggle="modal" data-target="#show-detail-modal" style="text-decoration:underline;"> --}}
                                                            {{ optional(\App\Models\AccountChart::find($acc_id))->code."-".optional(\App\Models\AccountChart::find($acc_id))->name }}
                                                        {{-- </a> --}}
                                                            </td>
                                                                @foreach($branches as $b_id)
                                                                    <?php
                                                                    if(!isset($total_other_income[$b_id])) $total_other_income[$b_id] = 0;
                                                                    $b = isset($bal[$b_id])?($bal[$b_id]??0):0;
                                                                    $total_other_income[$b_id] += $b;
                                                                    ?>
                                                                    <td style="text-align: right">{{ number_format($b??0,2) }}</td>
                                                                @endforeach

                                                            </tr>
                                                            @endforeach
                                                            <tr>
                                                                <td colspan="{{count($branches)+1}}" style="color: #006f6d;padding-right: 10px"><u>Total Other Income</u></td>
                                                            </tr>
                                                            @endif
                                                            @endif


                                        {{--Close Total Other Income--}}



                                        {{--Total Other Expense--}}
                                        @php
                                            $total_other_expense = [];
                                            $net_income = [];
                                        @endphp
                                        @if(isset($bals[80]))
                                            @if(count($bals[80])>0)
                                                <tr>
                                                    <td colspan="{{count($branches)+1}}" style="color: red;"><b>Other Expense</b></td>
                                                </tr>
                                                @forelse($bals[80] as $acc_id => $bal)

                                                    <tr>
                                                        <td style="padding-left: 60px;">
                                                        {{-- <a class="general-leg80" id="{{ $acc_id }}" data-toggle="modal" data-target="#show-detail-modal" style="text-decoration:underline;"> --}}
                                                        {{ optional(\App\Models\AccountChart::find($acc_id))->code."-".optional(\App\Models\AccountChart::find($acc_id))->name }}
                                                    {{-- </a> --}}
                                                        </td>
                                                        @foreach($branches as $b_id)
                                                            <?php
                                                            if(!isset($total_other_expense[$b_id])) $total_other_expense[$b_id] = 0;
                                                            $b = isset($bal[$b_id])?($bal[$b_id]??0):0;
                                                            $total_other_expense[$b_id] += $b;
                                                            ?>
                                                            <td style="text-align: right">
                                                                {{-- <a  class="general-leg80" id="{{ $acc_id }}" data-toggle="modal" data-target="#show-detail-modal"> --}}
                                                                    {{ number_format($b??0,2) }}
                                                                {{-- </a> --}}
                                                            </td>
                                                        @endforeach

                                                    </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="{{count($branches)+1}}" style="color: red;padding-right: 10px"><u>Total Other Expense</u></td>
                                                    </tr>
                                                    @endif
                                                    @endif



                                                    <tr>
                                                        <?php
                                                        $n=0;
                                                        ?>
                                                        @foreach($branches as $b_id)

                                                            @php
                                                                if(!isset($net_income[$b_id])) $net_income[$b_id] = 0;
                                                                $net_income[$b_id] = ( $net_ordinary[$b_id]??0) + ($total_other_income[$b_id]??0) + ($total_other_expense[$b_id]??0);
                                                                $n++;
                                                            @endphp
                                                            @if($net_income[$b_id] != $net_ordinary[$b_id])

                                                                @if ($n > 1)
                                                                    <td style="color: blue;"><b>{{ $net_income[$b_id] < 0?'Net Income':'Net Loss' }}</b> <u>{{ $net_income[$b_id] < 0?number_format(-$net_income[$b_id],2):number_format($net_income[$b_id],2) }}</u></td>
                                                                @else
                                                                    <td style="color: blue;"><b>{{ $net_income[$b_id] < 0?'Net Income':'Net Loss' }}</b></td>
                                                                    <td style="color: blue;text-align: right;padding-right: 50px"><u>{{ $net_income[$b_id] < 0?number_format(-$net_income[$b_id],2):number_format($net_income[$b_id],2) }}</u></td>
                                                                @endif

                                                            @endif
                                                        @endforeach


                                                    </tr>
        </tbody>
    </table>
<script>
    $(".general-leg40, .general-leg50, .general-leg60, .general-leg70, .general-leg80").click(function(){
        getReport($(this).attr("id"));
    });

    function getReport(id){
        var acc_chart_id= [id];
        var start_date = $('[name="start_date"]').val();
        var end_date = $('[name="end_date"]').val();
        var month = $('[name="month"]').val();
        var show_zero = $('[name="show_zero"]').val();
        var branch_id = $('[name="branch_id[]"]').val();
        var branch_id = 1;
        $.ajax({
            url: "{{ url('report/general-leger')}}",
            type: 'GET',
            async: false,
            dataType: 'html',
            data: {
                start_date:start_date,
                end_date:end_date,
                month:month,
                acc_chart_id:acc_chart_id,
                show_zero:show_zero,
                branch_id: branch_id
            },
            success: function (d) {
                $('.modal-body').html(d);
            },
            error: function (d) {
                alert('error');
                $('.modal-body').hide();
            }
        });
    }

    function printDiv() {

        var DivIdToPrintPop = document.getElementById('DivIdToPrintPop');//DivIdToPrintPop
        if(DivIdToPrintPop != null) {
            var newWin = window.open('', 'Print-Window');

            newWin.document.open();

            newWin.document.write('<html><body onload="window.print()">' + DivIdToPrintPop.innerHTML + '</body></html>');

            newWin.document.close();

            setTimeout(function () {
                newWin.close();
            }, 10);
        }
    }
</script>
@else
<h1>No data</h1>
@endif
</div>
