<div id="DivIdToPrint">
@if($bals != null)
    @php
        $today = date('Y-m-d');
    @endphp
    @include('partials.reports.header',
    ['report_name'=>'Balance Sheet','from_date'=>$start_date,'to_date'=>$end_date,'use_date'=>1])
    <div>{{$today}}</div>
    @foreach($branchs as $branch)
        <div><span class="pull-right" style="margin-bottom:4px;">{{\App\Models\Branch::find($branch)->title}},</span></div>
    @endforeach
    <table border="1" class="table-data" id="table-data">
        <tbody>
            <tr>
                <td style="color: blue;"><b>Assets</b></td>
                @foreach($branchs as $b)

                    <td colspan="0" style="color: blue;"><b>{{optional(\App\Models\BranchU::find($b))->title}}</b></td>
                @endforeach
            </tr>

            @php
            $total_current_asset = [];
            @endphp
            @if(isset($bals[10]) || isset($bals[12]) || isset($bals[14]))
                <tr>
                    <td colspan="{{count($branchs)+1}}" style="padding-left: 30px"><b>Current Assets</b></td>
                </tr>
                @foreach([10,12,14] as $sec)
                    @if(isset($bals[$sec]))
                        @if(count($bals[$sec])>0)
                            @forelse($bals[$sec] as $acc_id => $bal)
                                @php
                                    $chat_account = \App\Models\AccountChart::find($acc_id);
                                @endphp
                                <tr>
                                    <td style="padding-left: 60px;">{{ optional($chat_account)->code."-".optional($chat_account)->name}}</td>

                                    @foreach($branchs as $br_id)
                                        <?php
                                        if(!isset($total_current_asset[$br_id])) $total_current_asset[$br_id] = 0;
                                        $b = isset($bal[$br_id])?($bal[$br_id]??0):0;

                                        $total_current_asset[$br_id] += ($b??0);
                                        ?>
                                    <td style="text-align: right">{{ number_format($b??0,2) }}</td>

                                    @endforeach
                                </tr>
                            @endforeach
                        @endif
                    @endif
                @endforeach
                <tr>
                    <td style="padding-left: 30px"><b>Total Current Assets</b></td>
                    <?php $l_current_asset = 0; ?>
                    @foreach($branchs as $br_id)
                        <?php
                        //$l_current_asset += $total_current_asset[$br_id];

                        ?>
                        <td style="text-align: right;padding-right: 10px"><u>{{ number_format($total_current_asset[$br_id],2) }}</u></td>
                    @endforeach
                    {{--<td style="text-align: right;padding-right: 10px"><u>{{ number_format($l_current_asset,2) }}</u></td>--}}
                </tr>
            @endif

            @php
                $total_fixed_asset = [];

            @endphp
            @if(isset($bals[16]))

                <tr>
                    <td colspan="{{count($branchs)+1}}" style="padding-left: 30px"><b>Fixed Assets</b></td>
                </tr>
                @foreach([16] as $sec)

                    @if(isset($bals[$sec]))
                        @if(count($bals[$sec])>0)
                            @forelse($bals[$sec] as $acc_id => $bal)
                                @php

                                    $chat_account = \App\Models\AccountChart::find($acc_id);

                                @endphp
                                <tr>
                                    <td style="padding-left: 60px;">{{ optional($chat_account)->code."-".optional($chat_account)->name}}</td>
                                    @foreach($branchs as $br_id)
                                        <?php
                                        if(!isset($total_fixed_asset[$br_id])) $total_fixed_asset[$br_id] = 0;
                                        $b = isset($bal[$br_id])?($bal[$br_id]??0):0;
                                        $total_fixed_asset[$br_id] += ($b??0);
                                        ?>
                                        <td style="text-align: right">{{ number_format($b??0,2) }}</td>

                                    @endforeach
                                </tr>
                            @endforeach
                        @endif
                    @endif
                @endforeach
                <tr>

                    <td style="padding-left: 30px"><b>Total Fixed Assets</b></td>
                    @foreach($branchs as $br_id)

                    <td style="text-align: right;padding-right: 10px"><u>{{ number_format($total_fixed_asset[$br_id],2) }}</u></td>
                    @endforeach
                </tr>
            @endif


            @php
                $total_other_asset = [];

            @endphp
            @if(isset($bals[18]))
                <tr>
                    <td colspan="{{count($branchs)+1}}" style="padding-left: 30px"><b>Other Assets</b></td>
                </tr>
                @foreach([18] as $sec)
                    @if(isset($bals[$sec]))
                        @if(count($bals[$sec])>0)
                            @forelse($bals[$sec] as $acc_id => $bal)
                                @php

                                    $chat_account = \App\Models\AccountChart::find($acc_id);

                                @endphp
                                <tr>
                                    <td style="padding-left: 60px;">{{ optional($chat_account)->code."-".optional($chat_account)->name}}</td>
                                    {{--<td style="text-align: right">{{ number_format($bal??0,2) }}</td>--}}
                                    @foreach($branchs as $br_id)
                                        <?php
                                        if(!isset($total_other_asset[$br_id])) $total_other_asset[$br_id] = 0;
                                        $b = isset($bal[$br_id])?($bal[$br_id]??0):0;
                                        $total_other_asset[$br_id] += ($b??0);
                                        ?>
                                        <td style="text-align: right">{{ number_format($b??0,2) }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        @endif
                    @endif
                @endforeach
                <tr>
                    <?php
                        $l_other_asset =0;
                    ?>
                    <td style="padding-left: 30px"><b>Total Other Assets</b></td>
                    @foreach($branchs as $br_id)
                     <?php
                            //$l_other_asset += $total_other_asset[$br_id];
                     ?>
                    <td style="text-align: right;padding-right: 10px"><u>{{ number_format( $total_other_asset[$br_id],2) }}</u></td>
                    @endforeach
                </tr>
            @endif


            @php
                $total_all_assets = [];
                foreach($branchs as $br_id){
                    $total_all_assets[$br_id] =(isset( $total_current_asset[$br_id] )? $total_current_asset[$br_id] :0)+(isset($total_fixed_asset[$br_id])?$total_fixed_asset[$br_id]:0)
                    + (isset($total_other_asset[$br_id])?$total_other_asset[$br_id]:0);
                }

            @endphp
            <tr>
                <td style="color: blue;"><b>Total Assets</b></td>
                @foreach($branchs as $br_id)
                <td style="color: blue;padding-right: 20px;text-align: right;"><u>{{ numb_format($total_all_assets[$br_id],2) }}</u></td>
                @endforeach
            </tr>


           {{--////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////--}}
           <tr>
                <td colspan="{{count($branchs)+1}}" style="color: green;"><b>Liabilities & Owners Equity</b></td>
            </tr>
           <tr>
                <td colspan="{{count($branchs)+1}}" style="color: blue;padding-left: 10px"><b>Liabilities</b></td>
            </tr>
            @php
            $total_current_liabilities = [];
            @endphp
            @if(isset($bals[20]) || isset($bals[22]) || isset($bals[24]))
                <tr>
                    <td colspan="{{count($branchs)+1}}" style="padding-left: 30px"><b>Current Liabilities</b></td>
                </tr>
                @foreach([20,22,24] as $sec)
                    @if(isset($bals[$sec]))
                        @if(count($bals[$sec])>0)
                            @forelse($bals[$sec] as $acc_id => $bal)
                                @php

                                    $chat_account = \App\Models\AccountChart::find($acc_id);

                                @endphp
                                <tr>
                                    <td style="padding-left: 60px;">{{ optional($chat_account)->code."-".optional($chat_account)->name}}</td>

                                    @foreach($branchs as $br_id)
                                        <?php
                                        if(!isset($total_current_liabilities[$br_id])) $total_current_liabilities[$br_id] = 0;
                                        $b = isset($bal[$br_id])?($bal[$br_id]??0):0;
                                        $total_current_liabilities[$br_id] += ($b??0);
                                        ?>
                                        <td style="text-align: right">{{ number_format(-$b??0,2) }}</td>

                                    @endforeach
                                </tr>
                            @endforeach
                        @endif
                    @endif
                @endforeach
                <tr>
                    <?php
                        $l_current_liabilities = 0
                    ?>
                    <td style="padding-left: 30px"><b>Total Current Liabilities</b></td>
                    @foreach($branchs as $br_id)
                    <td style="text-align: right;padding-right: 10px"><u>{{ number_format(-$total_current_liabilities[$br_id],2) }}</u></td>
                    @endforeach
                </tr>
            @endif


            @php
                $total_long_term_liabilities = [];

            @endphp
            @if(isset($bals[26]))
                <tr>
                    <td colspan="{{count($branchs)+1}}" style="padding-left: 30px"><b>Long Term Liabilities</b></td>
                </tr>
                @foreach([26] as $sec)
                    @if(isset($bals[$sec]))
                        @if(count($bals[$sec])>0)
                            @forelse($bals[$sec] as $acc_id => $bal)
                                @php

                                     $chat_account = \App\Models\AccountChart::find($acc_id);
                                @endphp
                                <tr>
                                    <td style="padding-left: 60px;">{{ optional($chat_account)->code."-".optional($chat_account)->name}}</td>
                                    {{--<td style="text-align: right">{{ number_format(-$bal??0,2) }}</td>--}}
                                    @foreach($branchs as $br_id)
                                        <?php
                                        if(!isset($total_long_term_liabilities[$br_id])) $total_long_term_liabilities[$br_id] = 0;
                                        $b = isset($bal[$br_id])?($bal[$br_id]??0):0;
                                        $total_long_term_liabilities[$br_id] += ($b??0);
                                        ?>
                                        <td style="text-align: right">{{ number_format(-$b??0,2) }}</td>

                                    @endforeach
                                </tr>
                            @endforeach
                        @endif
                    @endif
                @endforeach

                <tr>
                    <td style="padding-left: 30px"><b>Total Long Term Liabilities</b></td>
                    @foreach($branchs as $br_id)
                    <td style="text-align: right;padding-right: 10px"><u>{{ number_format(-$total_long_term_liabilities[$br_id],2) }}</u></td>
                    @endforeach
                </tr>
            @endif


            @php
                $total_all_liabilities = [];
                foreach($branchs as $br_id){
                $total_all_liabilities[$br_id] = (isset($total_current_liabilities[$br_id])?$total_current_liabilities[$br_id]:0) + (isset($total_long_term_liabilities[$br_id])?$total_long_term_liabilities[$br_id]:0);
                }

            @endphp
            <tr>
                <td style="color: blue;padding-left: 10px"><b>Total Liabilities</b></td>
                @foreach($branchs as $br_id)
                <td style="color: blue;padding-right: 20px;text-align: right;"><u>{{ number_format(isset($total_all_liabilities[$br_id])?-$total_all_liabilities[$br_id]:0,2) }}</u></td>
                @endforeach
            </tr>


           {{--////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////--}}
           <tr>
                <td colspan="{{count($branchs)+1}}" style="color: blue;padding-left: 10px"><b>Owners Equity</b></td>
            </tr>

            @php
                $total_owners_equity = [];

            @endphp
            @if(isset($bals[30]))
                @foreach([30] as $sec)
                    @if(isset($bals[$sec]))
                        @if(count($bals[$sec])>0)
                            @forelse($bals[$sec] as $acc_id => $bal)
                                @php

                                     $chat_account = \App\Models\AccountChart::find($acc_id);

                                @endphp
                                <tr>
                                    <td style="padding-left: 30px;">{{optional($chat_account)->code."-".optional($chat_account)->name}}</td>
                                    {{--<td style="text-align: right">{{ number_format(-$bal??0,2) }}</td>--}}

                                    @foreach($branchs as $br_id)
                                        <?php
                                        if(!isset($total_owners_equity[$br_id])) $total_owners_equity[$br_id] = 0;
                                        $b = isset($bal[$br_id])?($bal[$br_id]??0):0;

                                        $total_owners_equity[$br_id] += ($b??0);
                                        ?>
                                        <td style="text-align: right">{{ number_format($b??0,2) }}</td>

                                    @endforeach
                                </tr>
                            @endforeach
                        @endif
                    @endif
                @endforeach
            @endif
            @php
                $total_all_owners_equity = [];
                foreach($branchs as $br_id){
                $total_all_owners_equity[$br_id] = (isset($total_owners_equity[$br_id])?$total_owners_equity[$br_id]:0) +
                                           (isset($retainedEarningBegin[$br_id])?$retainedEarningBegin[$br_id]:0)  + (isset($profit[$br_id])?$profit[$br_id]:0);
                }

            @endphp
            <tr>
                <td style="padding-left: 30px;">Retained Earning</td>
                @foreach($branchs as $br_id)
                <td style="text-align: right">{{ number_format(-(isset($retainedEarningBegin[$br_id])?$retainedEarningBegin[$br_id]:0)??0,2) }}</td>
                @endforeach
            </tr>

            <tr>
                <td style="padding-left: 30px;">Net Profit/Net Loss</td>
                @foreach($branchs as $br_id)
                <td><span style="text-align: left">{{ (isset($profit[$br_id])?$profit[$br_id]:0)<0?'Net Profit':'Net Loss' }}</span><span style="float: right">{{number_format(-(isset($profit[$br_id])?$profit[$br_id]:0)??0,2) }}</span></td>
                @endforeach
            </tr>


            <tr>
                <td style="color: blue;padding-left: 10px"><b>Total Owners Equity</b></td>
                @foreach($branchs as $br_id)
                <td style="color: blue;padding-right: 20px;text-align: right;"><u>{{ number_format(-(isset($total_all_owners_equity[$br_id])?$total_all_owners_equity[$br_id]:0),2) }}</u></td>
                @endforeach
            </tr>
            <tr>
                <td style="color: green;"><b>Total Liabilities & Owners Equity</b></td>
                @foreach($branchs as $br_id)
                <td style="color: green;padding-right: 20px;text-align: right;"><u>{{ number_format(-((isset($total_all_owners_equity[$br_id])?$total_all_owners_equity[$br_id]:0)+(isset($total_all_liabilities[$br_id])?$total_all_liabilities[$br_id]:0)),2) }}</u></td>
                @endforeach
            </tr>
           {{--////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////--}}

        </tbody>
    </table>

@else
<h1>No data</h1>
@endif
</div>
