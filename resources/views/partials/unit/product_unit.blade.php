@php
$id = isset($id)?$id:0;
$_e = isset($entry)?$entry:null;
@endphp
<table class="table table-bordered">
    <thead>
    <tr>
        <th>{{_t('Name')}}</th>
        <th>{{_t('Quantity Unit')}}</th>
        <th></th>
    </tr>
    </thead>
    <tbody  class="gen">

        @if($_e != null)
           @include('partials.unit.gen_product_unit',['product_id'=>optional($_e)->id])
        @endif
    </tbody>
</table>


@push('crud_fields_scripts')

    <script>
        $(function () {

            $('.generate_unit').on('click',function (e) {
                e.preventDefault();
                genProductUnit({{$id}});

            });


            @if(isset($id))
                @if($id>0)
                    genProductUnit({{$id}});
                @endif
            @endif

        });


        function genProductUnit(id) {
            var unit = $('[name="unit_variant[]"]').val();

            // console.log(warehouse);
            // console.log(spec);

            $.ajax({
                type:'POST',
                url:'{{url('api/gen-unit-variant')}}',
                data:{
                    unit:unit,
                    id:id
                },
                success:function(res){
                    $('.gen').html(res);
                }

            });
        }
    </script>
@endpush
