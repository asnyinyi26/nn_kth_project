@php
$product_id = isset($product_id)?$product_id:null;
@endphp
@if(isset($units))

   {{-- @foreach($warehouses as $warehouse)--}}
        {{--<tr>
            <td colspan="5"><span style="color: red;">{{$warehouse->name}}</span></td>
        </tr>--}}
        @foreach($units as $unit)
            <?php
                $rand_id = rand(1,1000).time().rand(1,1000);


            $u_val = \App\Models\ProductUnitVariant::where('product_id',$product_id)
                ->where('unit_id',$unit->id)
                ->first();

            ?>
            <tr>
                <td style="padding-left: 20px;"><span style="color: blue;">{{$unit->title}}</span></td>
                <td>
                    <input type="number" value="{{optional($u_val)->qty}}"   class="form-control" name="unit_qty[{{ $unit->id }}]">
                    <input type="hidden" value="{{$unit->id}}"   class="form-control" name="unit_variant_id[{{ $unit->id }}]">
                </td>
                <td>
                    <i class="fa fa-times tip remove-product-line" title="Remove" style="cursor:pointer;"></i>
                </td>
            </tr>
        @endforeach
    {{--@endforeach--}}

@endif
