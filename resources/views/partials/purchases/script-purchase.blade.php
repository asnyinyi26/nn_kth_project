<?php
$_e = isset($entry)?$entry:null;
$purchase_details = optional($_e)->purchase_details;
$purchase_currency = optional($_e)->currencies;
$symbol = isset($purchase_currency->currency_symbol)?$purchase_currency->currency_symbol:'';

?>
<div class="div-del-detail"></div>
<table class="table table-bordered">
    <thead >
    <tr style="background-color:#3c8dbc;color:white;">
        <th>{{_t('Product')}} ({{_t('Code-Name')}})</th>
        <th  style="width: 100px;">{{_t('Unit Cost')}}</th>
        <th  style="width: 100px;">{{_t('Quantity')}}</th>
        <th style="width: 70px;"> {{_t('FOC')}}</th>
        <th style="width: 120px;"> {{_t('UOM')}}</th>
        {{--<th>{{_t('In Hand')}}</th>--}}
        <th>{{_t('Discount')}}</th>
       {{-- <th>{{_t('Tax')}}</th>--}}
        <th>{{_t('Amount')}}</th>
        <th><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
    </tr>
    </thead>
    <tbody id="product-list">
    @if($purchase_details != null)
        @if(count($purchase_details)>0)
            @foreach($purchase_details as $r)
                @include('partials.purchases.product-list',['r'=>$r])
            @endforeach
        @endif
    @endif
    </tbody>
    <tfoot>
    <tr>
        <th colspan="2" class="right">{{_t('Total')}}</th>
        <th  class="right"><span class="f-qty">0</span></th>
        {{--<th></th>--}}
        <th class="right"><span class="f-discount"></span></th>
        <th class="right"><span class="f-foc"></span></th>
        <th class="right"><span class="f-uom"></span></th>
       {{-- <th class="right"><span class="f-tax"></span></th>--}}
        <th class="right"><span class="f-amount"></span></th>
        <th class="right"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
    </tr>
    </tfoot>
</table>

<div class="well well-sm bottom-total " style="margin-bottom: 0px; position: fixed; bottom: 0px; width: 90%; z-index: 1;">
    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;width: 80%">
        <tbody>
        <tr>
            <th>{{_t('Items')}} <span class="pull-right g_items">{{optional($_e)->total_qty}}</span></th>
            <th>{{_t('Total')}} <span class="pull-right"><span class="g_total_amt">{{optional($_e)->subtotal}}</span> <span class="currency">{{$symbol}}</span></span></th>
            <th>{{_t('Discount')}} <span class="pull-right"><span class="g_discount">{{optional($_e)->discount_amount}}</span> <span class="currency">{{$symbol}}</span></span></th>
            <th>{{_t('FOC')}} <span class="pull-right"><span class="g_foc"></span></span></th>
            <th>{{_t('UOM')}} <span class="pull-right"><span class="g_foc"></span></span></th>
            {{--<th>{{_t('Tax')}} <span class="pull-right"><span class="g_tax">{{optional($_e)->tax_amount}}</span> <span class="currency">{{$symbol}}</span></span></th>--}}
            <th>{{_t('Shipping')}} <span class="pull-right"><span class="g_shipping">{{optional($_e)->shipping}}</span> <span class="currency">{{$symbol}}</span></span></th>
            <th>{{_t('Grand Total')}} <span class="pull-right"><span class="g_total">{{optional($_e)->grand_total}}</span> <span class="currency">{{$symbol}}</span></span></th>
            <th style="width: 50px;"><button type="button" class="btn btn-info add_payment"  data-toggle="modal" data-target="#payModal">Add Payment</button></th>
        </tr>
        </tbody>
    </table>
</div>

@include('partials.payment_template',['_e' => $_e])

@push('crud_fields_styles')
    <style>
        .box{
            padding-bottom:70px;
        }
        .table-bottom{
            margin-bottom: 100px !important;
        }
    </style>
@endpush

@push('crud_fields_scripts')

    <script>
        var is_return = $('[name="is_return"]').val();

        is_return = is_return>0?is_return:0;

        $(function () {



            $(window).bind('scroll', function() {
                if($(window).scrollTop() >= $('.box').offset().top + $('.box').outerHeight() - window.innerHeight) {
                    $(".bottom-total").addClass('table-bottom');
                }else{
                    $(".bottom-total").removeClass('table-bottom')
                }
            });

            $('body').on('click','.remove-product-line',function () {
                var r_id = $(this).data('id');

                var del_detail_id = $(this).data('del_detail_id');
                var tr = $('#p-'+r_id);
                tr.remove();

                calculate_grand_total();

                if(del_detail_id > 0) {
                    $('.div-del-detail').append('<input type="hidden"  value="'+del_detail_id+'"  name="del_detail_id[]">');
                }
            });

            $('body').on('keyup','.line_qty',function () {
                var qty = $(this).val();
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                tr.find('.p-line_qty').val(qty);
                calculate_line_total(tr);
            });
            $('body').on('keyup','.foc',function () {
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                var foc = tr.find('.foc').val();
                //tr.find('.span-line_foc').text(foc);
                tr.find('.line_foc').val(foc);
            });
            $('body').on('keyup','.line_foc',function () {
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                var foc = $(this).val();
                //tr.find('.span-line_foc').text(foc);
                tr.find('.foc').val(foc);
            });

            $('body').on('keyup','.p-line_qty',function () {
                var qty = $(this).val();
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                tr.find('.line_qty').val(qty);
                calculate_line_total(tr);
            });

            $('body').on('change','.line_tax_id',function () {
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                calculate_line_total(tr);
            });

            $('body').on('change','.line_spec_id',function () {
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                var spec = tr.find('.line_spec_id option:selected').text();
                tr.find('.product_spec_name').html('('+spec+')');
                calculate_line_total(tr);
            });

            $('body').on('keyup','.unit_discount',function () {

                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                calculate_line_total(tr);
            });

            $('body').on('keyup','.unit_cost',function () {
                //var net_cost = $(this).val();
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                calculate_line_total(tr);
            });

            $('body').on('keyup','.p-cal-line_subtotal',function () {
                var subtotal = $(this).val();
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                var qty = tr.find('.line_qty').val();
                var cost = 0;

                if(qty != 0){
                    cost = round( subtotal/qty,6);
                }

                tr.find('.unit_cost').val(cost);
                calculate_line_total(tr);
            });

            $('body').on('click','.calculate_line_total',function () {
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);
                var qty = tr.find('.line_qty').val();
                var subtotal = tr.find('.p-cal-line_subtotal').val();
                var cost = 0;
                if(qty != 0){
                    cost = round(subtotal/qty,6);
                }
                tr.find('.unit_cost').val(cost);
                calculate_line_total(tr);
            });

            //========== cal G-Total ==================
            //=========================================
            $('[name="discount"]').on('keyup',function () {
                calculate_grand_total();
            });

            $('[name="shipping"]').on('keyup',function () {
                calculate_grand_total();

            });

            $('[name="tax_id"]').on('change',function () {
                calculate_grand_total();
            });

            $('[name="currency_id"]').on('change',function () {
                var symbol = $('[name="currency_id"] option:selected').data('symbol');
                var exchange = $('[name="currency_id"] option:selected').data('exchange');
                $('.currency').text(symbol);
                $('.h_exchange').val(exchange);
                //calculate_grand_total();
            });

            $('.bill-order').hide();
            $('.bill-received').hide();
            $('.bill-received-order').hide();
            $('#purchase_status').hide();

            $('[name = "purchase_type"]').on('change',function () {
                var purchase_type = $(this).val();
                if(purchase_type == "bill-only-from-order"){
                    $('.bill-order').show();
                    $('.bill-received').hide();
                    $('.bill-received-order').hide();
                    $('#purchase_status').show();
                }else if(purchase_type == "bill-only-from-received"){
                    $('.bill-order').hide();
                    $('.bill-received').show();
                    $('.bill-received-order').hide();
                    $('#purchase_status').hide();
                }else if(purchase_type == "bill-and-received-from-order"){
                    $('.bill-received').hide();
                    $('.bill-order').hide();
                    $('.bill-received-order').show();
                    $('#purchase_status').show();
                }else {
                    $('.bill-order').hide();
                    $('.bill-received').hide();
                    $('.bill-received-order').hide();
                    $('#purchase_status').hide();
                }
            });

            if('{{ optional($_e)->purchase_type}}' == "bill-only-from-order"){
                $('.bill-order').show();
                $('.bill-received').hide();
                $('.bill-received-order').hide();
            }else if('{{ optional($_e)->purchase_type}}' == "bill-only-from-received"){
                $('.bill-order').hide();
                $('.bill-received').show();
                $('.bill-received-order').hide();
            }else if('{{ optional($_e)->purchase_type}}' == "bill-and-received-from-order"){
                $('.bill-received').hide();
                $('.bill-order').hide();
                $('.bill-received-order').show();
            }else {
                $('.bill-order').hide();
                $('.bill-received').hide();
                $('.bill-received-order').hide();
            }
            $('[name="bill_order_id"]').on('change',function () {
               var id =  $(this).val();
                window.location.href = '{{url("/api/bill-from-order")}}'+'/'+id;
            });
            $('[name="bill_received_order_id"]').on('change',function () {
                var id =  $(this).val();
                window.location.href = '{{url("/api/bill-received-from-order")}}'+'/'+id;
            });

            $('[name="return_bill_received_id"]').on('change',function () {
                var id =  $(this).val();

                window.location.href = '{{url("/api/return-bill-received-id")}}'+'/'+id;
            });
            $('[name="currency_id"]').parent().hide();


            $('[name="supplier_id"]').on('change',function () {

                var supplier_id = $(this).val();

                $.ajax({
                    type: 'GET',
                    url: '{{ url('api/get-supplier-currency_id') }}',
                    data: {
                        supplier_id: supplier_id,
                    },
                    success: function (res) {
                        //console.log(res);
                        var currency_id = res.currency_id;
                        $('[name="currency_id"]').val(currency_id);
                        $('[name="tax_id"]').val(res.tax_id);
                        $('[name="payment_term_id"]').val(res.payment_term_id);
                        $('[name="currency_id"]').trigger('change');

                    }

                });



                //
                // var symbol = $('[name="currency_id"] option:selected').data('symbol');
                // var exchange = $('[name="currency_id"] option:selected').data('exchange');
                // $('.currency').text(symbol);
                // $('.h_exchange').val(exchange);
                //calculate_grand_total();

            });

            $('body').on('click','.add-warehouse-detail',function () {
                var r_id = $(this).data('id');
                var tr =  $('#p-'+r_id);
                var warehouse_id = $(this).data('warehouse_detail');
                var main_w_id = $('[name="warehouse_id"]').val();
                alert(main_w_id);
                $.ajax({
                    type: 'GET',
                    url: '{{url('api/get-warehouse-detail')}}',
                    data: {
                        warehouse_id: warehouse_id,
                        line_main_id: r_id
                    },
                    success: function (res) {
                        tr.find('.warehouse-list').append(res);
                        $('.line_expire_date').datepicker({
                            format: 'yyyy-mm-dd'
                        });
                        tr.find('.line_location_id').select2({
                            theme: "bootstrap"
                        });
                    }

                });
            });


            $('body').on('keyup','.line_amount',function () {
                var r_id = $(this).data('id');
                var tr = $('#p-'+r_id);

                calculate_line_total2(tr);
            });


            $('body').on('change','.line_location_id',function () {
                var location_id = $(this).val();
                var tr = $(this).parent().parent();
                var tr_qty = $(this).parent().parent().parent().parent().parent();
                var p_id = tr_qty.find('.add-warehouse-detail').data('id');
                var _tr =  $('#p-'+p_id);

                $.ajax({
                    type: 'GET',
                    url: '{{url('api/get-location-from-purchase')}}',
                    dataType: 'JSON',
                    data: {
                        location_id: location_id
                    },
                    success: function (res) {
                        tr.find('.line_qty').val(res.qty);

                        tr.find('.line_qty').trigger('change');
                        tr.find('.line_lot').val(res.lot);

                        tr.find('.line_expire_date').val(res.factory_expire_date);
                        calculate_line_total(_tr);
                        calculate_line_qty(_tr);
                    }

                });
                calculate_line_total(_tr);
            });
            @if(isset($id))
            @if($id>0)
            calculate_grand_total();
            @endif
            @endif

            $('[name="good_received_id"]').on('change',function () {
                var good_received_id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '{{url('api/get-good-received')}}',
                    //dataType: 'JSON',
                    async: false,
                    data: {
                        good_received_id: good_received_id
                    },
                    success: function (res) {
                        $('#product-list').empty();
                        $('#product-list').append(res);
                    }

                });

                $.ajax({
                    type: 'GET',
                    url: '{{url('api/get-good-received-supplier')}}',
                    //dataType: 'JSON',
                    async: false,
                    data: {
                        good_received_id: good_received_id
                    },
                    success: function (res) {
                        console.log(res);
                        $('[name="supplier_id"]').html('<option value="'+res.id+'">'+res.name+'</option>');
                        $('[name="supplier_id"]').trigger('change');
                    }

                });
            });
        });

        function calculate_grand_total() {

            //== total line QTY ======
            var total_qty = 0;
            $('.line_qty').each(function () {
                var qty = $(this).val()-0;
                if (qty > 0) {
                    total_qty += qty;
                }
            });
            $('.f-qty').text(total_qty);
            $('.g_items').text(total_qty);
            $('.h_items').val(total_qty);

            //== total line discount ======
            var total_discount = 0;
            $('.line_discount_amount').each(function () {
                var discount = $(this).val()-0;
                if (discount > 0) {
                    total_discount += discount;
                }
            });
            $('.f-discount').text('('+total_discount+')');

            //== total line tax ======
            var total_tax = 0;
            $('.line_tax_amount').each(function () {
                var tax = $(this).val()-0;
                if (tax > 0) {
                    total_tax += tax;
                }
            });

            total_tax = round(total_tax,6);

            $('.f-tax').text(total_tax);

            //== total line amount ======
            var subtotal = 0;
            $('.line_amount').each(function () {
                var total = $(this).val()-0;
                if (total > 0) {
                    subtotal += round(total, 6);
                }
            });

            subtotal = round(subtotal);
            $('.f-amount').text(subtotal);
            $('.h_total').val(subtotal);
            $('.h_balance').val(subtotal);
            $('.g_total_amt').text(subtotal);

            //== cal G total ========
            var p_discount = $('[name="discount"]').val();
            var p_discount_p =0;
            var p_discount_a =0;

            if (p_discount.indexOf('%') != -1) {
                p_discount_p = p_discount.replace(/%/g, ' ');
            }
            else {
                p_discount_a = p_discount;
            }

            var g_discount = 0;
            if (p_discount_a > 0 && p_discount_p == 0) {
                g_discount =  round(p_discount_a, 6);
            } else if (p_discount_a == 0 && p_discount_p > 0) {
                g_discount = round(subtotal * p_discount_p / 100, 6);
            }

            var g_total_after_dis = subtotal - g_discount;
            var g_shipping = $('[name="shipping"]').val()-0;

            var g_tax = $('[name="tax_id"] option:selected').data('tax');

            var g_total_tax = 0;
            if(g_tax >0) {
                g_total_tax = round( g_total_after_dis * (g_tax / 100), 6);
            }

            var g_total = round( g_total_after_dis + g_total_tax + g_shipping,6);
            $('.g_discount').text(g_discount);
            $('.h_discount').val(g_discount);

            $('.g_shipping').text(g_shipping);
            $('.h_shipping').val(g_shipping);

            $('.g_tax').text(g_total_tax);
            $('.h_tax').val(g_total_tax);

            $('.g_total').text(g_total);
            $('.h_gtotal').val(g_total);

            $('.quick-amt').data('amt', g_total);
        }

        function calculate_line_total(tr){
            var tax = tr.find('.line_tax_id option:selected').data('tax');
            var qty = tr.find('.line_qty').val();
            var cost = tr.find('.unit_cost').val();
           /* var cost = 0;
             cost = tr.find('.unit_cost').val();*/
            //var amount = tr.find('.line_amount').val();
            var unit = tr.find('.line_unit_id');
            var spec = tr.find('.line_spec_id');
            var _discount = tr.find('.unit_discount').val();
            var discount_p = 0;
            var discount_a = 0;

            var net_cost = 0;
            var discount_amount = 0;

            if(qty != 0 || qty != '') {

                /*var line_unit_cost = 0;
                if(amount>0){
                    cost = amount/qty;
                }*/

                if (_discount.indexOf('%') != -1) {
                    discount_p = _discount.replace(/%/g, ' ');
                }else {
                    discount_a = _discount;
                }

                if (discount_a > 0 && discount_p == 0) {
                    net_cost = cost;
                    discount_amount = discount_a;//*qty;
                } else if (discount_a == 0 && discount_p > 0) {
                    net_cost = cost;// round( cost * (1 - discount_p / 100),4);
                    discount_amount = round( cost*qty*discount_p/100,4);
                } else {
                    net_cost = cost;
                }

               //alert(net_cost);
                var unit_tax  = round(net_cost * tax / 100,6);

                var line_total =0;
                if(tax > 0) {
                    line_total = round( (net_cost * qty  - discount_amount)* (1 + tax / 100),6);
                }else {
                    line_total = round(net_cost * qty - discount_amount,4);
                }


                //=== unit cost ======
                tr.find('.span-p-net_unit_cost').text(net_cost);
                tr.find('.net_unit_cost').val(net_cost);
                tr.find('.span-net_unit_cost').text(net_cost);

                //=== tax ============
                tr.find('.unit_tax').val(unit_tax);
                tr.find('.span-p-unit_tax').text(unit_tax);
                tr.find('.line_tax_amount').val(round(unit_tax*qty,6));
                tr.find('.span-line_tax_amount').text((round(unit_tax*qty,6)) + ' (' + tax + '%)');


                tr.find('.span-line_discount_amount').text('('+discount_amount+')');
                tr.find('.line_discount_amount').val(discount_amount);
                //tr.find('.unit_cost').val(round(net_cost,4));
                tr.find('.span-line_amount').text(round(line_total,6));
                tr.find('.line_amount').val(line_total);


            }

            calculate_grand_total();
        }


        function calculate_line_total2(tr){
            var tax = tr.find('.line_tax_id option:selected').data('tax');
            var qty = tr.find('.line_qty').val();
            //var cost = tr.find('.unit_cost').val();
           /* var cost = 0;
             cost = tr.find('.unit_cost').val();*/
            var amount = tr.find('.line_amount').val();
            var unit = tr.find('.line_unit_id');
            var spec = tr.find('.line_spec_id');
            var _discount = tr.find('.unit_discount').val();
            var discount_p = 0;
            var discount_a = 0;

            var net_cost = 0;
            var discount_amount = 0;

            if(qty != 0 || qty != '') {

                /*var line_unit_cost = 0;
                if(amount>0){
                    cost = amount/qty;     q*p *(1-d) = a
                }*/

                if (_discount.indexOf('%') != -1) {
                    discount_p = _discount.replace(/%/g, ' ');
                }else {
                    discount_a = _discount;
                }

                if (discount_a > 0 && discount_p == 0) {
                    net_cost = (amount + discount_a)/qty;
                    discount_amount = discount_a;//*qty;
                } else if (discount_a == 0 && discount_p > 0) {
                    net_cost = round( amount/(qty*(1-(discount_p/100))),4 );// round( cost * (1 - discount_p / 100),4);
                    discount_amount = round( net_cost*qty*discount_p/100,4);
                } else {
                    net_cost = amount/qty;
                }

               //alert(net_cost);
                var unit_tax  = round(net_cost * tax / 100,6);

                var line_total =0;
                if(tax > 0) {
                    line_total = round( (net_cost * qty  - discount_amount)* (1 + tax / 100),6);
                }else {
                    line_total = round(net_cost * qty - discount_amount,4);
                }


                //=== unit cost ======
                tr.find('.span-p-net_unit_cost').text(net_cost);
                tr.find('.net_unit_cost').val(net_cost);
                tr.find('.span-net_unit_cost').text(net_cost);

                //=== tax ============
                tr.find('.unit_tax').val(unit_tax);
                tr.find('.span-p-unit_tax').text(unit_tax);
                tr.find('.line_tax_amount').val(round(unit_tax*qty,6));
                tr.find('.span-line_tax_amount').text((round(unit_tax*qty,6)) + ' (' + tax + '%)');


                tr.find('.span-line_discount_amount').text('('+discount_amount+')');
                tr.find('.line_discount_amount').val(discount_amount);
                tr.find('.unit_cost').val(round(net_cost,4));
                tr.find('.span-line_amount').text(round(line_total,6));
                //tr.find('.line_amount').val(line_total);


            }

            calculate_grand_total();
        }


        function select_product(product_id) {
            $.ajax({
                type: 'GET',
                url: '{{url('api/purchase-select-product')}}',
                data: {
                    product_id: product_id,
                    is_return: is_return,
                },
                success: function (res) {
                    $('#product-list').append(res);
                }

            });
        }

        $('.add_payment').on('click', function () {
            var item = $('.h_items').val() - 0;
            if (item == 0 || item == null) {
                alert('no item selected');
                return false;
            }
        });


        $(function () {
            @if($_e != null)


                var h_purchase_type = $('.h_purchase_type').val();

                if( h_purchase_type == 'bill-only-from-order' || h_purchase_type == 'bill-and-received-from-order' || h_purchase_type == 'return-from-bill-received')
                {
                    $('[name="purchase_type"] option:selected').val(h_purchase_type);
                    $('[name="purchase_type"]').trigger('change');

                    if( h_purchase_type == 'bill-only-from-order'){
                        $('[name="bill_order_id"]').html('<option value="{{$id}}">{{ optional(\App\Models\Purchase::find($id))->reference_no  }}</option>');
                        $('form').prop('action','{{url('admin/bill-only')}}');

                    }

                    if( h_purchase_type == 'bill-and-received-from-order'){
                        $('[name="bill_received_order_id"]').html('<option value="{{$id}}">{{ optional(\App\Models\Purchase::find($id))->reference_no  }}</option>');
                        $('form').prop('action','{{url('admin/bill-received')}}');


                    }
                    if(h_purchase_type == 'return-from-bill-received'){
                        $('[name="return_bill_received_id"]').html('<option value="{{$id}}">{{ optional(\App\Models\Purchase::find($id))->reference_no  }}</option>');
                        $('form').prop('action','{{url('admin/return-bill-received')}}');

                    }

                    $('[name="reference_no"]').val('');
                    $('[name="id"]').val('0');
                    $('[name="_method"]').remove();
                }else {
                    $('[name="purchase_type"] option:not(:selected)').each(function () {
                        $(this).remove();
                    });
                }

                $('[name="purchase_type"]').parent().hide();
            @endif

        });
    </script>
@endpush
