<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Excel</title>
</head>
<body>
    <textarea style="width: 100%;" placeholder="Note" class="note" rows="2"></textarea>
    <table class="table" cellspacing="0" cellpadding="5px">
        <tr>
            <th>Payment Date</th>
            <th>Cash In</th>
            <th>Payment Number</th>
            <th>Principle</th>
            <th>Interest</th>
            <th>Service</th>
            <th>Penalty</th>
            <th>Saving</th>
            <th>Owed Balance</th>
            <th style="width: 80px">Action</th>
        </tr>
        @if($payments != null)
            <?php
                $total_principle = 0;
                $total_interest = 0;
                $total_charge = 0;
                $total_penalty = 0;
                $total_compulsory = 0;
            $last_schedule_id = 0;
            //dd($payments);
            ?>
        @foreach($payments as $report)
            <?php
                    if ($loop->first){
                    $last_schedule_id = \App\Models\LoanCalculate::where('disbursement_id',$report->loan_id)
                        ->where(function ($w){
                            $w->orWhere('principal_p','>',0)
                                ->orWhere('interest_p','>',0)
                                ->orWhere('penalty_p','>',0)
                                ->orWhere('service_charge_p','>',0)
                                ->orWhere('compulsory_p','>',0)
                            ;
                        })->max('id');
                        //dd($report);
                    }
                    $loan = \App\Models\Loan::find($report->loan_id);
                    //dd($report->payment_id);
                    $payment = \App\Models\LoanPayment::find($report->payment_id,['payment_number']);
                    $acc = \App\Models\GeneralJournal::where('reference_no',optional($payment)->payment_number)->get()->toArray();
                     $result = array();
                        foreach ($acc as $ac){
                            $result[] =$ac['id'];
                        } ;
                        //dd($result); 
                    $acc_detail = \App\Models\GeneralJournalDetail::whereIn('journal_id',$result)->where('description','Payment')->where('name',$loan->client_id)->first();
                    //dd($acc_detail);
                    $cash_acc = \App\Models\AccountChart::where('code',optional($acc_detail)->acc_chart_code)->first();
                    $schedule_backup = \App\Models\ScheduleBackup::where('loan_id',$report->loan_id)->first();
                    $total_principle += $report->principal_p;
                    $total_interest += $report->interest_p;
                    $total_charge += $report->service_charge_p;
                    $total_penalty += $report->penalty_p;
                    $total_compulsory += $report->compulsory_p;
            ?>
            <tr>
                <td>{{ \Carbon\Carbon::parse($report->payment_date)->format('Y-m-d') }}</td>
                <td>{{ optional($cash_acc)->name}}</td>
                <td>{{ optional($payment)->payment_number}}</td>
                <td>{{ $report->principal_p }}</td>
                <td>{{ $report->interest_p }}</td>
                <td>{{ $report->service_charge_p }}</td>
                <td>{{ $report->penalty_p }}</td>
                <td>{{ $report->compulsory_p }}</td>
                <td>{{ $report->owed_balance }}</td>
                @if($last_schedule_id == $report->schedule_id && $loop->last && $schedule_backup != null)
                    <td style="width: 80px"><a href="{{url("/api/delete-payment")}}" data-payment_id="{{$report->payment_id}}" class="btn btn-xs btn-danger roll-back">Roll Back</a></td>
                @else
                    <td></td>
                @endif
            </tr>
        @endforeach
            <tr>
                <td colspan="3" style="text-align: right;"><b>Total</b></td>
                <td>{{$total_principle}}</td>
                <td>{{$total_interest}}</td>
                <td>{{$total_charge}}</td>
                <td>{{$total_penalty}}</td>
                <td>{{$total_compulsory}}</td>
            </tr>
        @endif
    </table>
    <script>
        $('.roll-back').on('click',function (e) {
            e.preventDefault();
            var url = $(this).prop('href');
            var payment_id = $(this).data('payment_id');
            var note = $('.note').val();

            $.ajax({
                type: 'GET',
                url: url,
                data: {
                    payment_id: payment_id,
                    note: note,
                },
                success: function (res) {
                    window.location.reload();
                }

            });

        });
    </script>
</body>
</html>
