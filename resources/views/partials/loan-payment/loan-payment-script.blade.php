@push('crud_fields_styles')

@endpush

@push('crud_fields_scripts')

    <script>
        $(function () {
            $('[name="penalty_amount"]').on('keyup',function () {
                get_payment();
            });
            $('[name="other_payment"]').on('keyup',function () {
                get_payment();
            });
            $('[name="payment"]').on('keyup',function () {
                var payment = $(this).val()-0;
                var total_payment = $('[name="total_payment"]').val()-0;
                var owed_balance = total_payment - payment;
                if(owed_balance >=0) {
                    $('[name="owed_balance"]').val(round(owed_balance, 2));
                }else{
                    $(this).val(total_payment);
                    $('[name="owed_balance"]').val(0);
                }
            });
            get_payment();
        });
        function get_payment() {

            var penalty_amount = $('[name="penalty_amount"]').val()-0;
            var principle = $('[name="principle"]').val()-0;
            var interest = $('[name="interest"]').val()-0;
            var old_owed = 0;// $('[name="old_owed"]').val()-0;
            var other_payment = $('[name="other_payment"]').val()-0;
            var compulsory_saving = $('[name="compulsory_saving"]') ? $('[name="compulsory_saving"]').val()-0:0;
            compulsory_saving = compulsory_saving>0?compulsory_saving:0;

            var charge =0;
            $('.service-charge').each(function () {
                var v = $(this).val()-0;
                if(v>0){
                    charge += v;
                }
            });

            var payment = 0;
            payment = penalty_amount + principle + interest + old_owed + other_payment + compulsory_saving +charge;

            $('[name="payment"]').val(round(payment,2));
            $('[name="total_payment"]').val(round(payment,2));
            $('[type="submit"]').on('click',function () {
                $(this).hide();
            });
        }
    </script>

@endpush
