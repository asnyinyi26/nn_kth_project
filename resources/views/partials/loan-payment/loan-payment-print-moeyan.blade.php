<div class="print-payment">
    <div id="DivIdToPrintPop">
    <style type="text/css">
	hr.style_dot {
		border-top: 1px dotted #8c8b8b;
	}

	hr.style_out {
		border-top: 1px solid #8c8b8b;
	}

	hr.style1 {
		border-top: 2px solid #8c8b8b;
	}

	hr {
		margin-bottom: 1px;
		margin-top: 0px;
	}

	.payment_voucher {
		text-align: center;
		vertical-align: text-top;
		position: relative;
		top: -10px;
		font-style: italic;
	}

	fieldset {
		border-radius: 5px;
		height: auto;
		min-height: 110px;
	}

	fieldset_ {
		min-width: 0;
		padding: 0;
		margin: 0;
		border: 0
	}

	legend_ {
		display: block;
		width: 100%;
		padding: 0;
		font-size: 21px;
		line-height: inherit;
		color: #333;
		border: 0;
		border-bottom: 1px solid #e5e5e5
	}

	.payto_refernce table tr td {
		padding-left: 5px;
		font-size: 12px;
	}

	.signature {
		width: 300px;
		text-align: left;
		margin: 0 auto;
		font-size: 12px;
		margin-top: 15px;
	}

	.signature span {
		font-size: 8px;
	}

	div[size="A4"] {
		width: 21cm;
		height: 29.7cm;
		margin: 25px auto;
	}
	@media print {
		.modal-dialog {
			width: 100% !important;
		}

		.modal-content {
			border: none !important;
		}

	}

	#logo img {
		width: 150px;
	}

	.cash {
		float: left;
		margin-left: 5px;
	}

	.date {
		float: right;
		margin-right: 53px
	}

	table,
	th,
	td {
		border: 1px solid black;
		line-height: 30px;
	}

	.wrap2 {
		clear: both;
	}

	.left {
		float: left;
		width: 60%;
		padding-left: 15px;
	}

	.right {
		float: right;
		width: 60%;
		padding-left: 15px;
	}
</style>
<?php
    $m = getSetting();
    $logo = getSettingKey('logo',$m);
    $company = getSettingKey('company-name',$m);
?>
@if($row != null)
    <div id="logo" style="float:left;">
        <img src="{{ asset($logo) }}" width="130"/>
    </div>


    <div style="padding-top:10px;">
        <p class="cash" style="float: left;">CASH RECEIPT <br>
        <p class="date" style="float: left;">DATE: {{ optional($row)->payment_date != null ? date('d-m-Y', strtotime(optional($row)->payment_date)):''}}<br>
        {{$row->payment_number}}</p>
    </div>

    <table class="table table-bordered schedule" style="border: 1px solid black;margin-top:5px;font-size:11px;width:90%;margin-left:2px">
        <tbody>
        <tr style="background-color:#009900;color:black;">
            <td style="text-align: center;">Client Name</td>
            <td>{{optional(optional($row)->client_name)->name}}</td>
        </tr>

        <tr>
            <td style="text-align: center;">NRC No</td>
            <td>{{optional(optional($row)->client_name)->client_number}}</td>
        </tr>

        <tr>
            <td style="text-align: center;">Penalty Fees</td>
            <td>{{number_format($row->penalty_amount),2}}</td> 
        </tr>

        <tr>
            <td style="text-align: center;">Total Coll;Amount</td>
            <td>{{number_format($row->payment),2}}</td>
        </tr>

        </tbody>
    </table>
	<?php $total_word = App\Helpers\S::convert_number_to_words($row->payment);?>
    <div style="font-size:11px; text-transform: uppercase">AMOUNT IN WORDS : {{$total_word}}</div>

    <div class="col-sm-12">
        <table style="border: 0;">
            <tr>
                <td style="padding:10px;border: 0; font-size: 11px;text-align: right;"><b>ငွေပေးသူ:</b> </td>
                <td style="border: 0;font-size: 11px;">....................................</td>
            </tr>
            <tr>
                <td style="padding:10px;border: 0; font-size: 11px;text-align: right;">ဖုန်း:</td>
                <td style="border: 0;font-size: 11px;">....................................</td>
            </tr>
            <tr>
                <td style="padding:10px;border: 0;font-size: 11px;text-align: right;">ငွေလက်ခံသူ</td>
                <td style="border: 0;font-size: 11px;">....................................</td>
            </tr>
            <tr>
                <td style="padding:10px;border: 0;font-size: 11px;text-align: right;">Cashier:</td>
                <td style="border: 0;font-size: 11px;">....................................</td>
            </tr>
        </table>
    </div>
</div>
    <div class="action" style="float: right;">
        <button type="button" onclick="printDiv() " style="cursor: pointer; background: #0b58a2;padding: 10px 20px; color: #fff; margin-bottom: 10px;">PRINT</button>
    </div>
@endif
</div>
<script>
   // if (typeof printDiv !== 'undefined') {

        function printDiv() {

            var DivIdToPrintPop = document.getElementById('DivIdToPrintPop');//DivIdToPrintPop

            if(DivIdToPrintPop != null) {
                var newWin = window.open('', 'Print-Window');

                newWin.document.open();

                newWin.document.write('<html><body onload="window.print()">' + DivIdToPrintPop.innerHTML + '</body></html>');

                newWin.document.close();

                setTimeout(function () {
                    newWin.close();
                }, 10);
            }
        }

   // }
</script>
