<?php
    $disburse_detail = \App\Models\LoanCalculate::where('disbursement_id',$row->id)->orderBy('id')->get();
    $c = $disburse_detail != null?count($disburse_detail):0;
?>
<div id="clear-{{ $row->id }}">
<table border="1" class="" style="border-collapse: collapse;min-width: 1000px;">
    <thead>
        <tr class="bg-success" style="height: 25px;padding: 5px">
            <th style="padding: 5px;"></th>
            <th style="padding: 5px;">No</th>
            <th style="padding: 5px;">Date</th>
            <th style="padding: 5px;">Principal</th>
            <th style="padding: 5px;">Interest</th>
            <th style="padding: 5px;">Payment</th>
            <th style="padding: 5px;">Balance</th>
            <th style="padding: 5px;"></th>
        </tr>
    </thead>
    @if($disburse_detail != null)
        @foreach($disburse_detail as $r)
            <tr  style="height: 25px;">
                <?php
                    $total_p = $r->principal_p+$r->interest_p+$r->compulsory_p+$r->service_charge_p;
                ?>
                <td style="padding: 5px;">
                    @if($r->payment_status == 'paid')
                        <i class="fa fa-check"></i>
                    @else
                        <input data-index="{{ $loop->index }}" id="payment-checked-{{ $row->id }}-{{ $loop->index }}" type="checkbox" class="payment-checked-{{ $row->id }}" data-id="{{$r->id}}">
                    @endif
                </td>
                <td style="padding: 5px;">{{$r->no}}</td>
                <td style="padding: 5px;">{{\Carbon\Carbon::parse($r->date_s)->format('Y-m-d')}}</td>
                <td style="text-align: left;padding: 5px;">{{number_format($r->principal_s,-2)}}</td>
                <td style="text-align: left;padding: 5px;">{{number_format($r->interest_s,-2)}}</td>
                <td style="text-align: left;padding: 5px;">{{number_format($r->total_s,-2)}}</td>
                <td style="text-align: left;padding: 5px;">{{number_format($r->balance_s,-2)}}</td>
                @if($total_p >0)
                <td style="padding: 5px;"><a href="{{url("/admin/view-payment?schedule_id={$r->id}")}}" data-remote="false" data-toggle="modal" data-target="#show-view-payment" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a></td>
                @else
                    <td></td>
                @endif
            </tr>
        @endforeach
        <tr>
            <td colspan="8" style="padding: 20px;border: none">
                <button class="btn btn-info btn-flat btn-block btn-payment-{{ $row->id }}"><i class="fa fa-money"></i>Add Payment</button>
            </td>
        </tr>

    @endif
</table>

</div>
<div id="page{{ $row->id }}" style="width: 100%; height: 80%">
    <div id="popup{{ $row->id }}">
        <iframe id="iframe{{ $row->id }}"></iframe>
    </div>
</div>
<div class="modal fade" id="show-view-payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-x" style="width: 100%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel2"></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                {{-- <button type="button" onclick="printDiv()" class="btn btn-default glyphicon glyphicon-print"></button>--}}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<style>
    #popup{{ $row->id }} {
        background-color: white;
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 9999;
        display: none;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4)
    }
    #iframe{{ $row->id }} {
        border: 0;
        width: 70%;
        height: 80%;
        margin: 30px auto;
        position: relative;
        display: block;
        box-shadow: 0 2px 3px rgba(0,0,0,0.125);
    }

    html, body, #page{{ $row->id }} { height: 100%; }



</style>
<script>

    $(function () {

        $('.payment-checked-{{ $row->id }}').attr("disabled", true);
        $('.payment-checked-{{ $row->id }}:first').removeAttr("disabled");

        $('body').on('change','.payment-checked-{{ $row->id }}',function () {
            var index = $(this).data('index')-0 + 1;
            if ( this.checked ) {
                $('#payment-checked-{{ $row->id }}-' + index).removeAttr("disabled");
            }else {
                var cc = '{{ $c }}' - 0;
                for ( ii = index; ii < cc; ii++) {
                    $('#payment-checked-{{ $row->id }}-' + ii).prop('checked', false);
                    $('#payment-checked-{{ $row->id }}-' + ii).attr("disabled", true);
                }
            }
        });


    });

    $(function () {

        $("#show-view-payment").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);

            $(this).find(".modal-body").load(link.attr("href"));
        });
        $('body').on('click','.btn-payment-{{ $row->id }}',function (e) {
            e.preventDefault();
            var i = 0;
            var param = '';
            $('.payment-checked-{{ $row->id }}:checked').each(function () {
                var id_c = $(this).data('id') -0;
                param += ((i>0?'x':'') + '"'+id_c+'"');
                i++;
            });

            if(param != '' && i >0){

                var src = '{!! url('admin/loanpayment/create?is_frame=1&id='.$row->id) !!}&param=' + param ;

                $('#popup{{ $row->id }}').show();
                $('#iframe{{ $row->id }}').prop('src',src);
                //document.getElementById('page').className = "darken";

                document.getElementById('page{{ $row->id }}').onclick = function() {
                    $('#popup{{ $row->id }}').hide();
                    window.location.reload();
                    $('#iframe{{ $row->id }}').prop('src','');
                };


                return false;

            }
        });
    });
    function myIframe{{ $row->id }}() {
        $('#popup{{ $row->id }}').hide();
        window.location.reload();
        $('#clear-{{ $row->id }}').remove();
    }
</script>
