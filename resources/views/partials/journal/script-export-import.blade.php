<div class="row" style="padding-top: 25px">
    <div class="col-md-9">
        <input type="file" name="open_detail_file" class="form-control">
    </div>
    <div class="col-md-3">
        <span><a href="{{url('api/download-journal-expense')}}" class="btn btn-primary">Download</a></span>
    </div>

</div>

@push('crud_fields_scripts')
    <script>

        $(function () {
            $('form').prop('enctype', 'multipart/form-data');
        });

    </script>
@endpush
