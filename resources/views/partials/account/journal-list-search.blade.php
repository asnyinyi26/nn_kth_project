
@if($g_journals != null && $data != null)
    <?php
    $total_dr = 0;
    $total_cr = 0;
    $journal_id=0;
//    dd($g_journals);
    ?>
    @foreach($data as $g)

        <?php
        if(isset($_REQUEST['client_id']) && $_REQUEST['client_id'] != 0){
            $client_id = $_REQUEST['client_id'];
            $g_d = \App\Models\GeneralJournalDetail::where('id',$g->id)->where('name', $client_id)->first();
        }
        else {
            $g_d = \App\Models\GeneralJournalDetail::where('id',$g->id)->first();
        }
        
        $t_dr = 0;
        $t_cr = 0;
        //dd($g_d);
        ?>
            @if($g_d != null)
                <?php
                $ref = '';
                $type = $g_d->tran_type;
                $cus = optional(\App\Models\Customer::find($g_d->name));
                $acc_chart_name = \App\Models\AccountChart::where('code',$g_d->acc_chart_code)->get()->first();
                if($type == 'loan-deposit'){
                    $deposit = \App\Models\LoanDeposit::find($g_d->tran_id);
                    $ref = optional($deposit)->referent_no;
                }
                if($type == 'loan-disbursement'){
                    $deposit = \App\Models\PaidDisbursement::find($g_d->tran_id);
                    $ref = optional($deposit)->reference;
                }
                if($type == 'payment'){
                    $deposit = \App\Models\LoanPayment::find($g_d->tran_id);
                    $ref = optional($deposit)->payment_number;
                }
                $j_id=$g_d->journal_id;
                $branch = \App\Models\Branch::find($g_d->branch_id);
                ?>
                <tr>
                    <td style="white-space: nowrap;text-transform: capitalize">
                        @if ($j_id != $journal_id)
                        {{$g_d->tran_type}}
                        @endif
                    </td>
                    <td style="white-space: nowrap;">
                        @if ($j_id != $journal_id)
                        {{$g->date_general->format('Y-m-d')}}
                        @endif
                    </td>
                    <td style="text-align: center;white-space: nowrap;">
                        @if ($j_id != $journal_id)
                        {{$g->reference_no}}
                        @endif
                    </td>
                    <td style="white-space: nowrap;">

                        {{$g->tran_reference}}</td>
                    <td style="white-space: nowrap;">

                        {{optional($branch)->code}} - {{optional($branch)->title}}</td>
                     @if(companyReportPart() == 'company.mkt')
                    <td style="white-space: nowrap;">

                    {{Auth()->user()->user_code}}
                    </td>
                    @endif
                    <td style="white-space: nowrap;">
                        {{optional($cus)->client_number}}
                    </td>
                    <td style="white-space: nowrap;">
                        {{optional($cus)->name}}
                    </td>
                    <td style="white-space: nowrap;">{{$g_d->external_acc_chart_code}}</td>
                    @if (isset($g_d->acc_chart_code))
                        <td style="white-space: nowrap;">{{$g_d->acc_chart_code}}</td>
                    @else
                        <td style="white-space: nowrap;">No Data</td>
                    @endif
                    @if (isset($acc_chart_name->name))
                      <td>{{$acc_chart_name->name}}</td>
                    @else
                     <td style="white-space: nowrap;">No Data</td>
                    @endif
                    <td>{!! $g_d->description !!}</td>
                    <td style="text-align: right">{{$g_d->dr>0?numb_format($g_d->dr,2):''}}</td>
                    <td style="text-align: right">{{$g_d->cr>0?numb_format($g_d->cr,2):''}}</td>

                    <td style="text-align: center; white-space: nowrap;">
                        @if ($j_id != $journal_id)
                            @if($g_d->tran_type == 'journal')

                                @if(_can('update-general-journal'))
                                    <a href="{{url('admin/general-journal/'.$g->journal_id.'/edit')}}" class="btn btn-xs btn-warning" ><i class="fa fa-edit"></i> </a>
                                @endif

                                @if(_can('update-general-journal'))
                                    <a href="{{url('admin/delete-journal/'.$g->journal_id)}}" class="btn btn-xs btn-danger" ><i class="fa fa-trash"></i> </a>
                                @endif


                            @endif
                        @endif
                    </td>
                </tr>
        @if ($j_id != $journal_id)
            <?php
            $journal_id=$j_id;
            ?>
        @endif
                <?php
                $total_dr += $g_d->dr;
                $total_cr += $g_d->cr;
                ?>
                @endif
            @endforeach

    <tr>
        <td colspan="11"><b>Total</b></td>
        <td style="text-align: right"><b>{{$total_dr>0?numb_format($total_dr,2):''}}</b></td>
        <td style="text-align: right"><b>{{$total_cr>0?numb_format($total_cr,2):''}}</b></td>
        <td></td>
        <td></td>
    </tr>

@endif
