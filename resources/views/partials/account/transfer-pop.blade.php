<style>
    #show-detail-modal .modal-dialog{
        padding-left: 30%;
        padding-right: 30%;
        float:center;
        font-size: 19px;
        font-family: 'Bodoni MT'; 
    }

    table {
        border-collapse: collapse;
        font-size: 18px;
    }
    th{
        font-weight: bold;
        padding-right: 15px;
    }
    #border{
        border: 1px solid black;
        padding: 7px 10px;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

@if($row != null)
<?php
    $m = getSetting();
    $logo = getSettingKey('logo',$m);
    $company = getSettingKey('company-name',$m);

    $from_acc = \App\Models\AccountChart::find($row->from_cash_account_id, ['name']);
    $to_acc = \App\Models\AccountChart::find($row->to_cash_account_id, ['name']);
    $transfer_by = \App\User::find($row->transfer_by_id, ['name']);
    $receive_by = \App\User::find($row->receive_by_id, ['name']);
?>
<div id="DivIdToPrintPop">
    <center>
    <table>
        <tr>
            <td><span style = "font-weight:bold; font-size:19px;">MoeYan Microfinance Ltd Transfer</span></td>
        </tr>
    </table>
    </center>
    <br>
    <center>
    <table style="text-align: left;">
        <tr>
            <th width="110">Reference No</th>
            <td>: {{$row->reference_no}}</td>
        </tr>
        <tr>
            <th>From Cash</th>
            <td>: {{ $from_acc->name }}</td>
        </tr>
        <tr>
            <th>To Cash</th>
            <td>: {{ $to_acc->name }}</td>
        </tr>
        <tr>
            <th>Transfer By</th>
            <td>: {{ $transfer_by->name }}</td>
        </tr>
        <tr>
            <th>Receive By</th>
            <td>: {{ $receive_by->name }}</td>
        </tr>
        <tr>
            <th>Amount</th>
            <td>: {{ number_format($row->t_amount) }}</td>
        </tr>
        <tr>
            <th>စာသားဖြင့်</th>
            <td style="text-transform:uppercase">: {{ App\Helpers\S::convert_number_to_words($row->t_amount) }}</td>
        </tr>
        <tr>
            <th>Date</th>
            <td>: {{ $row->t_date }}</td>
        </tr>
        @if($row->description)
        <tr style="padding-top:10px;">
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="3" border="1" id="border">{{ Illuminate\Mail\Markdown::parse($row->description) }}</td>
        </tr>
        @endif
        <tr>
            <th style="padding-top:10px;">ငွေပေးသူ:</th>
            <td style="padding-top:10px;">......................................</td>
        </tr>
        <tr>
            <th>ငွေလက်ခံသူ</th>
            <td>......................................</td>
        </tr>
        <tr>
            <th>အတည်ပြုသူ</th>
            <td>......................................</td>
        </tr>
    </table>
    </center>
</div>
<br>
<div class="tab-content">
<script>

$(document).ready(function(){
    window.print();
});
</script>
@endif
