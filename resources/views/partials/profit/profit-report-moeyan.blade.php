<?php
$profit_details = \App\Models\GeneralJournalDetail::where('journal_id', $profit_id)
			 ->where('cr', '>' ,0) 
			 ->get();
			 
$total = \App\Models\GeneralJournalDetail::where('journal_id', $profit_id)
			 ->where('dr', '>' ,0) 
			 ->first();			 

$profit_general = \App\Models\JournalProfit::find($profit_id);

?>
<style type="text/css">
body{
	font-size:12px;
}

td {
	padding-top: 5px;
}
#data{
	margin-top:10px;
}

#data td{
	font-size:13px;
	font-weight:normal;
	padding:5px;
}

th{
	padding:5px;
	font-size:14px;
}

.bor{
	border:1px solid black;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<center>
      <div class="container mt-3">
      	<div>
			<span style="font-size:16px;">
				<strong>Star Moe Yan</strong>
				<strong>Microfinance</strong>
				<strong>Cash Receipt</strong>
			</span>
			<br><br>
			<span>Date :
				<strong>{{date('d-m-Y', strtotime(optional($profit_general)->date_general))}}</strong>

				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				Voucher No :
				<strong>{{optional($profit_general)->reference_no}}<strong>
			</span>
      	</div>
		</center>
		<center>
		<table id="data">
			<thead>
				<tr>
					<th class="bor">Ac Code</th>
					<th class="bor">Description</th>
					<th class="bor">Amount</th>
				</tr>
			</thead>

			<tbody>
			@foreach ($profit_details as $profit_detail)
			@php
				$profit = \App\Models\JournalProfit::find($profit_id);
			@endphp
				<tr>
					<td class="bor">
						{{optional($profit_detail)->acc_chart_code}}
					</td>

					<td class="bor" width="200">
						{{optional($profit)->note}}
					</td>

					<td class="bor">{{number_format(optional($profit_detail)->cr)}}</td>
				</tr>
			@endforeach
				

				<tr>
					<td colspan="2" class="bor">Total</td>
					<td  class="bor">{{number_format(optional($total)->dr)}}</td>
				</tr>
			</tbody>

			<tr>
			<td style="padding-top:20px">စာဖြင့် - &nbsp </td>
			<td colspan="2" style="padding-top:20px"><strong>{{ strtoupper(\App\Helpers\S::convert_number_to_words(optional($profit_detail)->cr))}} KYATS<strong></td>
			</tr>

			<tr>
				<th style="padding-top:10px;">ငွေပေးသူ:</th>
				<td style="padding-top:10px;">......................................</td>
			</tr>
			<tr>
				<th>ငွေလက်ခံသူ</th>
				<td>......................................</td>
			</tr>
			<tr>
				<th>အတည်ပြုသူ</th>
				<td>......................................</td>
			</tr>
		</table>
		<center>
<script>
$(document).ready(function(){
    window.print();
});
</script>
</div>