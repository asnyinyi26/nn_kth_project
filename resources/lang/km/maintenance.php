<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'serial_id' => 'លេខកូដសម្ភារ',
    'completed_date' => 'ថ្ងៃជួសជុលរួច',
    'start_date' => 'ថ្ងៃជួសជុល',
    'invoice_number' => 'លេខវិក្កយបត្រ',
    'exchange_rate' => 'អតា្រប្ដូរប្រាក់',
    'currency' => 'រូបិយប័ណ្ណ',
    'price' => 'តម្លៃ',
    'vendor_id' => 'ហាងជួសជុល',
    'image_invoice' => 'រូបភាពវិក្កយបត្រ',
    'image' => 'រូបភាព',
    'note' => 'កំណត់សម្កាល់ផ្សេងៗ',
    'user_id' => 'អ្នកប្រើប្រាស់',
    'm_type' => 'ប្រភេទការជួសជុល',


];
