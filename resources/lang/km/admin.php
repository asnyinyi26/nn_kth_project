<?php

return [

   'name' => 'ឈ្មោះ',
   'email' => 'អ៊ីម៉ែល',
   'phone' => 'ទូរស័ព្ទ',
   'password' => 'ពាក្យសម្ងាត់',
   'confirm_password' => 'បញ្ជាក់ពាក្យសម្ងាត់',
   'organization_chart' => 'រចនាសម្ព័ន្ធ',
   'photo' => 'រូបថត',
   'people' => 'មន្ដ្រី',
   'select_a_people' => 'ជ្រើសរើសមន្ដ្រី',


];
