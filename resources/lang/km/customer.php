<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'female' => 'ស្រី',
    'male' => 'ប្រុស',

    'active' => 'Active',
    'inactive' => 'Inactive',
    'blacklist' => 'Blacklist',


    'single' => 'នៅលីវ',
    'married' => 'រៀបការ',
    'widow' => 'មេម៉ាយ/ពោះម៉ាយ',
    'divorced' => 'លះលែង',
    'bon' => 'BON-ល្អ',
    'moy' => 'MOY-មធ្យម',
    'mau' => 'MAU-ខូចជួសជុលបាន',
    'ref' => 'REF ខូច100%',
    //'BON-ល្អ', 'MOY-មធ្យម', 'MAU-ខូចជួសជុលបាន', 'REF ខូច100%'

    'free' => 'ទំនេរ',
    'use' => 'កំពុងប្រើប្រាស់',
    'repair' => 'ជួសជុល',
    'loss' => 'ខូច​ ១០០%',


];
