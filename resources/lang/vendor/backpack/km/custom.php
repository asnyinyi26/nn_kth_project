<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\Base Language Lines
    |--------------------------------------------------------------------------
    */

    'house_no' => 'ផ្ទះលេខ',
    'street_no' => 'ផ្លូវលខ',
    'province_label' => 'Province',
    'province' => 'រាជធានីភ្នំពេញ',
    'district_1' => 'ខណ្ឌ',
    'district_2' => 'ស្រុក',
    'commune_label' => 'Commune',
    'commune_1' => 'សង្កាត់',
    'commune_2' => 'ឃុំ',
    'village' => 'ភូមិ',
    'city_1' => 'ក្រុង',
    'city_2' => 'ខេត្ត',
];
